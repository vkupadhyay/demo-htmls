<?php
/**
 * Template Name: Post detail Page
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

 get_header('custom');
?>
<style>
	.post-details-tags{
		list-style:none;
		margin:0px;
		padding:0px;
		display:flex;
		flex-wrap:wrap;
	}
	.post-details-tags li{
		padding: 6px 10px;
		border: 1px #ffc107 solid;
		border-radius: 5px;
		margin: 10px 5px 0 5px;
		background: #fff;
		color:#000;
	}
	.post-details-tags li:first-child{
		margin-left:0px;
	}
</style>
<div class="news-details-page">
	<div class="main-container-div">
		<div class="news-title">
			<h1 title="<?php the_title(); ?>"><?php the_title(); ?></h1>
			<div class="author-details-page"><?php get_template_part( 'template-parts/content', 'author' ); ?></div>
		<?php 	$tags = wp_get_post_tags( get_the_ID() );
if ( $tags ) {
    echo '<ul class="post-details-tags">';
    foreach ( $tags as $tag ) {
		 $tag_link = get_tag_link( $tag->term_id ); // Get the tag archive link
        echo '<li><a href="' . esc_url( $tag_link ) . '">' . $tag->name . '</a></li>';
    }
    echo '</ul>';
} ?>
		</div>
		<div class="custom-row">
			<div class="site-details-left">
			  <div class="margin-tp-btm"><?php dynamic_sidebar('AdsDisplay')?></div>
				<div class="news-content">
					<picture><?php the_post_thumbnail('full'); ?></picture>
                     <div style="height:15px;"></div>
					<?php echo the_content(); ?>
		  		  <div class="acf--img-section">
<?php if( have_rows('image_upload_box') ): ?>
    
    <?php while( have_rows('image_upload_box') ): the_row(); 
       
		$sub_value_image = get_sub_field('image_upload');
	    $sub_value_url = get_sub_field('image_url');
					  $image_id = get_sub_field('image_upload');
$img_url = wp_get_attachment_url($sub_value_image, 'full');
$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
$image_title = get_the_title($image_id);
        ?>
        <div class="acf--img">
           <div class="uploaded-img">
			<a href="<?php echo $sub_value_url; ?>" target="_blank" title="<?php echo $image_title; ?>"><img src="<?php echo $img_url; ?>" alt="<?php echo $image_title; ?>" title="<?php echo $image_title; ?>"> </a>
    <div class="download-link">
		<a class="btn-primary" title="<?php echo $image_title; ?>" href="<?php echo $img_url; ?>" download="">download</a>
			   </div>  
			</div>
			
			<h4 style=" margin: 14px 0;"><a href="<?php echo $sub_value_url; ?>" target="_blank" title="<?php echo $image_title; ?>"> <?php echo $image_title; ?> </a></h4>
        </div>
    <?php endwhile; ?>
  
<?php endif; ?>
		  </div>
					<div class="custom-full-img" style="margin-top: 10px;">
						<?php if( get_field('image_upload_1') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_1'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" title="<?php the_title(); ?>" title="<?php the_title(); ?>" href="<?php the_field('image_upload_1'); ?>" download>download</a>
							</div>
							<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
						</figure>
						
						
						<?php endif; ?>

						<div class="colrow">
						<?php if( get_field('image_upload_11') ): ?><div class="colrowbox">
							
							<figure class="acf--img">
								<img src="<?php the_field('image_upload_11'); ?>" alt="<?php the_title(); ?>" />
								<div class="download-link">
									<a class="btn-primary" title="<?php the_title(); ?>" title="<?php the_title(); ?>" href="<?php the_field('image_upload_11'); ?>" download>download</a>
								</div>
								<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
							</figure>
							
							
								</div><?php endif; ?>

								<?php if( get_field('image_upload_12') ): ?><div class="colrowbox">
							
							<figure class="acf--img">
								<img src="<?php the_field('image_upload_12'); ?>" alt="<?php the_title(); ?>" />
								<div class="download-link">
									<a class="btn-primary" title="<?php the_title(); ?>" title="<?php the_title(); ?>" href="<?php the_field('image_upload_12'); ?>" download>download</a>
								</div>
								<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
							</figure>
							
							
								</div><?php endif; ?>

								<?php if( get_field('image_upload_13') ): ?><div class="colrowbox">
							
							<figure class="acf--img">
								<img src="<?php the_field('image_upload_13'); ?>" alt="<?php the_title(); ?>" />
								<div class="download-link">
									<a class="btn-primary" title="<?php the_title(); ?>" title="<?php the_title(); ?>" href="<?php the_field('image_upload_13'); ?>" download>download</a>
								</div>
								<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
							</figure>
							
							
								</div><?php endif; ?>

							
							
						</div>

						<?php if( get_field('image_upload_2') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_2'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" title="<?php the_title(); ?>" href="<?php the_field('image_upload_2'); ?>" download>download</a>
							</div>
							<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
						</figure>
						
						<?php endif; ?>

						<?php if( get_field('image_upload_3') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_3'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" title="<?php the_title(); ?>" href="<?php the_field('image_upload_3'); ?>" download>download</a>
							</div>
							<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
						</figure>
						
						<?php endif; ?>

						<?php if( get_field('image_upload_4') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_4'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" title="<?php the_title(); ?>" href="<?php the_field('image_upload_4'); ?>" download>download</a>
							</div>
							<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
						</figure>
						
						<?php endif; ?>

						<?php if( get_field('image_upload_5') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_5'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" title="<?php the_title(); ?>" href="<?php the_field('image_upload_5'); ?>" download>download</a>
							</div>
							<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
						</figure>
						<div class="margin-tp-btm"><?php dynamic_sidebar('AdsDisplay')?></div>
						<?php endif; ?>

						<?php if( get_field('image_upload_6') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_6'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" title="<?php the_title(); ?>" href="<?php the_field('image_upload_6'); ?>" download>download</a>
							</div>
							<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
						</figure>
						
						<?php endif; ?>
						<?php if( get_field('image_upload_7') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_7'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" title="<?php the_title(); ?>" href="<?php the_field('image_upload_7'); ?>" download>download</a>
							</div>
							<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
						</figure>
						
						<?php endif; ?>
						<?php if( get_field('image_upload_8') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_8'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" title="<?php the_title(); ?>" href="<?php the_field('image_upload_8'); ?>" download>download</a>
							</div>
							<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
						</figure>
						
						<?php endif; ?>
						<?php if( get_field('image_upload_9') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_9'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" title="<?php the_title(); ?>" href="<?php the_field('image_upload_9'); ?>" download>download</a>
							</div>
							<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
						</figure>
						
						<?php endif; ?>
						<?php if( get_field('image_upload_10') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_10'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" title="<?php the_title(); ?>" href="<?php the_field('image_upload_10'); ?>" download>download</a>
							</div>
							<figcaption class="wp-element-caption"><?php the_title(); ?></figcaption>
						</figure>
						<div class="margin-tp-btm"><?php dynamic_sidebar('AdsDisplay')?></div>
						<?php endif; ?>
					</div>

					<?php get_template_part( 'template-parts/content', 'featured_post' ); ?>
				</div>
			</div>

			<div class="site-details-right">
			  <?php get_template_part( 'template-parts/content', 'right' ); ?>
			</div>
		</div>

		
	</div>

	<div class="related-wallpaper-section">
		<div class="main-container-div">
			<h3>Popular <span>Wallpapers</span></h3>
			<?php get_template_part( 'template-parts/content', 'related_wallpaper' ); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
