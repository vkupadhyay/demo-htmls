<?php
/**
 * Template Name: Mult Wallpaper
 */

get_header(); ?>

<div class="wallpapershow-section">
	<h2><?php the_title(); ?></h2>
	
	<div class="wallpapershow wallpapershow-2">
	<?php the_content(); ?>
	</div>
</div>

<?php get_footer(); ?>
