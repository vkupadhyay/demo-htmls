<?php
/**
 * Template Name: contact Page
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
?>

<div class="contact-page">
  <div class="full-container">
    <div class="custom-row">
      <div class="contact-form-box">
          <?php echo the_content(); ?>
      </div>
      <div class="contact-info">
        <h3>Connect with us</h3>
            <div class="social-media">
              <ul>
                <li class="facebook-link"><a href="https://www.facebook.com/freewallpapersforu/" target="_blank" title="https://www.facebook.com/freewallpapersforu/"><i><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/facebook.png" width="100%" alt="freewallpapers4u.in Facebook" /></i> Follow us on Facebook</a></li>
                <li class="instagram-link"><a href="https://www.instagram.com/freewallpapers4u.in/" target="_blank" title="https://www.instagram.com/freewallpapers4u.in/"><i><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/instagram.png" width="100%" alt="freewallpapers4u.in Instagram" /></i> Follow us on Instagram</a></li>
                <li class="youtube-link"><a href="#" target="_blank"><i><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/youtube.png" width="100%" alt="freewallpapers4u.in Youtube" /></i> Follow us on Youtube</a></li>
                <li class="mail-link"><a href="mailto:vimalsharma.sharma1@gmail.com" title="mailto:vimalsharma.sharma1@gmail.com" target="_blank"><i><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/mail.png" width="100%" alt="freewallpapers4u.in Mail" /></i> Follow us on Mail</a></li>
              </ul>
            </div>
      </div>
    </div>
   </div>
</div>


<?php get_footer(); ?>
