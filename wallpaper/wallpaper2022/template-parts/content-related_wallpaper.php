<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pictures
 */

?>

<div class="related-wallpaper">
			<ul class="wpp-list wpp-list-with-thumbnails">
			<?php
				query_posts(array('orderby' => 'rand', 'showposts' => 8));
				if (have_posts()) :
				while (have_posts()) : the_post(); ?>
				<li>
					<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
					<a href="<?php the_permalink() ?>" class="wpp-post-title" target="_self"><?php the_title(); ?></a>
                </li>
				<?php endwhile; endif; ?>
			</ul>
</div>
