<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pictures
 */

?>

<div class="featured_post">
						<?php
							$featured_posts = get_field('featured_post');
							if( $featured_posts ): ?>
							
													<div class="figure-row">
														<?php foreach( $featured_posts as $post ): 

								// Setup this post for WP functions (variable must be named $post).
								setup_postdata($post); ?>
														<figure class="acf--img">
															<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(full); ?></a>
															<div class="download-link">
																<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
																<a class="btn-primary" title="<?php the_title(); ?>" href="<?php echo $url ?>" download>download</a>
															</div>
															<h4 class="wp-element-caption" style="margin-top: 10px;"><a href="<?php the_permalink(); ?>" target="_blank" title="<?php the_title(); ?>"> <?php the_title(); ?></a></h4>
														</figure>

														
														<?php endforeach; ?>
													</div>
													<?php 
							// Reset the global post object so that the rest of the page works correctly.
							wp_reset_postdata(); ?>
						<?php endif; ?>
					</div>
