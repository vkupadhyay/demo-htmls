<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wallpaper
 */

get_header();
?>

<div class="custom-row">
   <div class="site-details-left">
      <h1><?php the_title(); ?></h1>
      
	  <div class="wallpapershow-section wallpapershow-3">
            <div class="wallpapershow wallpapershow-3">
			<?php if ( have_posts() ) : ?>
				<div class="figure-row">
				
						<?php
						/* Start the Loop */
						while ( have_posts() ) :
							the_post();get_template_part( 'template-parts/content', 'search' );

						endwhile;
						else :
							get_template_part( 'template-parts/content', 'none' );

						endif;
						?>
		        </div>
           
      </div> </div> </div>
    
    <div class="site-details-right">
      <?php get_template_part( 'template-parts/content', 'right' ); ?>
    </div>
</div>




<?php
get_footer();
