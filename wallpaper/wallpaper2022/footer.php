<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wallpaper
 */

?>
</section>
<!-- #content -->

<footer class="main-footer">
            <h3>free wallpapers for desktop & mobile phones</h3>
            <div class="desc--box">
               <p>Free Wallpapers - Download Free Cool Wallpapers for desktop & mobile phones, Download Free many categories hd Wallpapers for Mobile Phone, Free Wallpapers Backgrounds. Huge collection best Free desktop Wallpapers to download.Get the top searched phone wallpapers freewallpapers4u.in.</p>
            </div>
	<div class="footer-top-keywords">
		<p>
			<strong>High-resolution wallpapers</strong>, <strong>Abstract wallpaper designs</strong>, <strong>Nature-inspired backgrounds</strong>, <strong>4K desktop wallpapers</strong>, <strong>Mobile phone wallpapers</strong>, <strong>Minimalist wallpaper art</strong>, <strong>Vintage desktop backgrounds</strong>, <strong>HD scenic wallpapers</strong>, <strong>Customizable wallpaper images</strong>, <strong>Digital art wallpapers</strong>, <strong>Space-themed backgrounds</strong>, <strong>Colorful wallpaper patterns</strong>, <strong>Desktop wallpaper collection</strong>, <strong>Inspirational quote wallpapers</strong>, <strong>Dynamic dual monitor wallpapers</strong>, <strong>3D rendered desktop backgrounds</strong>, <strong>Animal-themed wallpaper art</strong>, <strong>Patterned mobile wallpapers</strong>, <strong>Architectural wallpaper designs</strong>, <strong>Retro wallpaper aesthetics</strong>, <strong>Fantasy landscape wallpapers</strong>, <strong>Watercolor desktop backgrounds</strong>, <strong>Motivational desktop wallpapers</strong>, <strong>Urban cityscape wallpapers</strong>, <strong>Vibrant abstract backgrounds</strong>
		</p>
	</div>
            <strong>All wallpapers are copyrighted by their respective authors. If the copyright of any wallpaper belongs to you, Contact Us and we'll remove it !</strong>
            <div class="footer-link"><a href="https://www.freewallpapers4u.in/privacy-policy/">Privacy policy</a></div>
       
         </footer>
       

<!-- #colophon -->
</section> 
<!-- #page -->


<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/slick.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script> 

</body></html>