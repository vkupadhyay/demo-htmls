$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 300) {
        $(".main-header").addClass("bluebackbg");
    } else {
        $(".main-header").removeClass("bluebackbg");
    }
});

// setTimeout(function() {
//     $('#splace').addClass("hide-splace");
//     $('#header').addClass("show-header");
//     $('#splaceText').addClass("show-text-hero");
// }, 1000);

$('#navclick').click(function(){
    $(mainContainer).toggleClass('menu-slide');
    $(clickmenu).toggleClass('menu-slide');
});

$(".button").click(() => {
    $("selector").addClass("popshow");
});

$('.heroslider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 500,
    fade: true,
    cssEase: 'linear'

});

$('.testimonials-slider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 2000

});

AOS.init();