 <?php
 $clientblock_id = 67;
 $clientblock = get_post($clientblock_id);//clientblock block
 $clientblock_image = get_the_post_thumbnail_url($clientblock);
 $clientLink= get_post_meta($clientblock_id,'readmore_button_link',true);
 
 ?>
<?php if($clientblock) { ?>
<div class="what__we__do__section padding_top_50">
<div class="wpr__1">
	<h2 class="fadeInDown wow"><?php echo $clientblock->post_title;?></h2>
	<div class="what__we__do__section_btm fadeInRightBig wow">
		<?php echo apply_filters('the_content',$clientblock->post_content);?>
		<div class="yello_btn"><a href="<?php echo $clientLink;?>"><?php echo get_post_meta($clientblock_id,'readmore_button_title',true);?></a></div>
	</div>

</div>
</div>
<?php } ?>				
