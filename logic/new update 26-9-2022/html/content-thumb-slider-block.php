<?php 
 
 	$args = array( 
	'post_type'   =>
'team', 'numberposts' => -1, 'post_status' => 'publish', 'orderby' => 'menu_order', 'order'=>'ASC', ); $peoples = get_posts( $args ); ?>
<?php	if($peoples){?>

<div class="profile__section">
	<div class="profile__section__in">
	
			<div class="profile__section__top">
				<div class="wpr__1">
					<h2 class="fadeInDown wow">This is us ...</h2>
				</div>
			</div>

			<div class="profile__section__btm">
				<div class="profile__list">
					<ul class="slideInRight wow">
						<?php foreach($peoples as $people) { 
							$postLink = get_permalink($people);
							$profile_url = types_render_field( 'profile-thumb-image', array( 'raw' =>true, 'id' =>$people->ID) ); $profile_position = types_render_field( 'profile-position', array( 'raw' =>true, 'id' =>$people->ID) ); ?>

						<li>
							<div class="profile__img">
								<a href="<?php echo $postLink;?>"><img src="<?php echo $profile_url;?>" alt="" /></a>
							</div>
							<div class="profile__text">
								<div class="profile__text__in">
									<h3>
										<a href="<?php echo $postLink;?>"><?php echo $people->post_title;?></a>
									</h3>
									<p>
										<a href="<?php echo $postLink;?>"><?php echo $profile_position;?></a>
									</p>
									<a class="readprofile" href="<?php echo $postLink;?>">READ PROFILE</a>
								</div>
							</div>
						</li>
						<?php }  ?>

						<li class="last-see-job-block" style="<?php isset($_GET['job_show'])?'':'';?>">
							<div class="profile__img fdfdfdf">
								<a href="/vacancy/client-services-administrator/"> <img src="/wp-content/uploads/2021/10/blank_image.png" alt="" /></a>
							</div>
							<div class="profile__text">
								<div class="profile__text__in">
									<h3><a href="/vacancy/client-services-administrator/">WANT TO JOIN A SMALL BUT GROWING BUSINESS? </a></h3>
									<p><a href="/vacancy/client-services-administrator/">We have an exciting vacancy in our team. </a></p>
									<a class="readprofile" href="/vacancy/client-services-administrator/">See job spec </a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		<div class="clear"></div>
	</div>
</div>

<?php }  ?>
