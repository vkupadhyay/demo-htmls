<?php

/**
 * Template Name: Logic Guides With Filter Template
 * Author: Raj keer
 */

get_header(); ?>


<div class="list__page logic-guides-page">

    <div class="simple__details__hd">
        <div class="wpr__2">
            <h1 class="page-title fadeInDown wow"><?php the_title(); ?></h1>
        </div>
    </div>

    <div class="blog__list">
        <div class="wpr__1">
            <div class="filter__outer heading-logic-guide">
                <div class="logic-guide-content-top">
                    <?php the_content(); ?>
                </div>
                <div class="clear"></div>
            </div>


            <div class="row">
                <div class="col-sm-3">
                    <div id="secondary" class="widget-area category-filter logic-guide-filter" role="complementary">
                        <h3>Filter by Category</h3>
                        <?php

                        if (isset($_GET['categories'])) {
                            $current_countries = $_GET['categories'];
                        }else{
                            $current_countries= [0];
                        }

                        $countries = get_categories(array(
                            'hide_empty'      => false,
                            'taxonomy' => 'guide-category',
                            'parent'  => 0,
                            'orderby' => 'name',
                            'order'   => 'ASC'
                        ));
                        ?>

                        <form action="<?php the_permalink(); ?>" id="filterForm" method="get">
                            <div class="fitler-item-list">

                                <?php
                                foreach ($countries as $country) {
                                   $setChecked = in_array($country->cat_ID, $current_countries)?"checked=\"checked\"":'';
                                    echo sprintf('<div class="filter-item checkbox-filter-item"><input type="checkbox" class="css-checkbox" name="categories[]" onChange="this.form.submit()" id="checkbox_%s" value="%s" %s', $country->name, $country->cat_ID, $setChecked);
                                    echo sprintf('/><label class="css-label" for="checkbox_%s">%s</label></div>', $country->name, $country->name);
                                }
                                ?>
                            </div>    
                            <a  class="guide-btn" href="<?php the_permalink(); ?>">SHOW ALL GUIDES</a>

                        </form>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="row">

                        <?php
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array(
                            'post_status' => 'publish',
                            'post_type'   => 'logic-guide',
                            'orderby' => 'post_date',
                            'posts_per_page' => '6',
                            'paged' => $paged,
                            'order' => 'DESC',
                        );
                        $the_query = new WP_Query($args);

                        ?>

                        <?php if ($the_query->have_posts()) : ?>

                            <!-- pagination here -->

                            <!-- the loop -->
                            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                                <?php
                                $download_guide_link = types_render_field("logic-guide-download-file");
                                $article_postThumb = types_render_field("logic-guide-thumb", array("row" => true, 'url' => true, "size" => "thumbnail-guide"));
                                ?>
                                <div class="col-sm-6 col-md-6">
                                    <div class="logic-guide-box zoomInUp wow">
                                        <div class="logic-guide-box-inner">
                                            <div class="blog__img">
                                                <a href="<?php echo $download_guide_link ?>">
                                                    <img src="<?php echo $article_postThumb; ?>">
                                                </a>
                                            </div>
                                            <div class="blog__text">
                                                <h3><a href="<?php echo $download_guide_link ?>"><?php the_title(); ?></a></h3>
                                                <div class="article-desp">
                                                    <?php the_excerpt() ?>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="yello_btn">
                                            <a href="<?php echo $download_guide_link ?>" download="<?php echo sanitize_title(get_the_title()); ?>">
                                                    DOWNLOAD GUIDE 
                                                    <img width="15" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logic-financial-arrow.png">
                                            </a>
                                        </div>
                                       
                                    </div>
                                </div>
                            <?php endwhile; ?>
                            <!-- end of the loop -->

                            <!-- pagination here -->

                            <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                            <p>No Guides found. Please try again later.</p>
                        <?php endif; ?>


                    </div>
                    <div class="pangination clearbox">
                        <nav class="navigation pagination" role="navigation" aria-label=" ">
                            <h2 class="screen-reader-text"> </h2>
                            <div class="nav-links">
                                <?php
                                echo paginate_links(array(
                                    'base'         => str_replace(999999999, '%#%', esc_url(get_pagenum_link(999999999))),
                                    'total'        => $the_query->max_num_pages,
                                    'current'      => max(1, get_query_var('paged')),
                                    'format'       => '?paged=%#%',
                                    'show_all'     => false,
                                    'type'         => 'plain',
                                    'end_size'     => 2,
                                    'mid_size'     => 2,
                                    'prev_next'    => true,
                                    'prev_text' => __('&laquo; Prev', 'textdomain'),
                                    'next_text' => __('Next &raquo;', 'textdomain'),
                                    'add_args'     => false,
                                    'add_fragment' => '',
                                ));
                                ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php
get_footer();
