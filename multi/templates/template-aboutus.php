<?php

/**
 * Template Name: About Us Template
 * Template Post Type:  page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
?>

<main id="site-content" role="main">

  <?php

  if (have_posts()) {

    while (have_posts()) {
      the_post();
      $featured_image = get_the_post_thumbnail_url($post, 'full');

  ?>
      <div class="heading-img" style="background: url('<?php echo $featured_image; ?>')no-repeat center center / cover;">
        <div class="width-l-md">
          <h2><?php the_title(); ?></h2>
        </div>
      </div>
      <div class="center-text-section">
        <div class="width-x-sm">

          <?php if (types_render_field('page-title', array('raw' => true))) { ?>
            <h3><?php echo types_render_field('page-title', array('raw' => true)); ?></h3>
          <?php } ?>
          <?php echo types_render_field('page-content', array('raw' => false)); ?>
        </div>
      </div>

      <?php get_template_part('template-parts/content-about-multi-values'); ?>

      <?php /* if (types_render_field('team-expertise-content', array('raw' => true))) { ?>

        <div class="team-expertise-section">
          <div class="width-main">
            <div class="row">
              <div class="col-sm-6">
                <div class="team-expertise-box">
                  <h2><?php echo types_render_field('team-expertise-heading', array('raw' => true)); ?></h2>
                  <?php echo types_render_field('team-expertise-content', array('raw' => false)); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php } */ ?>

      <?php if (have_rows('team_content_row')): ?>
        <div class="content-with-image-wrapper">

          <?php while (have_rows('team_content_row')): the_row();
            $image = get_sub_field('image');
          ?>
            <div class="team-expertise-section <?php echo get_sub_field('image_position'); ?>">
              <div class="width-main">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="team-expertise-box">
                      <h2><?php echo  get_sub_field('title'); ?></h2>
                      <?php echo  get_sub_field('content'); ?>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="team-expertise-image">
                     <?php 
                     if( !empty($image['url']) ) {
                      echo  "<img src='{$image['url']}' alt='' title='{$image['title']}'/>";
                  } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br/>
          <?php endwhile; ?>
        </div>
      <?php endif; ?>



      <?php get_template_part('template-parts/content-ourpartners'); ?>

      <?php if (types_render_field('join-our-team-content', array('raw' => true))) { ?>
        <div class="health-safety-section">
          <div class="join-graphic"></div>
          <div class="width-x-sm">
            <h3><?php echo types_render_field('join-our-team-title', array('raw' => true)); ?></h3>
            <?php echo types_render_field('join-our-team-content', array('raw' => false)); ?>
            <div class="green-btn"><a href="<?php echo types_render_field('join-our-team-contact-url', array('raw' => true)); ?>">CONTACT US</a></div>
          </div>
        </div>
      <?php } ?>

  <?php

    }
  }

  ?>

</main><!-- #site-content -->

<?php get_template_part('template-parts/footer-menus-widgets'); ?>

<?php get_footer(); ?>