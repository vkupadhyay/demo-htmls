<?php

/**
 * Template Name: Contact Template
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
?>

<main id="site-content" role="main">

    <?php

    if (have_posts()) {

        while (have_posts()) {
            the_post();
            $featured_image = get_the_post_thumbnail_url($post, 'full');
    ?>
            <div class="heading-img contact-page-top" style="background: url('<?php echo $featured_image; ?>')no-repeat center center / cover;">
                <div class="width-l-md">
                    <h2><?php the_title(); ?></h2>
                </div>
            </div>
            <div class="contactus-page-btm">
                <div class="width-l-md">
                    <div class="row row-m">
                        <div class="col-sm-12">
                            <h3><?php the_field('contact_heading') ?></h3>
                        </div>
                    </div>
                    <div class="row row-m">
                        <div class="col-sm-4">
                            <div class="contactus-page-left">

                                <div class="address-box">
                                    <?php echo types_render_field('contact-address', array('raw' => false)); ?>
                                </div>
                                <div class="contactus-link">
                                    <a href="tel:<?php echo types_render_field('contact-telephone', array('raw' => true)); ?>"><?php echo types_render_field('contact-telephone', array('raw' => true)); ?></a>
                                    <a href="mailto:<?php echo types_render_field('contact-email', array('raw' => true)); ?>"><?php echo types_render_field('contact-email', array('raw' => true)); ?></a>
                                </div>
                                <div class="contactus-btm-link">
                                    <?php if(types_render_field('google-map-link', array('raw' => true))):?>
                                    <div class="green-btn">
                                        <a href="<?php echo types_render_field('google-map-link', array('raw' => true)); ?>" target="_blank">GOOGLE MAP</a>
                                    </div>
                                    <?php endif; ?>
                                </div>

                            </div>
                        </div>

                        <?php if (have_rows('contact_list_rows')) : ?>
                            <?php while (have_rows('contact_list_rows')) : the_row();
                            ?>
                                <div class="col-sm-4">
                                    <div class="contactus-page-left">
                                        <div class="address-box">
                                            <?php echo get_sub_field('contact_address', array('raw' => false)); ?>
                                        </div>
                                        <div class="contactus-link">
                                            <a href="tel:<?php echo get_sub_field('contact_telephone', array('raw' => true)); ?>"><?php echo get_sub_field('contact_telephone', array('raw' => true)); ?></a>
                                            <a href="mailto:<?php echo get_sub_field('contact_email', array('raw' => true)); ?>"><?php echo get_sub_field('contact_email', array('raw' => true)); ?></a>
                                        </div>
                                        <div class="contactus-btm-link">
                                          
                                           <?php if(get_sub_field('contact_map', array('raw' => true))):?> <div class="green-btn"><a href="<?php echo get_sub_field('contact_map', array('raw' => true)); ?>" target="_blank">GOOGLE MAP</a></div><?php endif;?>
                                        </div>

                                    </div>
                                </div>

                            <?php endwhile; ?>
                        <?php endif; ?>

                    </div>

                </div>

        <?php
        }
    }

        ?>

</main><!-- #site-content -->

<?php get_template_part('template-parts/footer-menus-widgets'); ?>

<?php get_footer(); ?>