<?php

/**
 * Template Name: Projects Template
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
?>

<main id="site-content" role="main">

  <?php

  if (have_posts()) {

    while (have_posts()) {
      the_post();

  ?>

      <div class="project-page-top">
        <div class="width-l-md">
          <h2>PROJECTS</h2>
          <div class="project-page-top-list">


            <?php get_template_part('template-parts/content-featured-project'); ?>

            <?php
            $childargs = array(
              'post_type' => 'project',
              'posts_per_page' => 6,
              'post_status' => 'publish',
              'orderby' => array( 
                'menu_order' => 'ASC',
                'post_date'      => 'DESC', 
             ) ,
              'category__not_in' => [3], //'featured-project'
            );

            $project_query  = new WP_Query($childargs);
            $total_counts = $project_query->found_posts;
            $total_pages = $project_query->max_num_pages;

            ?>

            <?php if ($project_query->have_posts()) : ?>

              <div class="project-row-btm">
                <div class="row">

                  <?php while ($project_query->have_posts()) : $project_query->the_post(); ?>
                    <div class="col-sm-4">
                      <div class="project-sm-box">
                        <div class="project-sm-box-img">
                          <a href="<?php echo get_permalink($post); ?>" class="project-img-link">
                            <img src="<?php echo types_render_field('project-thumb', array('raw' => true)); ?>" alt="" />
                          </a>
                        </div>
                        <h4><?php echo $post->post_title; ?></h4>
                        <a href="<?php echo get_permalink($post); ?>" class="project-link">Read story <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                      </div>
                    </div>

                  <?php endwhile;
                  wp_reset_postdata(); ?>
                  <!-- show pagination here -->
                  <div class="loadmoreprojects_wrapper"></div>
                </div>
              </div>
            <?php else : ?>
              <!-- show 404 error here -->
            <?php endif; ?>

          </div>

        </div>

      </div>
      <div class="back-row"><?php if($total_pages>1) {?> <a href="#" data-total-page="<?php echo $total_pages; ?>" class="misha_loadmore loadmoreprojects">LOAD MORE</a> <?php } ?></div>
  <?php

    }
  }

  ?>

</main><!-- #site-content -->

<script>
  var misha_loadmore_params = {};
  misha_loadmore_params.max_page = '<?php echo $total_pages; ?>';
  misha_loadmore_params.current_page = 1;


  jQuery(document).ready(function($) { // use jQuery code inside this to avoid "$ is not defined" error
    $('.misha_loadmore').click(function(e) {
      e.preventDefault();

      var button = $(this),
        data = {
          'action': 'loadmore',
          'post_args':<?php echo json_encode($childargs)?>,
          'page': misha_loadmore_params.current_page
        };

      $.ajax({ // you can also use $.post here
        url: '<?php echo site_url() . '/wp-admin/admin-ajax.php'; ?>', // AJAX handler
        data: data,
        type: 'POST',
        beforeSend: function(xhr) {
          button.text('LOADING...'); // change the button text, you can also ad d a preloader image
        },
        success: function(data) {
          console.log("called");

          if (data) {
            $('.loadmoreprojects_wrapper').before(data); // insert new posts
            misha_loadmore_params.current_page++;

            if (misha_loadmore_params.current_page == misha_loadmore_params.max_page) {
              button.remove(); // if last page, remove the button
            }
            button.text('LOAD MORE');
          } else {
            button.remove(); // if no data, remove the button as well
          }
        },

        error:function(){
          console.log("Something went wrong!!!");
        }
      });

      return false;
    });
  });
</script>
<?php get_template_part('template-parts/footer-menus-widgets'); ?>

<?php get_footer(); ?>