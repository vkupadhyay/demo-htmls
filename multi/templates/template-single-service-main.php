<?php
/**
 * Template Name: Service Detail Template
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
?>

<main id="site-content" role="main">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();
      $featured_image = get_the_post_thumbnail_url($post,'full');

?>
 <div class="heading-img" style="background: url('<?php echo $featured_image;?>')no-repeat center center / cover;">
    <div class="width-l-md">
          <h2><?php the_title();?></h2>
        </div>
  </div>

      <div class="center-text-section">
      <div class="width-x-sm">
          <h3><?php echo types_render_field( 'page-title', array( 'raw' =>true) );?></h3>
         <?php echo types_render_field( 'page-content', array( 'raw' =>false) );?>
        </div>
  </div>
  
  <?php get_template_part( 'template-parts/content-child-service-row' ); ?>

<?php if(types_render_field( 'health-safety-section-content', array( 'raw' =>true) )) :?>
  <div class="health-safety-section">
  <div class="health-safety-graphic"></div>
    <div class="width-x-sm">
          <h3><?php echo types_render_field( 'health-safety-section-title', array( 'raw' =>true) );?></h3>
          <?php echo types_render_field( 'health-safety-section-content', array( 'raw' =>false) );?>
        </div>
  </div>

  <?php endif;?>

    <?php get_template_part( 'template-parts/content-projects-gallery' ); ?>


<?php
		}
	}

	?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
