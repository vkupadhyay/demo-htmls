<?php
/**
 * Template Name: Home Page Template
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
?>

<main id="site-content" role="main">
<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();
?>


  <?php get_template_part( 'template-parts/content-homeslider' ); ?>

    <div class="home_info">
        <div class="width-l-md">
            <?php get_template_part( 'template-parts/content-home-services' ); ?>

          
            <div class="home-row-txt">
                <h3><?php echo types_render_field( 'page-title', array( 'raw' =>true) );?></h3>
                <div class="home-txt-row">
                    <?php echo types_render_field( 'page-content', array( 'raw' =>false) );?>
                </div>
            </div>
        </div>
    </div>

    
     <?php get_template_part( 'template-parts/content-home-projects' ); ?>

     <?php get_template_part( 'template-parts/content-home-stats' ); ?>


    <div class="about-multi-section">
        <div class="about-multi-graphic"></div>
        <div class="width-l-md">
            <h2><?php echo types_render_field( 'about-multi-heading', array( 'raw' =>true) );?></h2>
            <div class="about-multi-section-in">
             <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7" style="background: url('<?php echo types_render_field( 'about-multi-team-image', array( 'raw' =>true) );?>')no-repeat center center / cover;">
                    <div class="about-multi-img">

                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <div class="about-multi-txt">
                        <h3><?php echo types_render_field( 'about-multi-title', array( 'raw' =>true) );?></h3>
                        <?php echo types_render_field( 'about-multi-content', array( 'raw' =>false) );?>
                        <div class="read-more"><a href="<?php echo types_render_field( 'read-more-url', array( 'raw' =>true) );?>">READ MORE</a></div>
                    </div>
                </div>
            </div>
            </div>
        </div>

    </div>
<?php
		}
	}

	?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
