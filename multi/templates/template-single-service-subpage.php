<?php
/**
 * Template Name: Service child Template
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
?>

<main id="site-content" role="main">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();
      $featured_image = get_the_post_thumbnail_url($post,'full');

      $post_parent = get_post($post->post_parent);
      $parent_title = '';
      $parent_url= '';
      if($post_parent){
        $parent_title = $post_parent->post_title;
        $parent_url= get_permalink($post_parent);
      }

?>
    
    
  <div class="sub-heading-img" style="background: url('<?php echo $featured_image; ?>')no-repeat center center / cover;">
    <div class="width-l-md">
          <h2><span><?php echo $parent_title;?></span> <?php the_title();?></h2>
        </div>
  </div>

      <div class="center-text-section">
      <div class="width-md">
      <h3><?php echo types_render_field( 'page-title', array( 'raw' =>true) );?></h3>
         <?php echo types_render_field( 'page-content', array( 'raw' =>false) );?>
      </div>
     </div>

<?php if(types_render_field( 'estimating-content', array( 'raw' =>true)) || types_render_field( 'cad-content', array( 'raw' =>true) )){ ?>
      <div class="sm-row-2 bg-dark-row">
        <div class="width-md">

      <?php if(types_render_field( 'estimating-content', array( 'raw' =>true))){ ?> 
          <div class="sm-row-2-box">
           <h3><?php echo types_render_field( 'estimating-heading', array( 'raw' =>true) );?></h3>
           <div class="row">
            <div class="col-sm-4 desktop-hide">
                         <div class="sm-row-2-box-img"><img src="<?php echo types_render_field( 'estimating-image', array( 'raw' =>true) );?>" alt=""></div>
                       </div>
            <div class="col-sm-8">
             <div class="sm-row-2-box-txt">
              <?php echo types_render_field( 'estimating-content', array( 'raw' =>false) );?>
             </div>
            </div>
            <div class="col-sm-4 mobile-hide">
              <div class="sm-row-2-box-img"><img src="<?php echo types_render_field( 'estimating-image', array( 'raw' =>true) );?>" alt=""></div>
            </div>
           </div>
          </div>
        <?php } ?>

       <?php if(types_render_field( 'cad-content', array( 'raw' =>true)) ){ ?> 
          <div class="sm-row-2-box">
           <h3><?php echo types_render_field( 'cad-heading', array( 'raw' =>true) );?></h3>
           <div class="row">
           <div class="col-sm-4">
              <div class="sm-row-2-box-img"><img src="<?php echo types_render_field( 'cad-image', array( 'raw' =>true) );?>" alt=""></div>
            </div>
            <div class="col-sm-8">
             <div class="sm-row-2-box-txt">
              <?php echo types_render_field( 'cad-content', array( 'raw' =>false) );?>
             </div>
            </div>
            
           </div>
          </div>
          <?php } ?>

        </div>
      </div>
     
    <?php } ?>
    
    <?php if(types_render_field( 'working-on-projects-content', array( 'raw' =>true)) || types_render_field( 'what-we-cover-content', array( 'raw' =>true) )){ ?>

      <div class="sm-row-2 ">
        <div class="width-md">

        <?php if(types_render_field( 'working-on-projects-content', array( 'raw' =>true))){ ?> 

          <div class="sm-row-2-box">
           <h3><?php echo types_render_field( 'working-on-projects-heading', array( 'raw' =>true) );?></h3>
           <div class="row">
           <div class="col-sm-4 desktop-hide">
                         <div class="sm-row-2-box-img"><img src="<?php echo types_render_field( 'working-on-projects-image', array( 'raw' =>true) );?>" alt=""></div>
                       </div>
            <div class="col-sm-8">
             <div class="sm-row-2-box-txt">
              <?php echo types_render_field( 'working-on-projects-content', array( 'raw' =>false) );?>
             </div>
            </div>
            <div class="col-sm-4 mobile-hide">
              <div class="sm-row-2-box-img"><img src="<?php echo types_render_field( 'working-on-projects-image', array( 'raw' =>true) );?>" alt=""></div>
            </div>
           </div>
          </div>
      <?php } ?>

      <?php if(types_render_field( 'what-we-cover-content', array( 'raw' =>true))){ ?> 

          <div class="sm-row-2-box">
           <h3><?php echo types_render_field( 'what-we-cover-heading', array( 'raw' =>true) );?></h3>
           <div class="row">
           <div class="col-sm-4">
              <div class="sm-row-2-box-img"><img src="<?php echo types_render_field( 'what-we-cover-image', array( 'raw' =>true) );?>" alt=""></div>
            </div>
            <div class="col-sm-8">
             <div class="sm-row-2-box-txt">
              <?php echo types_render_field( 'what-we-cover-content', array( 'raw' =>false) );?>
             </div>
            </div>
            
           </div>
          </div>
          <?php } ?>
        </div>
      </div>

       <?php } ?>
      <div class="back-row">
       <a  href="<?php echo $parent_url;?>">< BACK TO <?php echo strtoupper($parent_title);?></a>
      </div>
  
  <?php get_template_part( 'template-parts/content-testimonials' ); ?>
  

  <?php
		}
	}

	?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
