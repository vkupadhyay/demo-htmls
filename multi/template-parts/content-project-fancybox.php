<?php 
  $projects_images = get_post_meta( get_the_ID(),'wpcf-project-gallery-images');
?>
<?php if($projects_images){ ?>
 <div class="project-thumb">
      <div class="width-l-md">
        <div class="project-thumb-slider">

          <?php foreach($projects_images as $index=>$img) { ?>
            <?php 
              $thumb_url = $img; 
                $full_url = $img; 
                $imageID = dp_theme_get_image_id($thumb_url);
                      
               $image_thumb_url = wp_get_attachment_image_url($imageID, 'project-detail-gallery');
                
                if($image_thumb_url){ 
                $thumb_url = $image_thumb_url;     
                }
                      ?>
            <?php if($img){ ?>
              <div>
               <div class="project-thumb-items">
              <a href="<?php echo $full_url;?>" rel="group" data-fancybox="gallery-project" class="map-fancybox" ><img src="<?php echo $thumb_url;?>" />
              </a>
               </div>
             </div>
               <?php } ?>
          <?php } ?>    
          </div>
      </div>
</div>
 <?php } ?> 