<?php
  $our_partners =get_post_meta(get_the_ID(),'wpcf-our-partners-icons'); 
  $our_partners =array_filter($our_partners);
?>
<?php if($our_partners && (
  types_render_field( 'our-partners-title', array( 'raw' =>true) )
   || types_render_field( 'our-partners-content', array( 'raw' =>true))
   )){ ?>

<div class="our-partners-section">
    <div class="width-l-md">
       <h2><?php echo types_render_field( 'our-partners-title', array( 'raw' =>true) );?></h2>
        <div class="our-partners-btm">
          <?php echo types_render_field( 'our-partners-content', array( 'raw' =>false) );?>
          <div class="our-partners-row-ok">
              <div class="row">
                  <?php foreach($our_partners as $index=>$partner_logo) { ?>
                    <?php if($partner_logo){ ?>
                  <div class="col-sm-3" style="width: 20%;flex:0 0 20%;">
                      <div class="our-partners-logo"><img src="<?php echo $partner_logo;?>" alt="" /></div>
                  </div>
                  <?php } ?>
                 <?php } ?>
              </div>
          </div>
        </div>
    </div>
</div>
 <?php } ?> 