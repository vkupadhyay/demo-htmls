<?php 
  $post_meta =get_post_meta(get_the_ID()); 

$stats_title=  isset($post_meta['wpcf-stats-title'][0])?($post_meta['wpcf-stats-title']):array(); 
$stats_count=  isset($post_meta['wpcf-stats-count'][0])?($post_meta['wpcf-stats-count']):array(); 
$stats_count_suffix=  isset($post_meta['wpcf-stats-count-suffix'][0])?($post_meta['wpcf-stats-count-suffix']):array(); 
$stats_images = isset($post_meta['wpcf-stats-icon'][0])?($post_meta['wpcf-stats-icon']):array();

?>

<div class="project-info" id="stats_wrapper">
    <div class="width-l-md">
      <div class="row">

        <?php foreach($stats_images as $index=>$icon) { ?>
          <?php if($icon){ ?>
          <?php $stats_count_text =  isset($stats_count[$index])?$stats_count[$index]:'';?>
          <?php $stats_count_suffix_text =  isset($stats_count_suffix[$index])?$stats_count_suffix[$index]:'';?>
          <?php $stats_title_text=  isset($stats_title[$index])?$stats_title[$index]:'';?>
        <div class="col-lg-3 col-md-3 col-sm-3">
          <div class="project-info-box">
           <i><img src="<?php echo $icon;?>" alt="" /></i>
           <strong class="run-counter" data-count="<?php echo $stats_count_text;?>" data-count-suffix="<?php echo $stats_count_suffix_text;?>"><?php echo $stats_count_text;?><?php echo $stats_count_suffix_text;?></strong>
           <span><?php echo $stats_title_text;?></span>
          </div>
        </div>
        <?php } ?>
        <?php } ?>
        
      </div>
    </div>
    </div>
    <script type="text/javascript">
      jQuery(document).ready(function(){

      //reloadStats();
 
      function reloadStats(){
               jQuery('.run-counter').each(function () {

                if(!jQuery(this).hasClass('processing')){
                 jQuery(this).removeClass('processing');
               

                var count = jQuery(this).attr('data-count');
                var count_suffix = jQuery(this).attr('data-count-suffix');
                var currObj = jQuery(this);
                  jQuery(this).prop('Counter',0).animate({
                      Counter:count
                      }, {
                      duration: 4000,
                      easing: 'swing',
                      step: function (now) {
                      jQuery(this).text(Math.ceil(now) +count_suffix);
                      jQuery(this).removeClass('processing');
                      }
                      });   
                   }
                });

        }
var a = 0;
      $(document).on('scroll', function() {
            
            var oTop=  $('#stats_wrapper').offset().top - window.innerHeight 
                if (a == 0 && $(window).scrollTop() > oTop) {
                  reloadStats();
                }
               
           });
 
      });

    
    </script>