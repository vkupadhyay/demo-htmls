   <?php if (have_rows('content-row')) : $postcount = 0;?>
       <div class="row-2-section">
           <?php while (have_rows('content-row')) : the_row();

                // Load sub field value.
                $content_title = get_sub_field('content-row-title');
                $content_content = get_sub_field('content-row-content');
                $content_image = get_sub_field('content-row-image');
                $content_read_more_title = get_sub_field('content-row-read-more-title');
                $content_read_more_link = get_sub_field('content-row-read-more-link');
                $content_image_url =isset($content_image['service-content-row'])?$content_image['service-content-row']:$content_image['url'];

            ?>
               <?php if ($postcount++ % 2 == 0) { ?>
                   <div class="lefttext-rightimg text-with-img">
                       <div class="object-wrap-img-right object-bg-image" style="background-image: url(<?php echo $content_image_url; ?>)"> </div>
                       <div class="width-l-md">
                           <div class="row v-align min-height-500">
                               <div class="col-sm-5 col-md-5 col-lg-5">
                                   <h2><?php echo $content_title; ?></h2>
                                   <?php echo $content_content; ?>

                                   <?php if ($content_read_more_title && $content_read_more_link) : ?>
                                       <div class="green-btn"> <a style="text-transform: uppercase;" href="<?php echo $content_read_more_link; ?>"><?php echo $content_read_more_title; ?></a> </div>
                                   <?php endif; ?>
                               </div>
                           </div>
                       </div>
                   </div>
               <?php } else { ?>

                   <div class="lefttext-leftimg text-with-img">
                       <div class="object-wrap-img-left object-bg-image" style="background-image: url(<?php echo $content_image_url; ?>"></div>
                       <div class="width-l-md">
                           <div class="row v-align min-height-500">
                               <div class="col-sm-7 col-md-7 col-lg-7"></div>
                               <div class="col-sm-5 col-md-5 col-lg-5">
                                   <h2><?php echo $content_title; ?></h2>
                                   <?php echo $content_content; ?>

                                   <?php if ($content_read_more_title && $content_read_more_link) : ?>
                                       <div class="green-btn"> <a style="text-transform: uppercase;" href="<?php echo $content_read_more_link; ?>"><?php echo $content_read_more_title; ?></a> </div>
                                   <?php endif; ?>

                               </div>
                           </div>
                       </div>
                   </div>
               <?php } ?>

           <?php endwhile; ?>
       </div>
   <?php endif;  ?>