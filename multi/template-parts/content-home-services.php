<?php

// Check rows exists.
if (have_rows('service-list-row')) :
?>

  <div class="home-row-4">
    <div class="row">
      <?php
      // Loop through rows.
      while (have_rows('service-list-row')) : the_row();
        // Load sub field value.
        $service_icon = get_sub_field('service-list-row-icon');
        $service_title = get_sub_field('service-list-row-title');
        $service_read_more_link = get_sub_field('service-list-row-read-more-link');
      ?>
        <div class="col-lg-3 col-md-3 col-sm-3">
          <div class="home-info-box">
            <div class="home-info-icon"><img src="<?php echo $service_icon['url']; ?>" width="100%" alt="" /></div>
            <h2><?php echo $service_title; ?></h2>
            <?php if ($service_read_more_link) : ?>
              <div class="green-btn">
                <a href="<?php echo $service_read_more_link; ?>">FIND OUT MORE</a>
              </div>
            <?php endif; ?>
          </div>
        </div>
      <?php
      // End loop.
      endwhile;
      ?>
    </div>
  </div>
<?php endif; ?>