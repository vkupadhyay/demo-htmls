<?php if (have_rows('service-list-row')) : ?>
    <div class="service__page__list">
        <?php while (have_rows('service-list-row')) : the_row();

         // Load sub field value.
         $service_icon = get_sub_field('service-list-row-icon');
         $service_title = get_sub_field('service-list-row-title');
         $service_content = get_sub_field('service-list-row-content');
         $service_read_more_link = get_sub_field('service-list-row-read-more-link');
         $service_read_more_title = get_sub_field('service-list-row-read-more-title');

        ?>

            <div class="service__page__row">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="service__page__icon">
                            <img src="<?php echo $service_icon['url']; ?>" alt="" />
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-98 col-sm-9">
                        <div class="service__page__text">
                            <h3><?php echo $service_title; ?></h3>
                            <?php echo $service_content; ?>
                            <div class="green-btn"><a style="text-transform: uppercase;" href="<?php echo $service_read_more_link; ?>"><?php echo $service_read_more_title;?></a></div>
                        </div>
                    </div>
                </div>
            </div>

        <?php endwhile; ?>
    </div>
<?php endif; ?>