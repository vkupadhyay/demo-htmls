<div class="client-logo">
        <div class="width-l-md">
          <ul>

            <?php

              $footer_logos = get_post_meta(82,'wpcf-footer-logo');
              foreach( $footer_logos as $flogo) {
              ?>
              <li><img src="<?php echo $flogo;?>" alt=""/></li>
             <?php }
            ?>
           
        </ul>
        </div>
    </div>