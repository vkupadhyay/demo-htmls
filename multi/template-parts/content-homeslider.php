
<?php 
$childargs = array(
		'post_type' => 'slider',
		'posts_per_page' => -1,
		'post_status' => 'publish',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		);

		$child_posts = get_posts($childargs);
		?>
		<?php if($child_posts) { ?>
			<div class="home-slider">

		<?php foreach($child_posts as $slide_item){ ?> 

			<div class="home-slider-item">
          <div class="home-slider-out" style="background: url('<?php echo get_the_post_thumbnail_url($slide_item); ?>')no-repeat center center / cover;">
               <div class="width-l-md">
                   <h2>
                   	<span><?php echo types_render_field( 'slide-title-1', array( 'raw' =>true, 'id' =>$slide_item->ID) );?></span> 
                   	<?php echo types_render_field( 'slide-title-2', array( 'raw' =>true, 'id' =>$slide_item->ID) );?></h2>
                   <h1><?php echo types_render_field( 'slide-title-3', array( 'raw' =>true, 'id' =>$slide_item->ID) );?></h1>
               </div>
          </div>

          <!-- //<img src="<?php echo get_the_post_thumbnail_url($slide_item); ?>" width="100%" alt="" /> -->
         </div>

		<?php } ?>
	</div>
	<?php } ?>