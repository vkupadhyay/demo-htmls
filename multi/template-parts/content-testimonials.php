<?php

$args = array(
    'post_type'      => 'testimonial',
    'posts_per_page' => -1,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
 );


$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : $postcount = 0;?>

 <div class="testimonials-section">
  <div class="testimonials-graphic"></div>
     <div class="width-md">
       <h2>TESTIMONIALS</h2>
       <div class="testimonials-slider">

        <?php while ( $parent->have_posts() ) : $parent->the_post(); 
      ?>
     
      <div class="testimonials-slider-item">
          <strong><?php echo types_render_field( 'testimonial-author', array( 'raw' =>true) );?></strong>
          <span><?php echo types_render_field( 'testimonial-company', array( 'raw' =>true) );?></span>
          <?php the_content();?>
       </div>
    

    <?php endwhile; ?>
         
       </div>
     </div>
  </div>

  <?php endif; wp_reset_postdata(); ?>    

  