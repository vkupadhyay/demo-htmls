<?php 
$childargs = array(
		'post_type' => 'project',
		'posts_per_page' => 10,
		'post_status' => 'publish',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		//'meta_query' => array(array('key' => '_wpcf_belongs_service_id', 'value' => get_the_ID()))
		);

    $category_id = get_field('related_project_category');
    if($category_id){
      $childargs['cat'] =$category_id;
    }
		$child_posts = get_posts($childargs);


		?>
		<?php if($child_posts) { ?>
      <div class="recent-project-list">
      <h2>PROJECTS</h2>
      <div class="project-list-slider">

          <?php foreach($child_posts as $project_item){ ?> 
          	<?php $projectURL = get_permalink($project_item);?>
 <div>
        <div class="pro-slider-item">
              <div class="pro-slider-item-img">
              	<a href="<?php echo $projectURL;?>">
              		<img src="<?php echo types_render_field( 'project-thumb', array( 'raw' =>true,'id'=>$project_item->ID) );?>" alt="">
              	</a>
              </div>
              <div class="pro-slider-item-btm">
               <h4>
                <?php echo $project_item->post_title; ?>
              </h4>
            <?php echo types_render_field( 'project-short-description', array( 'raw' =>false,'id'=>$project_item->ID) );?>
            <div class="green-btn"><a href="<?php echo $projectURL;?>">FIND OUT MORE</a></div>
          </div>
            </div>
      </div>
    <?php } ?>

         
        </div>
  </div>		
		<?php } ?>