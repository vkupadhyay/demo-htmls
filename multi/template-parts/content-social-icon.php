<?php
$contact_id = 34;
?>
<div class="social-icons-box">
<a href="<?php echo types_render_field( 'linked-in', array( 'raw' =>true,'id'=>$contact_id) );?>"><img src="<?php echo get_stylesheet_directory_uri();?>/images/multi-icon-social-in.png" alt="" /></a>
<a href="mailto:<?php echo types_render_field( 'contact-email', array( 'raw' =>true,'id'=>$contact_id) );?>"><img src="<?php echo get_stylesheet_directory_uri();?>/images/multi-icon-amail.png" alt="" /></a>
<a href="tel:<?php echo types_render_field( 'contact-telephone', array( 'raw' =>true,'id'=>$contact_id) );?>"><img src="<?php echo get_stylesheet_directory_uri();?>/images/multi-icon-tel.png" alt="" /></a>
</div>