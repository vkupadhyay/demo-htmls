<?php

$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => 27,//service page ID
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
 );


$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : $postcount = 0;?>

   <div class="home-row-4">
      <div class="row">

    <?php while ( $parent->have_posts() ) : $parent->the_post(); 
      $service_sub_url = get_permalink();
      ?>
     
      <div class="col-lg-3 col-md-3 col-sm-3">
         <div class="home-info-box">
             <div class="home-info-icon"><img src="<?php echo types_render_field( 'service-icon', array( 'raw' =>true) );?>" width="100%" alt="" /></div>
             <h2><?php echo types_render_field( 'service-title', array( 'raw' =>true) );?></h2>
             <div class="green-btn">
                 <a href="<?php echo $service_sub_url;?>">FIND OUT MORE</a>
             </div>
         </div>
      </div>
<?php endwhile; ?>
 </div>
  </div>
<?php endif; wp_reset_postdata(); ?>    
                
               
             