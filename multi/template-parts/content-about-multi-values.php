<?php 
  $post_meta =get_post_meta(get_the_ID()); 

$mutli_values_title=  isset($post_meta['wpcf-stats-title'][0])?($post_meta['wpcf-stats-title']):array(); 
$mutli_values_count=  isset($post_meta['wpcf-stats-count'][0])?($post_meta['wpcf-stats-count']):array();  
$mutli_values_count_suffix=  isset($post_meta['wpcf-stats-count-suffix'][0])?($post_meta['wpcf-stats-count-suffix']):array(); 
$mutli_values_images = isset($post_meta['wpcf-stats-icon'][0])?($post_meta['wpcf-stats-icon']):array();

?>
<?php if($mutli_values_images && (types_render_field( 'multi-values-heading', array( 'raw' =>true) ) || types_render_field( 'multi-values-content', array( 'raw' =>true) ))){ ?>
<div class="multi-values-section">
	<div class="width-l-md">
	    <h2><?php echo types_render_field( 'multi-values-heading', array( 'raw' =>true) );?></h2>
	    <?php echo types_render_field( 'multi-values-content', array( 'raw' =>false) );?>
	    <div class="multi-values-row">
	        <div class="row">

	        <?php foreach($mutli_values_images as $index=>$icon) { ?>
	          <?php if($icon){ ?>
	          <?php $stats_count_text =  isset($mutli_values_count[$index])?$mutli_values_count[$index]:'';?>
	          <?php $stats_count_suffix_text =  isset($mutli_values_count_suffix[$index])?$mutli_values_count_suffix[$index]:'';?>
	          <?php $stats_title_text=  isset($mutli_values_title[$index])?$mutli_values_title[$index]:'';?>
	            <div class="col-sm-3">
	                <div class="multi-values-box">
	                    <div class="multi-values-img"><img src="<?php echo $icon;?>" width="100%" alt="" /></div>
	                    <h4><?php echo $stats_count_text;?> <?php echo $stats_count_suffix_text;?></h4>
	                    <span><?php echo $stats_title_text;?></span>
	                </div>
	            </div>
	             <?php } ?>
	        <?php } ?>    
	        </div>
	    </div>
	</div>
</div>
 <?php } ?> 