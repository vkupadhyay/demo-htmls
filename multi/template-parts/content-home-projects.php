<?php 
$childargs = array(
    'post_type' => 'project',
    'posts_per_page' => 4,
    'post_status' => 'publish',
    'orderby' => 'menu_order',
     'category_name' => 'home-projects  ',
    'order' => 'ASC',
    //'meta_query' => array(array('key' => '_wpcf_belongs_service_id', 'value' => get_the_ID()))
    );

    $child_posts = get_posts($childargs);
    ?>
    <?php if($child_posts) { ?>

<div class="home-prject-section">
    <div class="home-prject-graphic"></div>
      <div class="width-l-md">
        <h2>PROJECTS</h2>
        <div class="home-prject-slider">
       <div class="prject-slider">

          <?php foreach($child_posts as $project_item){ ?> 
            <?php $project_url = get_permalink($project_item);?>
         <div class="home-prject-item">
          <div class="row">
          <div class="col-lg-5 col-md-5 col-sm-5">
           <div class="home-prject-item-img">
            <a href="<?php echo $project_url;?>">
              <img src="<?php echo types_render_field( 'project-thumb', array( 'raw' =>true,'id'=>$project_item->ID) );?>" alt="" />
            </a>
          </div>
          </div>
          <div class="col-lg-7 col-md-7 col-sm-7">
           <div class="home-prject-item-text">
            <h3>
              <?php echo $project_item->post_title;?>
          </h3>
        <?php echo types_render_field( 'project-short-description', array( 'raw' =>false,'id'=>$project_item->ID) );?>
        <div class="read-more">
          <a href="<?php echo $project_url;?>">READ THE STORY</a>
        </div>
           </div>
          </div>
          </div>
         </div>

 <?php } ?>

       </div>
        </div>
      </div>
    </div>

<?php } ?>    