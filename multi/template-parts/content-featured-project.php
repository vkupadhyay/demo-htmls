
<?php 
$featued_args = array(
    'post_type' => 'project',
    'numberposts' => 1,
    'category_name' => 'featured-project',
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
    );

    $featured_posts = get_posts($featued_args);
    ?>
    <?php if($featured_posts) { ?>
      <div class="project-row-top">

          <?php foreach($featured_posts as $project_item){ ?> 
            <?php $projectURL = get_permalink($project_item);?>


              <div class="row">
                   <div class="col-sm-5">
                       <div class="project-row-top-txt">
                           <h3>
                              <?php echo $project_item->post_title; ?>
                           </h3>
                          <?php echo types_render_field( 'project-short-description', array( 'raw' =>false,'id'=>$project_item->ID) );?>
                           <div class="green-btn"><a href="<?php echo $projectURL;?>">READ STORY</a> </div>
                       </div>
                   </div>
                   <div class="col-sm-7">
                       <div class="project-row-top-img">
                        <img src="<?php echo  get_the_post_thumbnail_url($project_item,'full');?>"  />
                      </div>
                   </div>
               </div>
          <?php } ?>
      </div>    
    <?php } ?>