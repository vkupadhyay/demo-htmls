$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 1200) {
        $(".fixedbar").addClass("bluebackbg");
    } else {
        $(".fixedbar").removeClass("bluebackbg");
    }
});

$(document).ready(function(){
    var sections = $('section');
    var navLinks = $('#navbar .nav-link');

    $(window).on('scroll', function() {
        var currentScroll = $(this).scrollTop() + 60; // Offset to account for fixed nav
        var currentSection;

        sections.each(function() {
            var sectionTop = $(this).offset().top;

            if (currentScroll >= sectionTop) {
                currentSection = $(this);
            }
        });

        var id = currentSection && currentSection.length ? currentSection.attr('id') : '';

        navLinks.removeClass('active');
        if (id) {
            $('#navbar .nav-link[href="#' + id + '"]').addClass('active');
        }
    });

    navLinks.on('click', function(e) {
        e.preventDefault();
        var targetId = $(this).attr('href');
        var targetPosition = $(targetId).offset().top - 50; // Adjust offset as needed

        $('html, body').animate({
            scrollTop: targetPosition
        }, 500);
    });
});

$('#navclick').click(function(){
    $(mainContainer).toggleClass('menu-slide');
    $(clickmenu).toggleClass('menu-slide');
});

$(".button").click(() => {
    $("selector").addClass("popshow");
});

$('.testimonials-slider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 500,
    fade: true,
    cssEase: 'linear'

});



AOS.init();