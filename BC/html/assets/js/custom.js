$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 300) {
        $(".main-header").addClass("sm-header");
    } else {
        $(".main-header").removeClass("sm-header");
    }
});



$('#navclick').click(function(){
    $(mainContainer).toggleClass('menu-slide');
    $(clickmenu).toggleClass('menu-slide');
});


$('.heroslider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 500,
    fade: true,
    cssEase: 'linear'

});



AOS.init();