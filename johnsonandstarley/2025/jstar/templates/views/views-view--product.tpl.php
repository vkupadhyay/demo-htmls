<div class="dk-grey bottom">
  <div class="<?php print $classes; ?> row">
    <div class="large-8 large-centered columns">

    	<div id="categories" class="product-box product-list">
    
	      <?php if ($rows): ?>
	        <?php print $rows; ?>
	      <?php elseif ($empty): ?>
	        <?php print $empty; ?>
	      <?php endif; ?>

	    </div>

    </div>
  </div>
</div>