(function ($) {

	// News only

    var // General purpose vars
        htmlBody = $('html,body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),
        befFilters = function(){
           // var filters = $('#views-exposed-form-downloads-listing').find('.bef-tree'),
            var filters = $('#views-exposed-form-downloads-listing').addClass('ctools-auto-submit-full-form').find('.bef-nested > ul'),
                levelone = filters.children(),
                numlevelone = levelone.length,
                getCatTid = function(sParam){
                    var sPageURL = window.location.search.substring(1),
                        sURLVariables = sPageURL.split('&');

                    for (var i = 0; i < sURLVariables.length; i++){
                        var sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] == sParam)
                        {
                            return sParameterName[1];
                        }
                    }
                },
                currentCatTid = getCatTid('category'),
                currentCat = filters.find('input[value=' + currentCatTid + ']').parent();
                filters.addClass('bef-tree');
                $('.js-form-type-radio input[type=radio]').addClass('form-radio');

            for (var x=0;x<numlevelone;x++){
                var This = $(levelone[x]);

                This.children('ul').addClass('bef-tree-child');

                if (This.children('.bef-tree-child').length > 0){
                    This.addClass('hasChild').children('.form-item').append('<span class="toggle"><span>></span></span>');
                }
            }

            filters.on('click', '.toggle', function(){
                $(this).toggleClass('active').parent().siblings('.bef-tree-child').slideToggle();

                return false;
            });

            if (currentCat.length > 0){
                currentCat.addClass('active').siblings('.bef-tree-child').show();

                currentCat.parents('.hasChild').addClass('open-me').children('.form-item').find('.toggle').addClass('active');
            } else {
                
                var defaultTid = 24;
                var defaultTid = 0;
                // filters.find('input[value=' + defaultTid + ']').parent().addClass('active').siblings('.bef-tree-child').show();
                // filters.find('input[value=' + defaultTid + ']').siblings('.toggle').addClass('active');
            }

            var title = $('.form-item.active').children('label').text(),
                parentTitle = $('.hasChild.open-me').children('.form-item').children('label').text(),
                target = $('#js-title');
            console.log(title);

            title =title.replace('- Any -','');
            if (parentTitle.length > 0){
                if (title == parentTitle){
                    target.append('<h2>' + parentTitle + '</h2>');
                } else {
                    target.append('<h2 class="bold">' + parentTitle + '</h2>');
                    target.append('<h3>' + title + '</h3>');
                }
            } else {
                target.append('<h2>' + title + '</h2>');
            }
        },

        // ---- 
        // Prep functions to run
        // ---- 

        Ready = function(){
            befFilters();
        };


    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

    Window.on('load', function(){
        $('body').trigger('click');
    });

})(jQuery);