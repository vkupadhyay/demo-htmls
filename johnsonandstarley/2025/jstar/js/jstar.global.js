(function ($) {

    /*
        1. Set everything up as variables, e.g:
     
            myFunction = function(){
                var saveEverythingAsVariables = $('#everything');

                ... jquery in here
            }
     
        2. Then add function to required launcher, e.g:

            Ready = function(){
                myfunction();
            }
    */

    var 
        // General purpose vars
        htmlBody = $('html,body'),
        Body = $('body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),

        // Make all external links open in new window
        externalLinks = function(){
            var extLink = Wrapper.find($('a[href^="http"],a[href^="https"]'));
            extLink.attr('target','_blank');
        },

        // Ajax page loading
        ajaxLoading = function(){
            if ($.support.pjax) {                
                Document.pjax('a:not(a[href^="http"],a[href^="mailto"],a[target="_blank"], #admin-menu-account a)', '#wrapper');

                Document.on('pjax:clicked', function() {
                    Body.addClass('leaving');
                    htmlBody.velocity("scroll", { duration: 350, easing: "ease-in-out" });
                });
            }
        },

        headerScroll = function(){
            var header = $('#header');
          
            if (Window.scrollTop() > 1){  
                header.addClass("scrolled");
            }
            else{
                header.removeClass("scrolled");
            }
        },

        // Smooth scrolling
        smoothScroll = function(){
            Wrapper.on('click', '.scroll', function() {
                var $target = $(this.hash);
                $target.velocity("scroll", { duration: 300, offset: -60, easing: "ease-in-out" });
                return false;
            });
        },

        banners = function(){
            var banner = $('#js-banner'),
                setTri = function(){
                    var banH = banner.height();

                    if (banH > 500) {
                        var mod = banH - 500,
                            bl = 550 + mod,
                            bb = 500 + mod; 

                        $('<style>#js-banner:before{border-left-width:'+bb+'px}</style>').appendTo('head');
                        $('<style>#js-banner:before{border-bottom-width:'+bl+'px}</style>').appendTo('head');
                    } else {
                        var bl = 550,
                            bb = 500; 
                        $('<style>#js-banner:before{border-left-width:'+bb+'px}</style>').appendTo('head');
                        $('<style>#js-banner:before{border-bottom-width:'+bl+'px}</style>').appendTo('head');
                    }
                };

            setTri();

            Window.on('resize', function(){
                setTimeout(function(){
                    setTri();
                },200);
            });
        },

        registerForm = function(){
            var form = $('#webform-submission-register-your-appliance-node-19-add-form'),
                btn = form.find('.add-form'),
                fieldset = form.find('#edit-flexbox-02').hide();

            btn.on('click', function(){
                fieldset.slideDown(250);

                btn.parent().parent().slideUp();

                return false;
            });
        },

        modelContact = function(){
            var block = $('#block-bean-model-link-contact');

            block.addClass('clearfix');
        },

        cats = function(){
            var categories = $('#categories').find('ul.categories'),
                numCats = categories.children('li').length;

            if (numCats == 1){
                categories.find('li').addClass('featured');
            } else if (numCats == 2) {
                categories.addClass('two-col');
            }
        },

        modelH = function(){
            var setH = $('div.setHeight');

            setH.matchHeight();
        },

        mobileNav = function(){
            var menuToggle = $('#navToggle'),
              //  nav = $('#block-superfish-1'),
                nav = $('#block-mainmenu'),
                tradeNav = $('#block-menu-menu-trade-menu'),
                tradeNav = $('#block-trademenu'),
                secondaryL = $('#js-secondary'),
                header = $('#header'),
                headerCon = $('#js-headerContent'),
                mobHeader = $('#js-mobHeader'),
                status = 0,
                intiMobH = function(){
                    if (Window.width() < 1025){
                        if (status == 0){
                            nav.detach();
                            tradeNav.detach();
                            secondaryL.detach();

                            mobHeader.append(nav).append(tradeNav).append(secondaryL);

                            status = 1;
                        }
                    } else {
                        if (status == 1){
                            nav.detach();
                            tradeNav.detach();
                            secondaryL.detach();

                            headerCon.append(secondaryL).append(nav).append(tradeNav);

                            header.removeClass('active');
                            mobHeader.hide();
                            menuToggle.removeClass('active');

                            status = 0;
                        }
                    }
                };
                
            menuToggle.on('click', function(){
                mobHeader.slideToggle(250);
                $(this).toggleClass('active');
                header.toggleClass('active');
                return false;
            });

            intiMobH();

            Window.on('resize', function(){
                setTimeout(function(){
                    intiMobH();
                }, 200);
            });

            $('#superfish-main a.sf-with-ul').on('click',function(e){
                
                if(menuToggle.hasClass('active')){
                    e.preventDefault();
                    e.stopPropagation();
                    if(!$(this).hasClass('clicked')){
                        $(this).addClass('clicked');

                    }else{
                        window.location.href = $(this).attr('href');
                    }

                }
            });

        },

        cookies = function(){
            var notice = $('#cookie-notice'),
                btn = $('#pp-close'),
                cookie = Cookies.get('cookieNotice');

            btn.on('click', function(){
                Cookies.set('cookieNotice','showCookieNotice',{ expires: 30 });
                notice.hide();
                return false;
            });

            if (cookie == null){
                notice.show();
            }
        },

        removeBlankView = function(){

            $('.block-views-blockproduct-download-pdf-pdf-row').each(function(){
                if(jQuery(this).find('.pdf-download-box').length ==0){
                    $(this).addClass('hidden');
                }
            });

        }

        // ---- 
        // Prep functions to run
        // ----

        Ready = function(){
            cookies();
            externalLinks();
           // ajaxLoading();
            headerScroll();
            smoothScroll();
            mobileNav();
            banners();
            registerForm();
            modelContact();
            cats();
            modelH();
            removeBlankView();
        };

    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();

        $('#block-jstar-jstar-system-main').parents('.large-12.columns').addClass('root-columns').removeClass('columns');
    });

    Window.on('scroll', function(){
        headerScroll();
    });

})(jQuery);