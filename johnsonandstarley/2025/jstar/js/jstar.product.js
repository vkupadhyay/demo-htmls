(function ($) {

	// Homepage only

    var activeClass = function(){
        	let switchNavMenuItem = (menuItems) => {

			    var current = location.pathname

			    $.each(menuItems, (index, item) => {

			        $(item).removeClass('active')

			        if ((current.includes($(item).attr('href')) && $(item).attr('href') !== "/") || ($(item).attr('href') === "/" && current === "/")){
			            $(item).addClass('active')
			        }
			    })
			}

			switchNavMenuItem($('#superfish-1 li a'));
        },

        // ---- 
        // Prep functions to run
        // ---- 

        Ready = function(){
            activeClass();
        };


    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

})(jQuery);