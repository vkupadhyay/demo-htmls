/*
    Pippip v2 Gulpfile
*/

let gulp = require("gulp"),
  sass = require("gulp-sass"),
  autoprefixer = require("gulp-autoprefixer"),
  sourcemaps = require("gulp-sourcemaps"),
  plumber = require("gulp-plumber"),
  gutil = require("gulp-util"),
  paths = {
    sass: ["./sass/*.scss"],
    dist: ["./"]
  };

/*
 * Task - SASS
 */

gulp.task("sass", function() {
  gulp
    .src(paths.sass)
    .pipe(
      plumber(function(error) {
        // Output any errors without breaking stream
        gutil.log(error.message);
        this.emit("end");
      })
    )
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "compressed" }))
    .pipe(
      autoprefixer({
        browsers: ["last 3 versions", "ie 10", "iOS 8"]
      })
    )
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(paths.dist + "css"));
});

gulp.task("watch", function() {
  gulp.watch(paths.sass, ["sass"]);
});
