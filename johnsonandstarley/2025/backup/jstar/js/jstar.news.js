(function ($) {

	// News only

    var // General purpose vars
        htmlBody = $('html,body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),

        hideNews = function(){
            var news = $('#news').find('.listing'),
                btn = $('#show-all'),
                state = 0;

            btn.on('click', function(){
                var This = $(this);

                if (state == 0){
                    This.text('Show less news >');

                    news.addClass('see-all');

                    state = 1;
                } else {
                    This.text('Show all news >');

                    news.removeClass('see-all');

                    state = 0;
                }

                return false;
            });
        },

        // ---- 
        // Prep functions to run
        // ---- 

        Ready = function(){
            hideNews();
        };


    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

})(jQuery);