(function ($) {

	// Homepage only

    var // General purpose vars
        htmlBody = $('html,body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),

        bannerH = function(){
            var banner = Wrapper.find('.banner'),
                header = $('#header'),
                resize = function(){
                    var multiplier = 0;

                    if (Window.width() < 1500){
                        multiplier = 100;
                    } else {
                        multiplier = 350;
                    }

                    var winH = (Window.height() - header.height() - multiplier) + 'px';

                    banner.css({'max-height':winH});
                };

            resize();

            Window.on('resize', function(){
                setTimeout(function(){
                    resize();
                },200);
            });
        },

        // ---- 
        // Prep functions to run
        // ---- 

        Ready = function(){
            bannerH();
        };


    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

})(jQuery);