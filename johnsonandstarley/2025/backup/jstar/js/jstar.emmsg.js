(function ($) {

	// Emergency Message
    var emergMessage = $('#emMessage'),

        hideMessage = function(){
            var hideMsg = $('#emClose');

            hideMsg.on('click', function(){
                emergMessage.velocity("fadeOut", { duration: 500 });
                Cookies.set('emClosed','hideThatJazz',{ expires: 7 });
                return false;
            });
        },

        cookies = function(){
            var cookie = Cookies.get('emClosed');

            if (cookie == 'hideThatJazz'){
                emergMessage.remove();
            } else {
                emergMessage.show();
            }
        };

        // ---- 
        // Prep functions to run
        // ---- 

        Ready = function(){
            hideMessage();
            cookies();
        };


    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

})(jQuery);