<?php
function jstar_form_system_theme_settings_alter(&$form, &$form_state) {

  // Custom options
  $form['custom_options'] = array(
  	'#type' => 'fieldset',
  	'#title' => t('Custom Options'),
    '#description' => t('See jstar.plugins.js for plugin usage & options'),
  );

  // Swipebox Support
  $form['custom_options']['swipebox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Swipebox'),
    '#description' => t('Enable swipebox across site (usage: .swipebox on element to be opened)'),
    '#default_value' => theme_get_setting('swipebox'),
  );

  // Slick.js Support
  $form['custom_options']['slick'] = array(
    '#type' => 'checkbox',
    '#title' => t('Slick'),
    '#description' => t('Enable slick.js carousel (usage: .slick as wrapper)'),
    '#default_value' => theme_get_setting('slick'),
  );

  // MatchHeight.js Support
  $form['custom_options']['matchHeight'] = array(
    '#type' => 'checkbox',
    '#title' => t('MatchHeight'),
    '#description' => t('Enable matchHeight.js for equalising element heights (usage: .matchHeight on element to be affected)'),
    '#default_value' => theme_get_setting('matchHeight'),
  );

  // Slick.js Support
  $form['custom_options']['overlay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Overlay'),
    '#description' => t('Enable custom overlay template (see jstar.overlay.js for usage)'),
    '#default_value' => theme_get_setting('overlay'),
  );

  // get current year for copyright
  $year = date('Y');

  // Copyright text
  $form['custom_options']['copyright'] = array(
    '#type' => 'textfield',
    '#title' => t('Copyright text'),
    '#description' => t('Copyright text to be displayed on the site. It will be prepended by <em>&copy; '.$year.' ...</em>. See page.tpl.php for usage'),
    '#default_value' => theme_get_setting('copyright'),
  );

}