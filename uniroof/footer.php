<!-- <div class="contact_us_section">
  <div class="width_mid">
    <h1>CONTACT US</h1>
    <div class="contact_us_txt">
      <?php //dynamic_sidebar('sidebar-2');
      ?>
    </div>
  </div>
</div> -->

<div class="contact_us_section">
  <div class="row">
    <div class="col-md-6">
      <div class="flag_img"><img src="https://www.uniroof.com/wp-content/themes/uniroof-theme/images/flag-1.jpg" alt=""></div>
      <div class="footer_info">
        <h1>CONTACT USA</h1>
        <div class="contact_us_txt">
          <?php dynamic_sidebar('sidebar-2');
          ?>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="flag_img"><img src="https://www.uniroof.com/wp-content/themes/uniroof-theme/images/flag-2.jpg" alt=""></div>
      <div class="footer_info">
        <h1>CONTACT UK</h1>
        <div class="contact_us_txt">
          <?php dynamic_sidebar('sidebar-2');
          ?>
        </div>
      </div>
    </div>
  </div>
</div>


</div><!-- #content -->

<footer class="footer_box" id="colophon" role="contentinfo">
  <div class="container clearfix text-center">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Uniroof-footera-landscape-logo.png" alt="">
    <?php //dynamic_sidebar('sidebar-3'); 
    ?>
  </div>
</footer>

</div><!-- .site-content-contain -->
</div><!-- #page -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.matchHeight.js"></script>
<script>
  new WOW().init();
</script>
<script type="text/javascript">
  jQuery(function($) {

    $('.p_txt').matchHeight();
    $(".latest-action-slider").slick({
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1
    });

    var header = $(".home_hdr");
    $(window).scroll(function() {
      var scroll = $(window).scrollTop();

      if (scroll >= 200) {
        header.addClass('fixed_header');
      } else {
        header.removeClass("fixed_header");
      }
    });
  });
</script>

</body>

</html>