import Cookies from 'js-cookie';

// Header
const headerTop = document.querySelector('.header-top');
const offsetAmount = headerTop.offsetHeight;
const header = document.querySelector('.header');

function hasResized() {
  return header.offsetHeight - 2;
}

function hasScrolled() {
  if (window.pageYOffset > offsetAmount) {
    document.body.setAttribute('data-scrolled', 'true');
  } else {
    document.body.setAttribute('data-scrolled', 'false');
  }
}

document.addEventListener('scroll', () => {
  setTimeout(() => {
    hasScrolled();
  }, 122);
});

// Set amount to slide header up
header.style.setProperty('--headerHeight', offsetAmount + 'px');

// Accesibility
// const btnToggle = document.querySelector('.m-accessibility__btn-toggle');

// btnToggle.addEventListener('click', function() {
//   toggleState(document.body, 'show-access', 'true', 'false');
// });

const btnAccessText = document.querySelector('.m-accessibility__btn-text'),
  btnAccessContrast = document.querySelector('.m-accessibility__btn-contrast');

btnAccessText.addEventListener('click', function() {
  toggleState(document.body, 'access-text', 'true', 'false');
  toggleState(btnAccessText, 'state', 'active', '');

  if (!Cookies.get('bigtext')) {
    Cookies.set('bigtext', 'true', { expires: 30 });
  } else {
    Cookies.set('bigtext', '');
    btnAccessText.setAttribute('data-state', '');
  }
});

btnAccessContrast.addEventListener('click', function() {
  toggleState(document.body, 'access-contrast', 'true', 'false');
  toggleState(btnAccessContrast, 'state', 'active', '');

  if (!Cookies.get('highcontrast')) {
    Cookies.set('highcontrast', 'true', { expires: 30 });
  } else {
    Cookies.set('highcontrast', '');
    btnAccessContrast.setAttribute('data-state', '');
  }
});

if (Cookies.get('bigtext')) {
  document.body.setAttribute('data-access-text', 'true');
  btnAccessText.setAttribute('data-state', 'active');
}
if (Cookies.get('highcontrast')) {
  document.body.setAttribute('data-access-contrast', 'true');
  btnAccessContrast.setAttribute('data-state', 'active');
}
