import { mapStyles } from './components/mapstyling';
import { markerIconSvg } from './components/mapmarker';

// The array of locations to mark on the map.
const locations = [];
const locationsData = document.querySelectorAll('.location');
const numLocations = locationsData.length;

for (let x = 0; x < numLocations; x++) {
  const latlngRaw = locationsData[x].getAttribute('data-latlng').split('%');

  locations[x] = [
    latlngRaw[0],
    latlngRaw[1],
    locationsData[x].getAttribute('data-country'),
    locationsData[x].getAttribute('data-link'),
    locationsData[x].getAttribute('data-copy'),
    x,
  ];
}

// Set marker attributes.
const markerWidth = 80;
const markerHeight = 100;
const markerScale = 0.5;

const markerIcon = {
  url: markerIconSvg,
  size: new google.maps.Size(markerWidth, markerHeight),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(markerWidth * (markerScale / 2), markerHeight * markerScale),
  scaledSize: new google.maps.Size(markerWidth * markerScale, markerHeight * markerScale),
};

// Init map on Google 'load' event
google.maps.event.addDomListener(window, 'load', init);

// Init the map
function init() {
  const defaultZoom = 14; // The minimum zoom level
  const mapElementId = 'map';
  const mapElement = document.getElementById(mapElementId);
  const mapOptions = {
    zoom: defaultZoom,
    zoomControl: true,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.SMALL,
    },
    disableDoubleClickZoom: false,
    mapTypeControl: false,
    panControl: false,
    scaleControl: false,
    scrollwheel: false,
    streetViewControl: false,
    draggable: true,
    overviewMapControl: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: mapStyles,
  };

  // Create new map object
  const map = new google.maps.Map(mapElement, mapOptions);

  let infoWindow = new google.maps.InfoWindow();
  let bounds = new google.maps.LatLngBounds();

  for (let i = 0; i < locations.length; i++) {
    const position = new google.maps.LatLng(locations[i][0], locations[i][1]);

    const marker = new google.maps.Marker({
      position: position,
      map: map,
      icon: markerIcon,
    });

    let windowContent = `<a href="${locations[i][3]}"><h3>${locations[i][2]}</h3><p>${locations[i][4]}</p></a>`;

    if (locations[i][3] == '') {
      windowContent = `<h3>${locations[i][2]}</h3><p>${locations[i][4]}</p>`;
    }

    google.maps.event.addListener(
      marker,
      'click',
      (function(marker, i) {
        return function() {
          infoWindow.setContent(windowContent);
          infoWindow.setOptions({
            pixelOffset: new google.maps.Size(markerWidth * (markerScale / 2) * -1, 0),
          });
          infoWindow.open(map, marker);
        };
      })(marker, i)
    );

    bounds.extend(marker.position);
  }

  map.fitBounds(bounds);
}
