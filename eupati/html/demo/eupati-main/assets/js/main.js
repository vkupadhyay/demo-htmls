import zenscroll from 'zenscroll';
import toggleDataAttr from './components/toggleDataAttr';

// Search
const searchBtn = document.querySelector('.a-search');

searchBtn.addEventListener('click', toggleSearch);

function toggleSearch() {
  toggleDataAttr(document.body, 'search', 'open', 'closed');
}
