<?php

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

require_once(ABSPATH . 'wp-admin/includes/screen.php');

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width)){
    $content_width = 900;
}

if (function_exists('add_theme_support')){
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail\
    add_image_size('gallery', 800, 450, true);
    add_image_size('imgtext', 468, '');
    add_image_size('teaser', 470, 264, true);
    add_image_size('banner', 2000, 636, true);
    add_image_size('inline', 1000, '');

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('eupati', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function menu_top(){
	wp_nav_menu(
	array(
		'theme_location'  => 'top-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="a-menu a-menu--align-right a-menu__top">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

function menu_main(){
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="a-menu a-menu--align-right  a-menu__main">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

function menu_main_right(){
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu-right',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="a-menu a-menu--align-right a-menu__main-right">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

function menu_social(){
	wp_nav_menu(
	array(
		'theme_location'  => 'social-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="a-menu a-menu--align-right a-menu__social">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

function menu_footer_top(){
	wp_nav_menu(
	array(
		'theme_location'  => 'top-footer-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="a-menu a-menu__footer-top">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

function menu_footer(){
	wp_nav_menu(
	array(
		'theme_location'  => 'footer-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="a-menu a-menu--align-right a-menu__footer">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// Load HTML5 Blank scripts (header.php)
function eupati_header_scripts(){
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('eupmodernizer', get_template_directory_uri() . '/dist/js/modernizr.js', array(), '1.0.0', true);
        wp_enqueue_script('eupmodernizer');

        // wp_register_script('superfish', get_template_directory_uri() . '/dist/js/superfish.js', array('jquery'), '1.0.0', true);
        // wp_enqueue_script('superfish');

        wp_register_script('eupdefault', get_template_directory_uri() . '/dist/js/defaults.js', array(), '1.0.0', true);
        wp_enqueue_script('eupdefault');

        wp_register_script('eupglobal', get_template_directory_uri() . '/dist/js/global.js', array('jquery'), '1.0.1', true);
        wp_enqueue_script('eupglobal');

    }
}

// Conditional JS
// function eupati_conditional_scripts(){
//     if (is_page('pagenamehere')) {
//         // wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
//         // wp_enqueue_script('scriptname'); // Enqueue it!
//     }
// }

// CSS
function eupati_styles(){
    wp_register_style('eupati', get_template_directory_uri() . '/dist/css/global.css?4', array(), '1.0.24', 'all');
    wp_enqueue_style('eupati'); // Enqueue it!

    wp_register_style('eupati-print', get_template_directory_uri() . '/dist/css/print.css', array(), '1.0', 'all');
    wp_enqueue_style('eupati-print'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu(){
    register_nav_menus(array(
        'top-menu' => __('Top Menu', 'eupati'),
        'header-menu' => __('Header Menu', 'eupati'), // Main Navigation
        'header-menu-right' => __('Header Menu Right', 'eupati'),
        'top-footer-menu' => __('Top Footer Menu', 'eupati'),
        'footer-menu' => __('Footer Menu', 'eupati'),
        'social-menu' => __('Footer social links', 'eupati')
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = ''){
    $args['container'] = false;
    return $args;
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist){
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes){
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('News sidebar', 'eupati'),
        'description' => __('News section sidebar', 'eupati'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination(){
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'prev_text' => '&laquo;',
        'next_text' => '&raquo;'
    ));
}

// Custom Excerpts
function eupati_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'eupati_custom_excerpt_length', 999 );


// // Custom View Article link to Post
// function html5_blank_view_article($more){
//     global $post;
//     return null;
// }

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag){
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html ){
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'eupati_header_scripts'); // Add Custom Scripts to wp_head
// add_action('wp_print_scripts', 'eupati_conditional_scripts'); // Add Conditional Page Scripts
add_action('wp_enqueue_scripts', 'eupati_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
// add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
// add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
// add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
// add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
// Prevent WP from adding <p> tags on pages
function disable_wp_auto_p( $content ) {

    // remove_filter( 'the_content', 'wpautop' );
    remove_filter( 'the_excerpt', 'wpautop' );

    return $content;
}
add_filter( 'the_content', 'disable_wp_auto_p', 0 );

/**
 * Disable the emoji's
 */
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
   }
   add_action( 'init', 'disable_emojis' );
   
   /**
    * Filter function used to remove the tinymce emoji plugin.
    * 
    * @param array $plugins 
    * @return array Difference betwen the two arrays
    */
   function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
    return array();
    }
   }
   
   /**
    * Remove emoji CDN hostname from DNS prefetching hints.
    *
    * @param array $urls URLs to print for resource hints.
    * @param string $relation_type The relation type the URLs are printed for.
    * @return array Difference betwen the two arrays.
    */
   function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
    if ( 'dns-prefetch' == $relation_type ) {
    /** This filter is documented in wp-includes/formatting.php */
    $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
   
   $urls = array_diff( $urls, array( $emoji_svg_url ) );
    }
   
   return $urls;
   }

// Disable WP embeds
   function disable_embeds_code_init() {

    // Remove the REST API endpoint.
    remove_action( 'rest_api_init', 'wp_oembed_register_route' );
   
    // Turn off oEmbed auto discovery.
    add_filter( 'embed_oembed_discover', '__return_false' );
   
    // Don't filter oEmbed results.
    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
   
    // Remove oEmbed discovery links.
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
   
    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );
    add_filter( 'tiny_mce_plugins', 'disable_embeds_tiny_mce_plugin' );
   
    // Remove all embeds rewrite rules.
    add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );
   
    // Remove filter of the oEmbed result before any HTTP requests are made.
    remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
   }
   
   add_action( 'init', 'disable_embeds_code_init', 9999 );
   
   function disable_embeds_tiny_mce_plugin($plugins) {
       return array_diff($plugins, array('wpembed'));
   }
   
   function disable_embeds_rewrites($rules) {
       foreach($rules as $rule => $rewrite) {
           if(false !== strpos($rewrite, 'embed=true')) {
               unset($rules[$rule]);
           }
       }
       return $rules;
   }

// Disable Media Button
function RemoveAddMediaButtonsForNonAdmins(){
    if ( !current_user_can( 'manage_options' ) ) {
        remove_action( 'media_buttons', 'media_buttons' );
    }
}
add_action('admin_head', 'RemoveAddMediaButtonsForNonAdmins');

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_filter( 'wp_get_attachment_image_attributes', 'gs_change_attachment_image_markup' );

/**
* Change src and srcset to data-src and data-srcset, and add class 'lazyload'
* @param $attributes
* @return mixed
*/
function gs_change_attachment_image_markup($attributes){
    if (isset($attributes['srcset'])) {
        $attributes['data-srcset'] = $attributes['srcset'];
        $attributes['srcset'] = '';
    }
    $attributes['class'] .= ' lazy';
    return $attributes;
}

// function custom_active_item_classes($classes = array(), $menu_item = false){            
//     global $post;
//     if ($post->post_type){

//         $classes[] = ($menu_item->url == get_post_type_archive_link($post->post_type)) ? 'current-menu-item active' : '';
//         return $classes;
//     }
// }
// add_filter( 'nav_menu_css_class', 'custom_active_item_classes', 10, 2 );

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 500;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    }	
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
    return $excerpt;
}

// Admin Bar
add_theme_support( 'admin-bar', array( 'callback' => '__return_false' ) );

add_action('admin_head', 'wpml_custom_dropdown');

function wpml_custom_dropdown() {
  echo '<style>
  #wp-admin-bar-WPML_ALS-default {
    max-height: 50vh;
    overflow: scroll;
  }
  </style>';
}

// Disable Gravity forms tabindex
add_filter( 'gform_tabindex', '__return_false' );

/**
 * Customise Admin Menu
 */
add_action( 'admin_bar_menu', 'customise_admin_menu', 999 );

function customise_admin_menu( $wp_admin_bar ) {
  $wp_admin_bar->remove_menu( 'customize' );
  $wp_admin_bar->remove_menu( 'comments' );
}