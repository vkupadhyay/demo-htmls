		<a href="#top" class="a-back-top" title="<?php _e('Back to the top', 'eupati');?>"><?php _e('Back to the top', 'eupati');?><span>^</span></a>

		<footer class="o-footer" role="contentinfo">

			<div class="o-footer__top">
				<div class="grid">
					<div class="col-sm-12">
						<div class="m-footer-row">
							<div class="m-footer-row__left">
								<a href="https://eupati.eu" title="<?php _e('Go to the EUPATI Main Portal', 'eupati'); ?>" class="a-footer-logo" aria-label="<?php _e('Go to the EUPATI Main Portal', 'eupati'); ?>">
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	viewBox="0 0 121.5 28.1" style="enable-background:new 0 0 121.5 28.1;" xml:space="preserve">
										<g>
											<g>
												<polygon class="st0" points="0.6,4.7 0,0.5 15.5,0.5 15.5,4.5 5.8,4.5 5.8,11.7 14.4,11.7 13.4,15.7 5.8,15.7 5.8,23.6 16,23.6 
													15,27.6 0.6,27.6 		"/>
												<path class="st0" d="M38.7,4.7l-0.6-4.2h5.2v16.2c0,6.8-4.5,11.4-11.7,11.4c-10.4,0-10.6-9.6-10.6-10.3V4.7l-0.6-4.2h5.8v16.9
													c0,4,2.6,6.7,6.1,6.7s6.3-2.9,6.3-7V4.7z"/>
												<path class="st0" d="M58,0.5c4.9,0,9.6,2.7,9.6,8.5c0,4.8-4.6,9-10.9,9h-1.2v6.1l0.4,3.5h-5.7V4.7l-0.6-4.2H58z M57.3,14
													c2.7-0.1,4.9-2.3,4.8-5c0-3.1-2.3-4.5-4.8-4.5h-1.8V14H57.3z"/>
												<path class="st0" d="M77.1,0.8L80.5,0l8.3,22.6l2.3,5h-5.9l-2.3-6.3h-9l-2.3,6.3h-4.9L77.1,0.8z M75.2,17.1h6.5l-2.8-7.8l-0.4-1.6
													l-0.4,1.6L75.2,17.1z"/>
												<polygon class="st0" points="89.3,0.5 111.8,0.5 111.8,4.6 103.1,4.6 103.1,24.1 103.5,27.6 97.8,27.6 97.8,4.6 89.3,4.6 		"/>
												<polygon class="st0" points="121.1,24.1 121.5,27.6 115.8,27.6 115.8,4.7 115.2,0.5 121.1,0.5 		"/>
											</g>
										</g>
									</svg>
								</a>
								
								<?php menu_footer_top(); ?>
							</div>
							<div class="m-footer-row__right">
								<?php menu_social(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="o-footer__bottom">
				<div class="grid">
					<div class="col-sm-12">
						<div class="m-footer-row">
							<div class="m-footer-row__left">
								<small>
									&copy; <?php _e('Copyright', 'eupati'); ?> EUPATI <?php print date('Y');?> - <?php _e('Design by', 'eupati'); ?> <a href="https://blooberrydesign.co.uk" title="<?php _e('Design by', 'eupati'); ?> Blooberry Design" target="_blank" rel="noopener noreferrer">Blooberry Design</a>
								</small>
							</div>
							<div class="m-footer-row__right">
								<?php menu_footer(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>

		</footer>

		<?php wp_footer(); ?>
	</body>
</html>
