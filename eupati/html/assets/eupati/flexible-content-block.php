<?php if (have_rows('content_blocks')) : ?>
    <?php while (have_rows('content_blocks')) : the_row(); ?>
        <?php $current_layout  = get_row_layout(); ?>
        <?php if ($current_layout == 'text') : ?>
            <?php get_template_part('content-blocks/block', 'text'); ?>
        <?php elseif ($current_layout == 'image') : ?>
            <?php get_template_part('content-blocks/block', 'image'); ?>
        <?php elseif ($current_layout == 'quotes') : ?>
            <?php get_template_part('content-blocks/block', 'quotes'); ?>
        <?php elseif ($current_layout == 'video') : ?>
            <?php get_template_part('content-blocks/block', 'video'); ?>
        <?php elseif ($current_layout == 'gallery') : ?>
            <?php get_template_part('content-blocks/block', 'gallery'); ?>
        <?php elseif ($current_layout == 'downloads') : ?>
            <?php get_template_part('content-blocks/block', 'downloads'); ?>
        <?php elseif ($current_layout == 'image_text') : ?>
            <?php get_template_part('content-blocks/block', 'image-text'); ?>
        <?php elseif ($current_layout == 'accordion') : ?>
            <?php get_template_part('content-blocks/block', 'accordion'); ?>
        <?php elseif ($current_layout == 'form') : ?>
            <?php get_template_part('content-blocks/block', 'form'); ?>
        <?php elseif ($current_layout == 'page_links') : ?>
            <?php get_template_part('content-blocks/block', 'page-links'); ?>
        <?php elseif ($current_layout == 'pullout') : ?>
            <?php get_template_part('content-blocks/block', 'pullout'); ?>
        <?php elseif ($current_layout == 'newsletter') : ?>
            <?php get_template_part('content-blocks/block', 'newsletter'); ?>
        <?php elseif ($current_layout == 'embed') : ?>
            <?php get_template_part('content-blocks/block', 'embed'); ?>
        <?php elseif (in_array($current_layout, ['icons_block','icon_and_text_block', 'image_and_text_box', 'overlay_content_with_background', 'video_with_overlay', 'statistics_content', 'testimonial_slider', 'help_content', 'copy_content_section', 'parallax_background'])) : ?>
            <?php get_template_part('content-blocks/block', str_replace('_', '-', $current_layout)); ?>
        <?php else :?>
            <!-- <?php echo $current_layout;?>-->
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>