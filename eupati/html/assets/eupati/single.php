<?php get_header(); ?>

	<main role="main">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php //post_class(); ?> class="o-article">

				<div class="grid justify-center m-no-banner">
					<div class="col-sm-8 col-lg-6">
							<h1><?php the_title(); ?></h1>
					</div>
					<div class="col-sm-3 col-lg-2 col-bleed"></div>
				</div>

				<section class="grid justify-center">
					<div class="col-sm-8 col-lg-6">

						<?php if ( has_post_thumbnail()) : ?>
							<?php the_post_thumbnail(); ?>
						<?php endif; ?>

						<?php the_content(); ?>

						<?php if( have_rows('content_blocks') ): ?>
							<?php while( have_rows('content_blocks') ): the_row(); ?>
								<?php if( get_row_layout() == 'text' ): ?>
									<?php get_template_part('post-blocks/block','text');?>
								<?php elseif( get_row_layout() == 'image' ): ?>
									<?php get_template_part('post-blocks/block','image');?>
								<?php elseif( get_row_layout() == 'quotes' ): ?>
									<?php get_template_part('post-blocks/block','quotes');?>
								<?php elseif( get_row_layout() == 'video' ): ?>
									<?php get_template_part('post-blocks/block','video');?>
								<?php elseif( get_row_layout() == 'gallery' ): ?>
									<?php get_template_part('post-blocks/block','gallery');?>
								<?php elseif( get_row_layout() == 'downloads' ): ?>
									<?php get_template_part('post-blocks/block','downloads');?>
								<?php elseif( get_row_layout() == 'image_text' ): ?>
									<?php get_template_part('post-blocks/block','image-text');?>
								<?php elseif( get_row_layout() == 'accordion' ): ?>
									<?php get_template_part('post-blocks/block','accordion');?>
								<?php elseif( get_row_layout() == 'form' ): ?>
									<?php get_template_part('post-blocks/block','form');?>
								<?php endif; ?>
							<?php endwhile; ?>
						<?php endif; ?>

						<div class="m-entity m-post-info">
							<p class="m-post-info__date"><?php _e( 'Date posted', 'eupati' ); echo ': ';  the_time('F j, Y'); ?></p>
							<p class="m-post-info__author"><?php _e( 'Author', 'eupati' ); echo ': ';  the_author(); ?></p>
							<p class="m-post-info__cats"><?php _e( 'Categories: ', 'eupati' ); the_category(', '); ?></p>
						</div>
					</div>

					<div class="col-sm-3 col-lg-2">
						<div class="m-news-sidebar">
							<?php get_sidebar(); ?>
						</div>
					</div>
				</section>

			</article>

		<?php endwhile; endif; ?>

		<section class="a-angle a-angle--nowt">
			<div class="grid justify-center">
				<div class="col-sm-12 col-lg-10">

					<h2 class="a-title a-title--bottom"><?php _e('Related News stories', 'eupati');?></h2>
					
					<div class="m-news m-news--3">
						<?php
							$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) );
							if( $related ) foreach( $related as $post ) {
							setup_postdata($post); ?>
								<?php include $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/eupati/teasers/news.php';?>
							<?php }
							wp_reset_postdata(); 
						?>
					</div>

				</div>
			</div>
		</section>

		<aside class="a-back">
			<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" title="<?php _e('Back to news &amp events','eupati'); ?>" class="a-btn"><?php _e('Back to news &amp events','eupati'); ?></a>
		</aside>

	</main>

<?php get_footer(); ?>
