<section class="m-entity m-entity__text">

  <?php if (get_sub_field('title')){?>
    <h2>
      <?php the_sub_field('title'); ?>
    </h2>
  <?php }?>

  <?php the_sub_field('copy'); ?>

</section>