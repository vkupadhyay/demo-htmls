<section class="m-entity m-entity__accordion">
  <div class="js-accordion">
    <?php
      if( have_rows('accordion_row') ): while ( have_rows('accordion_row') ) : the_row();
    ?>
      <p class="js-accordion__header h3"><?php echo get_sub_field('title');?></p>
      <div class="js-accordion__panel">
        <div class="js-accordion__panel-inner">
          <?php echo get_sub_field('copy');?>
        </div>
      </div>
    <?php 
      endwhile; endif;
    ?>
  </div>
</section>