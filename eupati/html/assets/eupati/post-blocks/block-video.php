<section class="m-entity m-entity__video">
  <div class="m-entity__video-inner-sm">
    <?php if (get_sub_field('title')){?>
      <h2>
        <?php the_sub_field('title'); ?>
      </h2>
    <?php }?>

    <?php if (get_sub_field('video')) {?>
      <div class="video-container">
        <?php the_sub_field('video'); ?>
      </div>
    <?php } ?>
  </div>

  <?php if (get_sub_field('copy')) {?>
    <?php the_sub_field('copy'); ?>
  <?php } ?>
</section>