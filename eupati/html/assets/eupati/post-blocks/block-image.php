<?php if (get_sub_field('image')):?>
  <section class="m-entity m-entity__image">
    <?php if (get_sub_field('title')){?>
      <h2>
        <?php the_sub_field('title'); ?>
      </h2>
    <?php }?>

    <?php $image = get_sub_field('image');?>
    <figure>
        <?php echo wp_get_attachment_image( $image, 'full' ); ?>
    </figure>
  </section>
<?php endif;?>