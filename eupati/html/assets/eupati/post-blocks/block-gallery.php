<section class="m-entity m-entity__gallery-sm">
  <?php if (get_sub_field('title')){?>
    <h2>
      <?php the_sub_field('title'); ?>
    </h2>
  <?php }?>

  <?php $images = get_sub_field('images');        
    if( $images ): ?> 
      <div class="m-entity__gallery-inner gallery">

      <?php foreach( $images as $image ): ?>
        <a href="<?php echo esc_url($image['url']); ?>" title="Open image" data-caption="<?php print $image['title'];?>">
          <img data-src="<?php print $image['sizes']['gallery'];?>" alt="<?php print $image['alt'];?>" class="lazy">
        </a>
      <?php endforeach; ?>

      </div>
  <?php endif; ?>
</section>