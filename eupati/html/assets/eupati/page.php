<?php get_header(); ?>

<main role="main">

	<div class="grid m-no-banner justify-center">
		<div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6">
			<h1><?php the_title(); ?></h1>
			
			<div class="m-entity__body">
				<?php the_content(); ?>
			</div>
		</div>
	</div>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<?php get_template_part('flexible-content','block'); ?>


	<?php endwhile; ?>

	<?php else: ?>

			<h2><?php _e( 'Sorry, nothing to display.', 'eupati' ); ?></h2>

	<?php endif; ?>

	<?php global $post;
  if ( $post->post_parent ) { ?>
		<aside class="a-back">
			<a href="<?php echo get_permalink( $post->post_parent ); ?>" title="<?php _e('Back to','eupati'); ?> <?php echo get_the_title( $post->post_parent ); ?>" class="a-btn"><?php _e('Back to','eupati'); ?> <?php echo get_the_title( $post->post_parent ); ?></a>
		</aside>
	<?php } ?>

</main>

<?php get_footer(); ?>
