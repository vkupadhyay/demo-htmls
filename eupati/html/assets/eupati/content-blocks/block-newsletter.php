<?php if(get_sub_field('show_newsletter') == 1){?>
  <div class="a-newsletter-wrapper a-newsletter-wrapper--page">
    <?php echo do_shortcode( '[ic_add_posts id="303" post_type="newsletter" template="single-newsletter.php"]' ); ?>
    <span class="a-newsletter-wrapper--page__stripe"></span>
  </div>
<?php } ?>
