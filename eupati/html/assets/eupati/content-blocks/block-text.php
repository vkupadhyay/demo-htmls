<section class="m-entity m-entity__text">
  <div class="grid justify-center">
    <div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6">
      
      <?php if (get_sub_field('title')){?>
        <h2>
          <?php the_sub_field('title'); ?>
        </h2>
      <?php }?>

      <?php the_sub_field('copy'); ?>
      
    </div>
  </div>
</section>