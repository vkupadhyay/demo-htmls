<section class="m-entity m-entity__quote">
  <div class="grid justify-center">
    <div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6">

      <div class="glide">

        <div class="glide__track" data-glide-el="track">
          <ul class="glide__slides">
            
            <?php 
              if( have_rows('quotes') ): while ( have_rows('quotes') ) : the_row();
            ?>
              <li class="glide__slide">

                <blockquote>
                  <?php echo get_sub_field('quote'); ?>
                </blockquote>

                <cite>
                  <span class="name">
                    <?php echo get_sub_field('author'); ?>
                  </span>
                  <?php
                    if (get_sub_field('source')){
                      echo ' - ' . get_sub_field('source'); 
                    }
                   ?>
                </cite>
                
              </li>
            <?php
              endwhile; endif;
            ?>
          </ul>
        </div>

        <div class="glide__bullets" data-glide-el="controls[nav]">
          <?php 
            if( have_rows('quotes') ): while ( have_rows('quotes') ) : the_row();
          ?>
            <button class="glide__bullet" data-glide-dir="=<?php echo get_row_index() - 1;?>"></button>
          <?php
            endwhile; endif;
          ?>
        </div>

      </div>

    </div>
  </div>
</section>