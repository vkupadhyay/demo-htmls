<section class="full-img-section videos-overlay">
    <div class="full-img-box">
        <?php if (get_sub_field('video')) { ?>
            <div class="video-container">
                <?php the_sub_field('video'); ?>
            </div>
        <?php } ?>
        <!-- <picture>
            <source media="(min-width:650px)" srcset="https://eupati.oxypay.in/wp-content/themes/eupati-main/dist/img/dv1811032.jpg" />
            <source media="(min-width:465px)" srcset="https://eupati.oxypay.in/wp-content/themes/eupati-main/dist/img/dv1811032.jpg" />
            <img src="https://eupati.oxypay.in/wp-content/themes/eupati-main/dist/img/dv1811032.jpg" alt="Flowers" />
        </picture> -->
    </div>
    <div class="full-img-container">
        <div class="grid justify-center">
            <div class="col-sm-10">
                <div class="full-img-content a-align--center">
                    <?php if (get_sub_field('title')) { ?>

                        <h2><?php the_sub_field('title'); ?></h2>
                    <?php } ?>

                    <div class="text-paragraph">
                        <?php the_sub_field('copy'); ?>

                    </div>
                    <div class="full-img-button">
                        <button class="video-btn"><img src="<?php echo  site_url() ?>/wp-content/themes/eupati-main/assets/img/video-icon.svg" alt="" /> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>