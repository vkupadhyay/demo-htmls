<?php if (get_sub_field('overlay_title') || get_sub_field('overlay_content')) : ?>
    <section class="full-img-section">
        <div class="full-img-box">
            <?php
            $background_image = get_sub_field('background_image');
            if ($background_image) { ?>
                <picture>
                    <source media="(min-width:650px)" srcset="<?php echo $background_image['url']; ?>" />
                    <source media="(min-width:465px)" srcset="<?php echo $background_image['url']; ?>" />
                    <img src="<?php echo $background_image['url']; ?>" alt="<?php echo $background_image['alt']; ?>" />
                </picture>
            <?php } ?>
        </div>
        <div class="full-img-container">
            <div class="grid justify-center">
                <div class="col-sm-10">
                    <div class="full-img-content a-align--center">
                        <?php if (get_sub_field('overlay_title')) { ?>
                            <h2>
                                <?php the_sub_field('overlay_title'); ?>
                            </h2>
                        <?php } ?>
                        <div class="text-paragraph">
                            <?php the_sub_field('overlay_content'); ?>
                        </div>

                        <?php
                        $link = get_sub_field('overlay_cta');
                        if ($link) {
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <div class="full-img-button">
                                <a href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" target="<?php echo $link_target; ?>" class="a-btn a-btn--wide"><?php echo $link_title; ?></a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>