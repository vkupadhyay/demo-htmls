<section class="m-entity m-entity__gallery">
  <div class="grid justify-center">
    <div class="col-sm-12 col-md-10 col-lg-8">

      <?php if (get_sub_field('title')){?>
        <h2 class="a-title a-title--bottom">
          <?php the_sub_field('title'); ?>
        </h2>
      <?php }?>

      <?php $images = get_sub_field('images');        
        if( $images ): ?> 
          <div class="m-entity__gallery-inner gallery">

          <?php foreach( $images as $image ): ?>
            <a href="<?php echo esc_url($image['url']); ?>" title="Open image" data-caption="<?php print $image['title'];?>">
              <img data-src="<?php print $image['sizes']['gallery'];?>" alt="<?php print $image['alt'];?>" class="lazy">
            </a>
          <?php endforeach; ?>

          </div>
      <?php endif; ?>
    
    </div>
  </div>
</section>