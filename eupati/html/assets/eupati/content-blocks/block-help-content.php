<?php if (get_sub_field('left_content_group') || get_sub_field('right_content_group')) : ?>

	<section class="help-section blue-bg section-pd-tb" <?php if (get_sub_field('help_bg')) {
															echo 'style="background-color: ' . get_sub_field('help_bg') . ';"';
														} ?>>
		<div class="grid justify-center">
			<div class="col-sm-11">
				<div class="section-hd a-align--center">
					<?php if (get_sub_field('help_title')) { ?>
						<h2>
							<?php the_sub_field('help_title'); ?>
						</h2>
					<?php } ?>
					<div class="text-paragraph">
						<?php the_sub_field('help_copy'); ?>
					</div>
				</div>

				<div class="help-container">
					<div class="custom-row">
						<?php if (get_sub_field('left_content_group')) : ?>
							<?php while (have_rows('left_content_group')) : the_row(); ?>

								<div class="organization-box" <?php if (get_sub_field('left_content_background')) {
																	echo 'style="background-color: ' . get_sub_field('left_content_background') . ';"';
																} ?>>
									<?php if (get_sub_field('left_content_title')) { ?>
										<h4>
											<?php the_sub_field('left_content_title'); ?>
										</h4>
									<?php } ?>

									<?php if (have_rows('left_content_rows')) : ?>
										<ul class="colm colm--1">

											<?php while (have_rows('left_content_rows')) : the_row(); ?>

												<li>
													<i><img src="<?php echo get_sub_field('item_icon')['url'];?>" alt="" /></i>
													<h4><?php the_sub_field('item_title'); ?></h4>
													<div class="text-paragraph">
														<?php the_sub_field('item_text'); ?> 
													</div>

													<?php
													$link = get_sub_field('item_cta');
													if ($link) {
														$link_url = $link['url'];
														$link_title = $link['title'];
														$link_target = $link['target'] ? $link['target'] : '_self';
													?>

														<div class="bottom-btn">
															<a href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" target="<?php echo $link_target; ?>" class="a-btn a-btn--alt"><?php echo $link_title; ?></a>
														</div>
													<?php } ?>

												</li>
											<?php endwhile; ?>
										</ul>
									<?php endif; ?>

								</div>
							<?php endwhile; ?>
						<?php endif; ?>
						<?php if (get_sub_field('right_content_group')) : ?>

							<?php while (have_rows('right_content_group')) : the_row(); ?>

								<div class="individual-box" <?php if (get_sub_field('right_content_background')) {
																echo 'style="background-color: ' . get_sub_field('right_content_background') . ';"';
															} ?>>
									<?php if (get_sub_field('right_content_title')) { ?>
										<h4>
											<?php the_sub_field('right_content_title'); ?>
										</h4>
									<?php } ?>

									<?php if (have_rows('right_content_rows')) : ?>
										<ul class="colm colm--2">

											<?php while (have_rows('right_content_rows')) : the_row(); ?>
												<li>
													<i><img src="<?php echo get_sub_field('item_icon')['url'];?>" alt="" /></i>
													<h4><?php the_sub_field('item_title'); ?></h4>
													<div class="text-paragraph">
														<?php the_sub_field('item_text'); ?>
													</div>
													<?php
													$link = get_sub_field('item_cta');
													if ($link) {
														$link_url = $link['url'];
														$link_title = $link['title'];
														$link_target = $link['target'] ? $link['target'] : '_self';
													?>

														<div class="bottom-btn">
															<a href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" target="<?php echo $link_target; ?>" class="a-btn a-btn--alt"><?php echo $link_title; ?></a>
														</div>
													<?php } ?>
												</li>
											<?php endwhile; ?>
										</ul>
									<?php endif; ?>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>