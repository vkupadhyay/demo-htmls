<?php get_header(); ?>

<main role="main">

	<div class="grid m-no-banner justify-center">
		<div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6">
			<h1><?php _e( 'Page not found', 'eupati' ); ?></h1>
			<div class="m-entity">
				<h2>
					<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'eupati' ); ?></a>
				</h2>
			</div>
		</div>
	</div>
	
</main>

<?php get_footer(); ?>
