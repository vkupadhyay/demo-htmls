<?php get_header(); ?>

	<main role="main">

		<div class="grid justify-center m-no-banner">
			<div class="col-sm-12">

				<h1 class="a-title a-title--bottom"><?php _e( 'Posts tagged as', 'eupati' ); echo ' '; single_cat_title(); ?></h1>

			</div>
		</div>

		<section class="grid justify-center">
			<div class="col-md-12 col-lg-6">

				<?php get_template_part('loop'); ?>

				<?php get_template_part('pagination'); ?>

			</div>
			<div class="col-md-12 col-lg-2">

				<div class="m-news-sidebar">
					<?php get_sidebar(); ?>
				</div>
				
			</div>
		</section>

	</main>

<?php get_footer(); ?>
