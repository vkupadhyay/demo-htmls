<?php include $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/eupati/templates/html-header.php';?>

<header class="o-header header" role="banner">

    <div class="o-header__logo">
      <div class="grid">
        <div class="col-sm-12 col-bleed">
          <a href="<?php echo home_url(); ?>" title="<?php _e('Home', 'eupati');?>" class="o-header__logo-link">
            <?php 
              $logo = get_field('logo', 'option');

              if ($logo != null){
            ?>
              <img class="logo-img" src="<?php print $logo['url'];?>" alt="<?php echo get_bloginfo( 'name' ); ?>" />
            <?php
              } else {
            ?>
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/logo.svg" alt="EUPATI Logo" class="logo-img">
            <?php } ?>
          </a>
        </div>
      </div>
    </div>

    <div class="o-header__top header-top">
      <div class="grid justify-center">
        <div class="col-sm-12 col-bleed">
          <div class="m-header-row m-header-row--top">
            <div class="m-accessibility">
              <button class="m-accessibility__btn m-accessibility__btn-toggle"><?php print _e('Accessibility', 'eupati');?></button>
              
              <div class="m-accessibility__options">
                <button class="m-accessibility__btn m-accessibility__btn-text"><?php print _e('Bigger text', 'eupati');?></button>
                <button class="m-accessibility__btn m-accessibility__btn-contrast"><?php print _e('High contrast', 'eupati');?></button>
              </div>
            </div>
            
            <div class="m-nav__bottom-menu-top">
              <?php menu_top(); ?>
            </div>
            
            <div class="m-wpml">
              <?php echo do_action('wpml_add_language_selector'); ?>
            </div>

            <a href="https://eupati.eu" title="<?php print _e('Back to EUPATI', 'eupati');?>" class="a-back-eupati"><?php print _e('Back to EUPATI', 'eupati');?></a>

            <?php 
              $flag = get_field('flag', 'option');

              if ($flag != null){
            ?>
              <img class="a-site-flag" src="<?php print $flag['url'];?>" alt="<?php echo get_bloginfo( 'name' ); ?>" />
            <?php
              }
            ?>
          </div>
        </div>
      </div>
    </div>

    <div class="o-header__bottom">
      <div class="grid justify-center">
        <div class="col-sm-12 col-bleed">
          <div class="m-header-row m-header-row--bottom">
            
            <nav class="m-nav" role="navigation">
              <?php menu_main(); ?>
              
              <div class="m-nav__bottom-menu">
                <?php menu_top(); ?>
              </div>

              <a href="https://eupati.eu" title="<?php print _e('Back to EUPATI', 'eupati');?>" class="a-back-eupati a-back-eupati--mob"><?php print _e('Back to EUPATI', 'eupati');?></a>
            </nav>

            <button class="a-nav-toggle" title="Toggle Nav">
              <span class="a-nav-toggle__line a-nav-toggle__line-1"></span>
              <span class="a-nav-toggle__line a-nav-toggle__line-2"></span>
              <span class="a-nav-toggle__line a-nav-toggle__line-3"></span>
            </button>

          </div>
        </div>
      </div>
    </div>


</header>

<span id="top"></span>