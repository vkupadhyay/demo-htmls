<article class="o-teaser">
  <?php 
    $thumbnail = get_field('thumbnail');
    if ($thumbnail):
  ?>
    <a href="<?php the_permalink(); ?>" title="<?php echo _e('Image linking to ','eupati');?> <?php the_title(); ?>" class="o-teaser__thumb" aria-label="<?php echo _e('Image linking to ','eupati');?> <?php the_title(); ?>">
      <img data-src="<?php print $thumbnail['sizes']['teaser'];?>" alt="<?php print $thumbnail['alt'];?>" class="lazy" />
    </a>
  <?php elseif ( has_post_thumbnail()): ?>
    <a href="<?php the_permalink(); ?>" title="<?php echo _e('Image linking to ','eupati');?> <?php the_title(); ?>" class="o-teaser__thumb" aria-label="<?php echo _e('Image linking to ','eupati');?> <?php the_title(); ?>">
      <?php the_post_thumbnail('teaser'); ?>
    </a>
  <?php endif; ?>

  <div class="o-teaser__content">
    <h3>
      <a href="<?php the_permalink(); ?>" title="<?php echo _e('Read','eupati');?> <?php the_title(); ?>"><?php the_title(); ?></a>
    </h3>
 
    <p><?php echo excerpt(20);?></p>

    <a href="<?php the_permalink(); ?>" title="<?php echo _e('Read more about','eupati');?> <?php the_title(); ?>" class="a-more"><?php echo _e('Read more','eupati');?></a>
  </div>

</article>