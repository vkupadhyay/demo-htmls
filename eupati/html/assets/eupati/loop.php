<div class="m-news">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<?php include $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/eupati/teasers/news.php';?>

	<?php endwhile; ?>

	<?php else: ?>

		<article class="m-entity">
			<h2><?php _e( 'Sorry, nothing to display.', 'eupati' ); ?></h2>
		</article>

	<?php endif; ?>
</div>