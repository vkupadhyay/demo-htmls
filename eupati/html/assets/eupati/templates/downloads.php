<?php
  if( have_rows('downloads') ): while ( have_rows('downloads') ) : the_row();
?>
  <?php if (get_sub_field('title')){ ?>
    <h2 class="a-title a-title--bottom">
      <?php the_sub_field('title'); ?>
    </h2>
  <?php }?>

  <div class="m-entity__downloads-inner">
    <?php
      if( have_rows('download') ): while ( have_rows('download') ) : the_row();
      
        $type = strtolower(get_sub_field('type'));
        $downloadLink = null;

        if($type == 'file'){
          $file = get_sub_field('file');
          
          if( $file ){
            $downloadLink = '<a href="'. $file['url'] .'" title="Download '. $file['name'] . '" target="_blank" class="a-download a-download--' . $type .'">';
          }
        } else {
          $link = get_sub_field('link');
          if( $link ){
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
            
            $downloadLink = '<a href="'. $link_url .'" title="Open link" target="' . $link_target . '" class="a-download a-download--' . $type .'">';
          }
        }
    ?>

      <?php echo $downloadLink;?>

        <?php if($type == 'file'){?>
          <img src="<?php echo get_template_directory_uri(); ?>/dist/icon/icon-<?php echo $type;?>.svg" alt="<?php echo $type;?> Icon" class="a-download__icon" />
        <?php } else { ?>
          <aisde class="a-download__icon a-download__icon--link"></aisde>
        <?php }?>

        <h3><?php echo get_sub_field('title');?></h3>
        
        <?php if (strlen(str_replace(get_sub_field('copy'), ' ','')) > 0){echo get_sub_field('copy');}?>

        <div class="a-download__link">
          <?php
            if($type == 'file'){
              echo 'Download';
            } else {
              echo 'Open link';
            }
          ?>
        </div>
      </a>

    <?php
      endwhile; endif;
    ?>
  </div>
<?php
  endwhile; endif;
?>