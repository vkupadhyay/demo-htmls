<?php if (have_rows('home_hero_list')) : ?>

	<link href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet" />


	<section class="hero-section hero-slider-section">
		<div class="hero-section-containar hero-slider">


			<?php while (have_rows('home_hero_list')) : the_row(); ?>
				<?php $hero_image = get_sub_field('hero_image'); ?>
				<?php if ($hero_image) : ?>
					<div class="hero-slider-item hero-section-item">

						<figure>
							<img src="<?php print $hero_image['sizes']['banner']; ?>" alt="<?php print $hero_image['alt']; ?>" />
						</figure>

						<div class="hero-slider-container">
							<div class="hero-slider-content">
								<h1><?php the_sub_field('hero_heading'); ?></h1>
								<div class="hero-slider-txt">
									<?php the_sub_field('hero_content'); ?>
								</div>
							</div>
						</div>
					</div>
				<?php
				endif;
				?>
			<?php
			endwhile;
			?>
		</div>
	</section>
<?php
endif;
?>