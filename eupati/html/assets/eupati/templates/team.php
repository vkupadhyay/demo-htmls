<?php
  $team = get_field('meet_the_team');
  if( $team['team_members'] ): ?>
    <section class="m-team m-entity m-entity--not">
    <?php if (isset($team['title'])){?>
      <h2 class="a-title a-title--bottom"><?php echo $team['title']; ?></h2>
    <?php }?>

      <div class="m-team__list">
        <?php
          foreach ($team['team_members'] as $member) {
        ?>
          <div class="m-team__member" role="contentinfo" tabindex="0">
            <img data-src="<?php echo esc_url( $member['image']['url'] ); ?>" alt="<?php echo esc_attr( $member['image']['alt'] ); ?>" class="lazy" />

            <div class="m-team__member-content a-align a-align--center">
              <div class="m-team__member-info">
                <h3><?php print $member['name'];?></h3>
                <span class="role">
                  <?php print $member['role'];?>
                </span>
              </div>
              <div class="m-team__member-bio">
                <?php print $member['copy'];?>
              </div>
            </div>

          </div>
        <?php 
          }
        ?>
    </div>

  </section>
<?php endif; ?>