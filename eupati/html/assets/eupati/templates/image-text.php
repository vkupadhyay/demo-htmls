<div class="m-img-text">
  <?php $link = get_sub_field('image_link');
    if( $link ){
      $link_url = $link['url'];
      $link_title = $link['title'];
      $link_target = $link['target'] ? $link['target'] : '_self';
      ?>
        <a href="<?php echo $link_url;?>" title="Open link" target="<?php echo $link_target;?>" class="m-img-text__link">
      <?php
    }
  ?>

  <?php $image = get_sub_field('image');?>
  <figure>
    <img src="<?php print $image['sizes']['imgtext'];?>" alt="<?php print $image['alt'];?>" />
  </figure>
  
  <?php if ($link){?>
    </a>
  <?php } ?>

  <div class="m-img-text__copy">
    <?php the_sub_field('copy'); ?>
  </div>
</div>