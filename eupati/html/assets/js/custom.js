$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $(".main-header").addClass("white-header");
    } else {
        $(".main-header").removeClass("white-header");
    }
});

$('#navclick').click(function(){
    $(mainContainer).toggleClass('menu-slide');
    $(clickmenu).toggleClass('menu-slide');
});

$(".button").click(() => {
    $("selector").addClass("popshow");
});

$('.hero-slider').slick({
    dots: true,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false
  });

  $('.stories-slider').slick({
    dots: false,
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true
  });