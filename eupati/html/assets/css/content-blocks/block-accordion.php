<section class="m-entity m-entity__accordion background-color<?php echo get_sub_field('background_color') ?>">
  <div class="grid justify-center">
    <div class="col-sm-12 col-md-10 col-lg-8">

      <div class="js-accordion">
        <?php
        if (have_rows('accordion_row')) : while (have_rows('accordion_row')) : the_row();
        ?>
            <p class="js-accordion__header h3 background-color<?php echo get_sub_field('background_color') ?> font-color<?php echo get_sub_field('title_color') ?>"><?php echo get_sub_field('title'); ?></p>
            <div class="js-accordion__panel background-color<?php echo get_sub_field('background_color') ?>">
              <div class="js-accordion__panel-inner font-color<?php echo get_sub_field('text_color') ?>">
                <?php echo get_sub_field('copy'); ?>
                <?php $image = get_sub_field('image'); ?>
                <?php if ($image) { ?>
                  <figure>
                    <picture>
                      <source media="(max-width: 700px)" srcset="<?php print $image['sizes']['large']; ?>" type="image/jpeg">
                      <source data-srcset="<?php print $image['sizes']['banner']; ?>" type="image/jpeg">
                      <img data-src="<?php print $image['sizes']['banner']; ?>" alt="<?php print $image['alt']; ?>" class="lazy" />
                    </picture>
                  </figure>
                <?php } ?>
              </div>
            </div>
        <?php
          endwhile;
        endif;
        ?>
      </div>

    </div>
  </div>
</section>