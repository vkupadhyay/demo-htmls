<?php if (get_sub_field('statistics_row')) : ?>
	<section class="our-statistics-section light-cyan-bg section-pd-tb background-color<?php echo get_sub_field('background_color') ?>">
		<div class="grid justify-center">
			<div class="col-sm-10">
				<div class="section-hd a-align--center">
					<?php if (get_sub_field('statistics_title')) { ?>
						<h2 class="font-color<?php echo get_sub_field('title_color') ?>">
							<?php the_sub_field('statistics_title'); ?>
						</h2>
					<?php } ?>
				</div>
				<div class="our-statistics-container a-align--center">
					<?php if (have_rows('statistics_row')) : ?>
						<ul class="statistics-ul">

							<?php while (have_rows('statistics_row')) : the_row(); ?>
								<li>
									<?php
									$statistics_icon = get_sub_field('statistics_icon');
									if ($statistics_icon) {
									?>
										<i>
											<img data-src="<?php print $statistics_icon['sizes']['small']; ?>" alt="<?php print $statistics_icon['alt']; ?>" class="lazy">
										</i>
									<?php } ?>
									<h3 data-counter="<?php the_sub_field('statistics_value'); ?>" data-suffix="<?php the_sub_field('statistics_value_suffix'); ?>" data-prefix="<?php the_sub_field('statistics_value_prefix'); ?>" class="run-counter font-color<?php echo get_sub_field('title_color') ?>"><?php the_sub_field('statistics_value_prefix'); ?><span class="value-counter-text font-color<?php echo get_sub_field('title_color') ?>"><?php the_sub_field('statistics_value'); ?></span><?php the_sub_field('statistics_value_suffix'); ?></h3>
									<div class="statistics-text">
										<p class="font-color<?php echo get_sub_field('text_color') ?>"><?php the_sub_field('statistics_copy'); ?></p>
									</div>
								</li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>