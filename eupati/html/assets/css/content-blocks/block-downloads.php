<?php $download_group = get_sub_field('downloads'); ?>
<?php if (isset($download_group['download']) && $download_group['download']) : ?>
  <section class="m-entity m-entity__downloads  background-color<?php echo $download_group['background_color'] ?>">
    <div class="grid justify-center">
      <div class="col-sm-12 col-md-10 col-lg-8">

        <?php include $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/eupati/templates/downloads.php'; ?>

      </div>
    </div>
  </section>
<?php endif; ?>