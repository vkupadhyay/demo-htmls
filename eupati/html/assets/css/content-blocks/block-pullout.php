<?php if(get_sub_field('icon') || get_sub_field('copy')) :?>
<div class="m-entity m-entity__pullout background-color<?php echo get_sub_field('background_color') ?> ">
  <div class="grid justify-center">
    <div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6">
  
      <?php 
        $image = get_sub_field('icon');
        if ($image){
      ?>
      <div class="m-pullout">
        <figure>
          <img data-src="<?php print $image['sizes']['small'];?>" alt="<?php print $image['alt'];?>" class="lazy">
        </figure>
      <?php } else { ?>
      <div class="m-pullout m-pullout--noimg">
      <?php } ?>
        
        <div class="m-pullout__content font-color<?php echo get_sub_field('text_color') ?>">
          <?php if (get_sub_field('title')){?>
            <h2 class="font-color<?php echo get_sub_field('title_color') ?>">
              <?php the_sub_field('title'); ?>
            </h2>
          <?php }?>

          <?php the_sub_field('copy'); ?>
          
          <?php
            $link = get_sub_field('button');
            $button_styles = get_sub_field('button_style');
            if( $link ){
              $link_url = $link['url'];
              $link_title = $link['title'];
              $link_target = $link['target'] ? $link['target'] : '_self';
              $svg ='<i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.4 18.4"><path fill="#fff" d="M9.2 0L6.4 2.8l4.3 4.4H0v4h10.7l-4.3 4.4 2.8 2.8 9.2-9.2z"/></svg></i>';
              ?>
            <a href="<?php echo $link_url;?>" title="<?php echo $link_title; ?>" target="<?php echo $link_target;?>" class="a-btn a-btn--alt a-btn--icon <?php echo getButtonClass($button_styles);?>"><?php echo $link_title.$svg; ?></a>
          <?php } ?>
        </div>

      </div>
    </div>
  </div>
</div>
<?php endif;?>