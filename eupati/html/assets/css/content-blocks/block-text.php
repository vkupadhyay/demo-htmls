<section class="m-entity m-entity__text simple_entity__text background-color<?php echo get_sub_field('background_color') ?> content-align-<?php echo get_sub_field('align_title') ?>">
  <div class="grid justify-center">
    <div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6 ">

      <?php if (get_sub_field('title')) { ?>
        <h2 class="font-color<?php echo get_sub_field('title_color') ?>">
          <?php the_sub_field('title'); ?>
        </h2>
      <?php } ?>

      <div class="text-desc font-color<?php echo get_sub_field('text_color') ?>">
        <?php the_sub_field('copy'); ?>
      </div>

    </div>
  </div>
</section>