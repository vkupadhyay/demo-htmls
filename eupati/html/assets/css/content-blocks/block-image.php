<?php if (get_sub_field('image')):?>
  <section class="m-entity m-entity__image background-color<?php echo get_sub_field('background_color') ?>">
    <div class="grid justify-center">
      <div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6">

        <?php if (get_sub_field('title')){?>
          <h2 class="a-title a-title--bottom font-color<?php echo get_sub_field('title_color') ?> line-color<?php echo get_sub_field('line_color') ?>">
            <?php the_sub_field('title'); ?>
          </h2>
        <?php }?>

        <?php $image = get_sub_field('image');?>
        <figure>
            <?php echo wp_get_attachment_image( $image, 'full' ); ?>
        </figure>

      </div>
    </div>
  </section>
<?php endif;?>