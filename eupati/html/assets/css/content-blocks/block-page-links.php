<?php if (get_sub_field('link')):?>
<div class="m-entity m-entity__page-links background-color<?php echo get_sub_field('background_color') ?>">
  <div class="grid justify-center">
    <div class="col-sm-12 col-lg-10">
  
      <div class="m-page-links">
        <?php
          if( have_rows('link') ): while ( have_rows('link') ) : the_row();
        ?>
          <?php 
            $page = get_sub_field('page');
            $button_styles = get_sub_field('button_style');

            if( $page ){
              foreach( $page as $p ){
                $url = get_permalink( $p->ID );
                $title = get_the_title( $p->ID );
                $excerpt = get_the_excerpt($p->ID);
                $image = get_the_post_thumbnail($p->ID, 'teaser');
                // $image = wp_get_attachment_image_src(get_the_post_thumbnail($p->ID), 'teaser');
              }
            } 
          ?>
          
          <a href="<?php echo $url; ?>" title="<?php echo $title; ?>" class="m-page-links__link  background-color<?php echo get_sub_field('background_color') ?>">


            <h3 class="font-color<?php echo get_sub_field('title_color') ?>"><?php echo $title; ?></h3>

            <?php
              $thumbnail = get_sub_field('thumbnail_override');
              if($thumbnail){
            ?>
              <img data-src="<?php print $thumbnail['sizes']['teaser'];?>" alt="<?php print $thumbnail['alt'];?>" class="lazy">
            <?php } else { ?>
              <?php echo $image; ?>
            <?php }?>
            
            <p class="font-color<?php echo get_sub_field('text_color') ?>">
              <?php 
                if (get_sub_field('copy_override')){
                  echo get_sub_field('copy_override');
                } else { 
                  echo $excerpt;  
                } 
              ?>
            </p>

            <span class="m-page-links__link-button <?php echo getButtonClass($button_styles);?>"><?php echo the_sub_field('button_label');?></span>
          </a>
        <?php
          endwhile; endif;
        ?>
      </div>

    </div>
  </div>
</div>
<?php endif;?>