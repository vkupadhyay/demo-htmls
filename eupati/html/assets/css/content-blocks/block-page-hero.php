<?php if (get_sub_field('show_page_hero')) : ?>
    <?php if (have_rows('page_hero_group')) : ?>
        <?php while (have_rows('page_hero_group')) : the_row(); ?>
            <?php $hero_image = get_sub_field('page_hero_image'); ?>
            <?php if ($hero_image) : ?>
                <section class="m-banner m-banner--home m-banner--heros block-page-hero <?php the_sub_field('content_layout_align'); ?>">
                    <figure>
                        <?php
                        $ID  = $hero_image['ID'];
                        $caption  = $hero_image['caption'];
                        $title  = $hero_image['title'];
                        $image_main = $hero_image['sizes']['gallery'];
                        if ($layout == 'm-banner--home--left') {
                            echo '<img src="' . $image_main . '" class="image-hero image-id-' . $ID . '" alt="' . ($caption ? $caption : $title) . '"  data-srcset="' . $image_main . ' 2560w"/>';
                        } else if ($layout == 'm-banner--home--right') {
                            echo '<img src="' . $image_main . '" class="image-hero image-id-' . $ID . '" alt="' . ($caption ? $caption : $title) . '"  data-srcset="' . $image_main . ' 2560w"/>';
                        } else {
                            echo acf_picture($hero_image);
                        }
                        ?>
                    </figure>
                    <div class="hero-slider--imgs" style="background-image:url(<?php echo $image_main?>) ;"></div>
                    <div class="hero-slider-container background-color<?php the_sub_field('background_color'); ?>">
                        <div class="hero-slider-content ">
                                <h1 class="font-color<?php the_sub_field('title_color'); ?>"><?php the_sub_field('page_hero_heading'); ?></h1>
                                <p class="font-color<?php the_sub_field('text_color'); ?>"><?php the_sub_field('page_hero_content'); ?></p>

                                    <?php
                                    $link = get_field('button');
                                    if ($link) :
                                        $link_url = $link['url'];
                                        $link_title = $link['title'];
                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                    <a class="a-btn a-btn--wide" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" title="<?php echo esc_html($link_title); ?>"><?php echo esc_html($link_title); ?></a>
                                <?php endif; ?>
                        </div>
                    
                    </div>
                    
                </section>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>
<?php endif; ?>