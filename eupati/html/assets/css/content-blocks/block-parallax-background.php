<?php
$parallax_image = get_sub_field('parallax_image');
if ($parallax_image) {
?>
<div class="e-with-fixed-bg">
	  <div class="bg-wrap">
		 <div class="bg" style="background-image: url(<?php print $parallax_image; ?>);"></div>
	  </div>
	</div>
<?php } ?>