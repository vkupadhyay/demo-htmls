<?php if(get_sub_field('image')):?>
<section class="m-entity m-entity__img-text background-color<?php echo get_sub_field('background_color') ?>">
  <div class="grid justify-center">
    <div class="col-sm-12 col-md-10 col-lg-8">

      <?php include $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/eupati/templates/image-text.php';?>
    
    </div>
  </div>
</section>
<?php endif;?>