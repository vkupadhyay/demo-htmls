<?php if (get_sub_field('title')) : ?>

    <section class="m-entity m-entity__gallery background-color<?php echo get_sub_field('background_color') ?>">
        <div class="grid justify-center">
            <div class="col-sm-12 col-md-10 col-lg-8">

                <?php if (get_sub_field('title')) { ?>
                    <h2 class="a-title a-title--bottom font-color<?php echo get_sub_field('title_color') ?> line-color<?php echo get_sub_field('line_color') ?>">
                        <?php the_sub_field('title'); ?>
                    </h2>
                <?php } ?>

                <?php $news_articles = get_sub_field('news_articles');
                if ($news_articles) : ?>
                    <div class="m-entity__gallery-inner new-gallery">

                        <?php foreach ($news_articles as $article) : ?>
                            <?php $image = $article['image'];?>
                            <?php $article_link = $article['article_link']; ?>
                            <a href="<?php echo esc_url($article_link['url']); ?>" title="<?php print $article_link['title']; ?>">
                                <img data-src="<?php print $image['sizes']['gallery']; ?>" alt="<?php print $image['alt']; ?>" class="lazy">
                            </a>
                        <?php endforeach; ?>

                    </div>
                <?php endif; ?>

            </div>
        </div>
    </section>

<?php else : ?>
    <?php
    $the_query = new WP_Query(array(
        'posts_per_page' => 3,
    ));
    ?>
    <?php if (0 && $the_query->have_posts()) : ?>
        <section class="a-angle a-angle--bottom background-color<?php echo get_sub_field('background_color') ?>">
            <div class="grid justify-center">
                <div class="col-sm-12 col-lg-10">

                    <?php if (get_sub_field('title')) { ?>
                        <h2 class="a-title a-title--bottom font-color<?php echo get_sub_field('title_color') ?> line-color<?php echo get_sub_field('line_color') ?>">
                            <?php the_sub_field('title'); ?>
                        </h2>
                    <?php } ?>

                    <div class="m-news m-news--3">
                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <?php include $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/eupati/teasers/news.php'; ?>
                        <?php endwhile; ?>
                    </div>

                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
<?php endif; ?>