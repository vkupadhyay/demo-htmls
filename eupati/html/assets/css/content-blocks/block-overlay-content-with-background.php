<?php if (get_sub_field('overlay_title') || get_sub_field('overlay_content') || get_sub_field('background_image')) : ?>
    <section class="full-img-section">
        <div class="full-img-box">
            <?php
            $background_image = get_sub_field('background_image');
            if ($background_image) { ?>
            <figure>  <img src="<?php echo $background_image['url']; ?>" alt="<?php echo $background_image['alt']; ?>" /></figure>
            <?php } ?>
        </div>
        <div class="full-img-container">
            <div class="grid justify-center">
                <div class="col-sm-8">
                    <div class="full-img-content a-align--center background-color<?php echo get_sub_field('background_color') ?>">
                        <?php if (get_sub_field('overlay_title')) { ?>
                            <h2 class="font-color<?php echo get_sub_field('title_color') ?>">
                                <?php the_sub_field('overlay_title'); ?>
                            </h2>
                        <?php } ?>
                        <div class="text-paragraph font-color<?php echo get_sub_field('text_color') ?>">
                            <?php the_sub_field('overlay_content'); ?>
                        </div>

                        <?php
                        $link = get_sub_field('overlay_cta');
                        $button_styles =  get_sub_field('button_style');
                        if ($link) {
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <div class="full-img-button">
                                <a href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" target="<?php echo $link_target; ?>" class="a-btn a-btn--wide <?php echo getButtonClass($button_styles);?>"><?php echo $link_title; ?></a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>