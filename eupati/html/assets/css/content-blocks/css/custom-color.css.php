<?php header("Content-type: text/css", true);?>
.m-entity {
padding: 30px 0;
margin: 0;
}

.o-footer__top {
padding: 0;
}

a.a-footer-logo svg {
max-width: 100px;
}

.m-entity__gallery:before{display: none;}
.m-banner--home--right .hero-slider-container, .m-banner--page--right .hero-slider-container{background-color:#f0b72f ;}
.m-banner--home--left .hero-slider-container, .m-banner--page--left .hero-slider-container{background-color:#00818f;}



<?php
  $field['choices']['#f0b72f'] = 'Yello(Amber): #f0b72f';
  $field['choices']['#1b4178'] = 'Dark Blue: #1b4178';
  $field['choices']['#007cbf'] = 'Light Blue: #007cbf';
  $field['choices']['#00818f'] = 'Aqua : #00818f';
  $field['choices']['#e3e6f1'] = 'Light Gray: #e3e6f1';
  $field['choices']['#3e3953'] = 'Purple grey: #3e3953';


  $field['choices']['#00a4b6'] = 'Teal Green: #00A4b6';
  $field['choices']['#005f77'] = 'Emerald Green: #005F77';
  $field['choices']['#d3e2f4'] = 'Blue tint Light 1: #D3E2F4';
  $field['choices']['#daeef2'] = 'Teal tint Light 2: #DAEEF2';

  $field['choices']['#191919'] = 'Body Text(90% black): #191919';
  $field['choices']['#333433'] = 'Larger Text(70% black): #333433';

  $field['choices']['#ffffff'] = 'White: #FFFFFF';

foreach ($field['choices'] as $k => $v) :  $k2 = str_replace('#', '', $k) ?>
    .background-color\<?php echo $k ?> {
    background-color: <?php echo $k ?> !important;
    }

    .full-video-content.background-color\<?php echo $k ?> {
    background-color: <?php echo $k ?>90 !important;
    }
    .full-img-section .full-img-container .full-img-content.background-color\<?php echo $k ?> {
    background-color: <?php echo $k ?>90 !important;
    }
    .line-color\<?php echo $k ?>.a-title:after {
    background-color: <?php echo $k ?> !important;
    }

    .font-color\<?php echo $k ?> {
    color: <?php echo $k ?> !important;
    }

    .button_color-\<?php echo $k ?> {
    background-color: <?php echo $k ?> !important;
    }

    .button_text_color-\<?php echo $k ?> {
    color: <?php echo $k ?> !important;
    }


    .button_color_hover-\<?php echo $k ?>:hover {
    background-color: <?php echo $k ?> !important;
    }

    .button_text_color_hover-\<?php echo $k ?>:hover {
    color: <?php echo $k ?> !important;
    }


    .a-btn--bbdrbtm.button_text_color-\<?php echo $k ?>::after{
    border-color: <?php echo $k ?> !important;
    }
    .a-btn--bbdrbtm.button_text_color_hover-\<?php echo $k ?>:hover::after{
    border-color: <?php echo $k ?> !important;
    }
    .newsletter-form-button_color-\<?php echo $k ?> input[type=submit]{
    background-color: <?php echo $k ?> !important;
    }

    .newsletter-form-button_text_color-\<?php echo $k ?> input[type=submit]{
    color: <?php echo $k ?> !important;
    }


    .newsletter-form-button_color_hover-\<?php echo $k ?> input[type=submit]:hover {
    background-color: <?php echo $k ?> !important;
    }

    .newsletter-form-button_text_color_hover-\<?php echo $k ?> input[type=submit]:hover {
    color: <?php echo $k ?> !important;
    }

    .promotion-btn.button_text_color-\<?php echo $k ?> i svg path {
    fill: <?php echo $k ?>;
    }

    .promotion-btn.button_text_color_hover-\<?php echo $k ?>:hover i svg path {
    fill: <?php echo $k ?>;
    }

    .m-pullout__content .button_text_color-\<?php echo $k ?> i svg path {
    fill: <?php echo $k ?>;
    }

    .m-pullout__content .button_text_color_hover-\<?php echo $k ?>:hover i svg path {
    fill: <?php echo $k ?>;
    }

    .video-btn.play-button_color-\<?php echo $k ?> .cls-1 {
    fill: <?php echo $k ?>;
    }

    .video-btn.play-button_text_color-\<?php echo $k ?> .cls-2 {
    fill: <?php echo $k ?>;
    }

    .video-btn.play-button_color_hover-\<?php echo $k ?>:hover .cls-1 {
    fill: <?php echo $k ?>;
    }

    .video-btn.play-button_text_color_hover-\<?php echo $k ?>:hover .cls-2 {
    fill: <?php echo $k ?>;
    }

    .slick-slide-arrow--<?php echo $k2 ?> .slick-arrow.slick-prev {
    background: url(/wp-content/themes/eupati-main/assets/img/arrows/arrow-left-<?php echo $k2; ?>.svg) no-repeat;
    background-size: contain;
    }
    .slick-slide-arrow--<?php echo $k2 ?> .slick-arrow.slick-next {
    background: url(/wp-content/themes/eupati-main/assets/img/arrows/arrow-right-<?php echo $k2 ?>.svg) no-repeat;
    background-size: contain;
    }
    .slick-slide-arrow-hover--<?php echo $k2 ?> .slick-arrow:hover.slick-next {
    background: url(/wp-content/themes/eupati-main/assets/img/arrows/arrow-right-<?php echo $k2 ?>.svg) no-repeat;
    background-size: contain;
    }
    .slick-slide-arrow-hover--<?php echo $k2 ?> .slick-arrow:hover.slick-prev {
    background: url(/wp-content/themes/eupati-main/assets/img/arrows/arrow-left-<?php echo $k2 ?>.svg) no-repeat;
    background-size: contain;
    }


    .m-entity__quote-sm.quote-color\<?php echo $k ?> blockquote:after,
    .m-entity__quote-sm.quote-color\<?php echo $k ?> blockquote:before,
    .m-entity__quote.quote-color\<?php echo $k ?> blockquote:after,
    .m-entity__quote.quote-color\<?php echo $k ?> blockquote:before {
    background-image: url(/wp-content/themes/eupati/content-blocks/load_svg.php?name=quote&color=<?php echo $k2 ?>);
    }
<?php endforeach; ?>