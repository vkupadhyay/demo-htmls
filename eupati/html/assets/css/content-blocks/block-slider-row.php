<?php if (get_sub_field('show_hero_slider')) : ?>
    <?php if (have_rows('home_hero_list')) : ?>
        <link href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet" />
        <section class="hero-section hero-slider-section">
            <div class="hero-section-containar hero-slider">

                <?php while (have_rows('home_hero_list')) : the_row(); ?>
                    <?php $hero_image = get_sub_field('hero_image'); ?>
                    <?php $layout = get_sub_field('slide_content_layout'); ?>
                    <?php if ($hero_image) : ?>
                        <div class="hero-slider-item hero-section-item">
                            <div class="m-banner m-banner--home <?php the_sub_field('slide_content_layout'); ?>">
                                <figure>
                                    <?php
                                     $ID  = $hero_image['ID'];
                                     $caption  = $hero_image['caption'];
                                     $title  = $hero_image['title'];
                                     $image_main = $hero_image['sizes']['gallery'];
                                    if ($layout == 'm-banner--home--left') {
                                        echo '<img src="' . $image_main . '" class="image-hero image-id-' . $ID . '" alt="' . ($caption ? $caption : $title) . '"  data-srcset="' . $image_main . ' 2560w"/>';
                                    } else if ($layout == 'm-banner--home--right') {
                                        echo '<img src="' . $image_main . '" class="image-hero image-id-' . $ID . '" alt="' . ($caption ? $caption : $title) . '"  data-srcset="' . $image_main . ' 2560w"/>';
                                    } else {
                                        echo acf_picture($hero_image);
                                    }
                                    ?>
                                </figure>
                                
                                <div class="hero-slider--imgs" style="background-image:url(<?php echo $image_main?>) ;"></div>
                                <div class="hero-slider-container background-color<?php the_sub_field('background_color'); ?>">
                                    <div class="hero-slider-content">
                                        <h1 class="font-color<?php the_sub_field('title_color'); ?>"><?php the_sub_field('hero_heading'); ?></h1>
                                        <div class="hero-slider-txt font-color<?php the_sub_field('text_color'); ?>">
                                            <?php the_sub_field('hero_content'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endwhile;  ?>
            </div>
        </section>
    <?php endif; ?>
<?php endif; ?>