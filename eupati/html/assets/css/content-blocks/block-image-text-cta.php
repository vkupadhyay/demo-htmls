<section class="help-content-section section-pd-tb  background-color<?php echo get_sub_field('background_color') ?> <?php if (get_sub_field('image_position')) { echo 'image-position-' . get_sub_field('image_position'); } ?>">
    <div class="grid justify-center">
        <div class="<?php echo get_sub_field('layout_width')=='compact'?'col-sm-12 col-md-8 col-lg-6':'col-sm-12 col-md-10 col-lg-10';?>">
        <?php  $button_styles =  get_sub_field('button_style');?>
            <div class="custom-row">
                <div class="help-content-img">
                    <?php $link = get_sub_field('image_link');
                    if ($link) {
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                        <a href="<?php echo $link_url; ?>" title="Open link" target="<?php echo $link_target; ?>" class="m-img-text__link">
                        <?php
                    }
                        ?>

                        <?php $image = get_sub_field('image'); ?>
                        <figure>
                            <img src="<?php print $image['sizes']['imgtext']; ?>" alt="<?php print $image['alt']; ?>" />
                        </figure>

                        <?php if ($link) { ?>
                        </a>
                    <?php } ?>
                </div>
                <div class="help-content-txt">
                    <h2 class="font-color<?php echo get_sub_field('title_color') ?>"> <?php the_sub_field('heading'); ?></h2>
                    <div class="text-desc font-color<?php echo get_sub_field('text_color') ?>">
                    <?php the_sub_field('copy'); ?>
                    
                    <?php if ($link) : ?>
                        <a href="<?php echo $link_url; ?>" title="Open link" target="<?php echo $link_target; ?>" class="a-btn a-btn--wide <?php echo getButtonClass($button_styles);?>"><?php echo  $link_title; ?></a>
                    <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>