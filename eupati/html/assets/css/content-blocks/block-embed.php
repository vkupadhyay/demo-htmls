<section class="m-entity m-entity__embed background-color<?php echo get_sub_field('background_color') ?>">
  <div class="grid justify-center">
    <div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6 font-color<?php echo get_sub_field('text_color') ?>">

      <?php the_sub_field('embed'); ?>
      
    </div>
  </div>
</section>