<?php if (get_sub_field('copy_content_title') || get_sub_field('copy_content_text')) {  $button_styles =  get_sub_field('button_style');?>

	<section class="simple-text-section light-cyan-bg section-pd-tb background-color<?php echo get_sub_field('background_color') ?>">
		<div class="grid justify-center">
			<div class="<?php echo get_sub_field('text_width')=='compact'?'col-sm-8 col-lg-6 col-xlg-4':"col-sm-8"; ?>">
				<div class="section-hd a-align--center">
					<?php if (get_sub_field('copy_content_title')) { ?>
						<h2 class="font-color<?php echo get_sub_field('title_color') ?>">
							<?php the_sub_field('copy_content_title'); ?>
						</h2>
					<?php } ?>

					<div class="text-paragraph font-color<?php echo get_sub_field('text_color') ?>">
						<?php the_sub_field('copy_content_text'); ?>
					</div>
					<?php
						$link = get_sub_field('copy_cta');
						if ($link) {
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>

							<div class="bottom-btn">
								<a href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" target="<?php echo $link_target; ?>" class="a-btn a-btn--wide <?php echo getButtonClass($button_styles);?>"><?php echo $link_title; ?></a>
							</div>
						<?php } ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>