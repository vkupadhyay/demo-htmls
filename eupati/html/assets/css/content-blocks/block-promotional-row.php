<?php if (get_field('promotional_area_group') && get_sub_field('show_promotional')) : $button_styles =  get_sub_field('button_style'); ?>

    <?php $button_styles['button_text_color'] = isset($button_styles['button_text_color']) && $button_styles['button_text_color'] ? $button_styles['button_text_color'] : '#1b4178' ?>
    <?php $button_styles['button_text_color_hover'] = isset($button_styles['button_text_color_hover']) && $button_styles['button_text_color_hover'] ? $button_styles['button_text_color_hover'] : '#ffffff' ?>
    <?php if (have_rows('promotional_area_group')) : while (have_rows('promotional_area_group')) : the_row(); ?>
            <section class="web-promotion-section background-color<?php the_sub_field('background_color'); ?>">
                <div class="grid justify-center">
                    <div class="col-sm-10">
                        <div class="custom-row">
                            <div class="promostion-left font-color<?php the_sub_field('text_color'); ?>">
                                <?php the_sub_field('promotional_text'); ?>
                            </div>

                            <div class="promostion-right">
                                <?php
                                $link = get_sub_field('promotional_cta');
                                if ($link) {
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    $svg = '<i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.4 18.4"><path d="M9.2 0L6.4 2.8l4.3 4.4H0v4h10.7l-4.3 4.4 2.8 2.8 9.2-9.2z"/></svg></i>';

                                    echo $downloadLink = '<a href="' . $link_url . '" title="' . $link_title . '" target="' . $link_target . '" class="promotion-btn ' . getButtonClass($button_styles) . '">' . $link_title . $svg . '</a>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endwhile; ?>
    <?php endif; ?>
<?php endif; ?>