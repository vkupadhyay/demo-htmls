<?php if (get_sub_field('left_content_group') || get_sub_field('right_content_group')) : ?>

	<?php
	$parent_bg_color = get_sub_field('background_color'); ?>
	<section class="help-section white-bg section-pd-tb background-color<?php echo get_sub_field('background_color') ?>">
		<div class="grid justify-center">
			<div class="col-sm-10">
				<div class="section-hd a-align--center">
					<?php if (get_sub_field('help_title')) { ?>
						<h2 class="font-color<?php echo get_sub_field('title_color') ?>">
							<?php the_sub_field('help_title'); ?>
						</h2>
					<?php } ?>
					<div class="text-paragraph font-color<?php echo get_sub_field('text_color') ?>">
						<?php the_sub_field('help_copy'); ?>
					</div>
				</div>

				<div class="help-container">
					<div class="custom-row">
						<?php if (get_sub_field('left_content_group')) : ?>
							<?php while (have_rows('left_content_group')) : the_row(); ?>

								<div class="organization-box" <?php if (get_sub_field('background_color')) {
																	echo 'style="background-color: ' . get_sub_field('background_color') . ';' . ($parent_bg_color ? "border-right:3px " . $parent_bg_color . " solid" : '') . '"';
																} else if ($parent_bg_color) {
																	echo 'style="border-right-color: ' . $parent_bg_color . ';"';
																} ?>>
									<?php if (get_sub_field('left_content_title')) { ?>
										<h4 class="font-color<?php echo get_sub_field('title_color') ?>">
											<?php the_sub_field('left_content_title'); ?>
										</h4>
									<?php } ?>

									<?php if (have_rows('left_content_rows')) : ?>
										<ul class="colm colm--1">

											<?php while (have_rows('left_content_rows')) : the_row(); ?>

												<li>
													<i><img src="<?php echo get_sub_field('item_icon')['url']; ?>" alt="" /></i>
													<div class="match-height">
														<h4 class="font-color<?php echo get_sub_field('title_color') ?>"><?php the_sub_field('item_title'); ?></h4>
														<div class="text-paragraph font-color<?php echo get_sub_field('text_color') ?>">
															<?php the_sub_field('item_text'); ?>
														</div>
													</div>
													<?php
													$link = get_sub_field('item_cta');
													if ($link) {
														$link_url = $link['url'];
														$link_title = $link['title'];
														$link_target = $link['target'] ? $link['target'] : '_self';
													?>

														<div class="bottom-btn">
															<a href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" target="<?php echo $link_target; ?>" class="a-btn a-btn--alt button-color<?php echo get_sub_field('button_text_color') ?>"><?php echo $link_title; ?></a>
														</div>
													<?php } ?>

												</li>
											<?php endwhile; ?>
										</ul>
									<?php endif; ?>

								</div>
							<?php endwhile; ?>
						<?php endif; ?>
						<?php if (get_sub_field('right_content_group')) : ?>

							<?php while (have_rows('right_content_group')) : the_row(); ?>

								<div class="individual-box background-color<?php echo get_sub_field('background_color') ?>">
									<?php if (get_sub_field('right_content_title')) { ?>
										<h4 class="font-color<?php echo get_sub_field('title_color') ?>">
											<?php the_sub_field('right_content_title'); ?>
										</h4>
									<?php } ?>

									<?php if (have_rows('right_content_rows')) : ?>
										<ul class="colm colm--2">

											<?php while (have_rows('right_content_rows')) : the_row(); ?>
												<li>
													<i><img src="<?php echo get_sub_field('item_icon')['url']; ?>" alt="" /></i>
													<div class="match-height">
														<h4 class="font-color<?php echo get_sub_field('title_color') ?>"><?php the_sub_field('item_title'); ?></h4>
														<div class="text-paragraph font-color<?php echo get_sub_field('text_color') ?>">
															<?php the_sub_field('item_text'); ?>
														</div>
													</div>
													<?php
													$link = get_sub_field('item_cta');
													if ($link) {
														$link_url = $link['url'];
														$link_title = $link['title'];
														$link_target = $link['target'] ? $link['target'] : '_self';
													?>

														<div class="bottom-btn">
															<a href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" target="<?php echo $link_target; ?>" class="a-btn a-btn--alt button-color<?php echo get_sub_field('button_text_color') ?>"><?php echo $link_title; ?></a>
														</div>
													<?php } ?>
												</li>
											<?php endwhile; ?>
										</ul>
									<?php endif; ?>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>