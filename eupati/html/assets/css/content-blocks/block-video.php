<section class="m-entity m-entity__video background-color<?php echo get_sub_field('background_color') ?>">

  <div class="grid justify-center">
    <div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6">

      <div class="m-entity__video-inner">
        <?php if (get_sub_field('title')) { ?>
          <h2 class="a-title a-title--bottom font-color<?php echo get_sub_field('title_color') ?> line-color<?php echo get_sub_field('line_color') ?>">
            <?php the_sub_field('title'); ?>
          </h2>
        <?php } ?>

        <?php if (get_sub_field('copy')) { ?>
          <div class="video-copy-text font-color<?php echo get_sub_field('text_color') ?>">
            <?php the_sub_field('copy'); ?> 
          </div>
        <?php } ?>


        <?php if (get_sub_field('video')) { ?>
          <div class="video-container">
            <?php the_sub_field('video'); ?>
          </div>
        <?php } ?>
      </div>

    </div>
  </div>

  <?php /*if (get_sub_field('copy')) {?>
    <div class="grid justify-center font-color<?php echo get_sub_field('text_color')?>">
      <div class="col-sm-8 col-md-6 col-xlg-4">
        <?php the_sub_field('copy'); ?>
      </div>
    </div>
  <?php } */ ?>

</section>