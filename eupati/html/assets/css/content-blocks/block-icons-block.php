<?php if (get_sub_field('icons_row') || get_sub_field('icon_content')) : ?>


	<section class="blue-support-info-section yellow-bg section-pd-tb background-color<?php echo get_sub_field('background_color') ?>">
		<div class="grid justify-center">
			<div class="col-sm-11">
				<div class="section-hd a-align--center">
					<?php if (get_sub_field('icon_heading')) { ?>
						<h2 class="font-color<?php echo get_sub_field('title_color') ?>">
							<?php the_sub_field('icon_heading'); ?>
						</h2>
					<?php } ?>
					<div class="text-paragraph font-color<?php echo get_sub_field('text_color') ?>">
						<?php the_sub_field('icon_content'); ?>

					</div>
				</div>
				<div class="support-info-box a-align--center">
					<?php if (get_sub_field('icons_row')) : ?>
						<ul class="colm colm-no-btn colm--<?php the_sub_field('items_to_show'); ?>">

							<?php while (have_rows('icons_row')) : the_row(); ?>
								<li>
									<div class="colm-width">
								<?php
								$icon_image = get_sub_field('icon_image');
								if ($icon_image) { ?>
									<i><img src="<?php echo $icon_image['url']; ?>" alt="<?php echo $icon_image['alt']; ?>" /></i>
								<?php } ?>
									<h4 class="font-color<?php echo get_sub_field('title_color') ?>"><?php the_sub_field('icon_title'); ?></h4>
								</div>
								</li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>