<?php if(get_sub_field('show_newsletter') == 1){ $button_styles = get_sub_field('button_style')?>
  <div class="a-newsletter-wrapper a-newsletter-wrapper--page background-color<?php echo get_sub_field('background_color') ?> font-color<?php echo get_sub_field('text_color') ?> <?php echo getButtonClass($button_styles,'newsletter-form-');?>" id="newsletter-form-<?php echo $currentRow =	get_row_index();?>">
    <?php echo do_shortcode( '[ic_add_posts id="303" post_type="newsletter" template="single-newsletter.php"]' ); ?>
    <span class="a-newsletter-wrapper--page__stripe--ridoff"></span>
  </div>
  <style>
    #newsletter-form-<?php echo $currentRow ;?> .m-newsletter{
      border-color: <?php echo get_sub_field('line_color')?get_sub_field('line_color'):'#007cbf' ?>;
      background-color: <?php echo get_sub_field('background_color_form')?get_sub_field('background_color_form'):'#FFFFFF' ?>;
    }

    #newsletter-form-<?php echo $currentRow ;?> .m-newsletter h3{
      color: <?php echo get_sub_field('title_color')?get_sub_field('title_color'):'#1b4178' ?>;
    }

    #newsletter-form-<?php echo $currentRow ;?> .m-newsletter h3{
      color: <?php echo get_sub_field('title_color')?get_sub_field('title_color'):'#1b4178' ?>;
    }
  </style>
<?php } ?>
