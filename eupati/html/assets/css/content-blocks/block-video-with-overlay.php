<?php if (get_sub_field('video')) : ?>

    <script>
        jQuery(document).ready(function($) {
            $(".video-btn").click(function() {
                $(".video-container-box iframe")[0].src += "?autoplay=1";
            });
        });
    </script>

    <section class="full-videos-section videos-overlay">
        <div class="full-videos-box">
            <?php if (get_sub_field('video')) { ?>
                <div class="video-container-box">
                    <?php the_sub_field('video'); ?>
                </div>
            <?php } ?>
        </div>
        <div class="full-video-container">
            <div class="grid justify-center">
                <div class="col-sm-8">
                    <div class="full-video-content a-align--center background-color<?php echo get_sub_field('background_color') ?>">
                        <?php if (get_sub_field('title')) { ?>

                            <h2 class="font-color<?php echo get_sub_field('title_color') ?>"><?php the_sub_field('title'); ?></h2>
                        <?php } ?>

                        <div class="text-paragraph font-color<?php echo get_sub_field('text_color') ?>">
                            <?php the_sub_field('copy'); ?>

                        </div>
                        <div class="full-img-button">
                            <?php $button_styles =  get_sub_field('button_style');
                            $button_styles['button_color'] = $button_styles['button_color']?$button_styles['button_color']:'#f0b72f';
                            $button_styles['button_text_color'] = $button_styles['button_text_color']?$button_styles['button_text_color']:'#ffffff';
                            $button_styles['button_color_hover'] = $button_styles['button_color_hover']?$button_styles['button_color_hover']:'#1b4178';
                            $button_styles['button_text_color_hover'] = $button_styles['button_text_color_hover']?$button_styles['button_text_color_hover']:'#ffffff';
                            ?>
                            <button class="video-btn <?php echo getButtonClass($button_styles, 'play-');?>">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.67 135.67">
                              
                                    <g id="Layer_2" data-name="Layer 2">
                                        <g id="ARTWORK">
                                            <circle class="cls-1" cx="67.84" cy="67.84" r="67.84" />
                                            <polygon class="cls-2" points="55.86 42.56 55.86 94.44 94.44 67.83 55.86 42.56" />
                                        </g>
                                    </g>
                                </svg>
                             
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>