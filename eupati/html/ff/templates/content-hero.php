<?php if (have_rows('home_hero_list')) : ?>

	<link href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet" />


	<section class="hero-section hero-slider-section">
		<div class="hero-section-containar hero-slider">


			<?php while (have_rows('home_hero_list')) : the_row(); ?>
				<?php $hero_image = get_sub_field('hero_image'); ?>
				<?php if ($hero_image) : ?>
					<div class="hero-slider-item hero-section-item">
					<figure>
	<img
		src="https://eupati.eu/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2021/12/Hero-image-NEW-FINAL-shorter-scaled.jpg.webp"
		class="attachment-post-thumbnail size-post-thumbnail lazy wp-post-image"
		alt=""
		srcset=""
		sizes="(max-width: 2560px) 100vw, 2560px"
		data-srcset="https://eupati.eu/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2021/12/Hero-image-NEW-FINAL-shorter-scaled.jpg.webp 2560w,  https://eupati.eu/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2021/12/Hero-image-NEW-FINAL-shorter-250x92.jpg.webp 250w,  https://eupati.eu/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2021/12/Hero-image-NEW-FINAL-shorter-700x257.jpg.webp 700w,  https://eupati.eu/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2021/12/Hero-image-NEW-FINAL-shorter-768x282.jpg.webp 768w,  https://eupati.eu/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2021/12/Hero-image-NEW-FINAL-shorter-1536x565.jpg.webp 1536w,  https://eupati.eu/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2021/12/Hero-image-NEW-FINAL-shorter-2048x753.jpg.webp 2048w,  https://eupati.eu/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2021/12/Hero-image-NEW-FINAL-shorter-120x44.jpg.webp 120w,  https://eupati.eu/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2021/12/Hero-image-NEW-FINAL-shorter-468x172.jpg.webp 468w,  https://eupati.eu/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2021/12/Hero-image-NEW-FINAL-shorter-1000x368.jpg.webp 1000w"
	/>
</figure>
<!--w=1024 ,h= 500, w=768 ,h= 449,  w=2560 ,h= 941-->
						<!-- <picture>
						    <source media="(min-width:992px) and (max-width:1024px)" srcset="https://eupati.oxypay.in/wp-content/themes/eupati-main/assets/img/Hero-image-NEW-FINAL-shorter-scaled-ipad-l.jpg">
							<source media="(min-width:768px) and (max-width:991px)" srcset="https://eupati.oxypay.in/wp-content/themes/eupati-main/assets/img/Hero-image-NEW-FINAL-shorter-scaled-ipad-p.jpg"
							<img src="https://eupati.oxypay.in/wp-content/themes/eupati-main/assets/img/Hero-image-NEW-FINAL-shorter-scaled.webp" alt="hero" /> 

							
						</picture> -->

						<div class="hero-slider-container">
							<div class="hero-slider-content">
								<h1><?php the_sub_field('hero_heading'); ?></h1>
								<div class="hero-slider-txt">
									<?php the_sub_field('hero_content'); ?>
								</div>
							</div>
						</div>
					</div>
				<?php
				endif;
				?>
			<?php
			endwhile;
			?>
		</div>
	</section>
<?php
endif;
?>