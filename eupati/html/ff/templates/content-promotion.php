<?php if (get_field('promotional_area_group')) : ?>

    <?php if (have_rows('promotional_area_group')) : while (have_rows('promotional_area_group')) : the_row(); ?>
            <?php if (get_sub_field('show_promotional')) : ?>

                <section class="web-promotion-section">
                    <div class="grid justify-center">
                        <div class="col-sm-10">
                            <div class="custom-row">
                                <div class="promostion-left">
                                    <?php the_sub_field('promotional_text'); ?>
                                </div>

                                <div class="promostion-right">
                                    <?php
                                    $link = get_sub_field('promotional_cta');
                                    if ($link) {
                                        $link_url = $link['url'];
                                        $link_title = $link['title'];
                                        $link_target = $link['target'] ? $link['target'] : '_self';

                                        echo $downloadLink = '<a href="' . $link_url . '" title="' . $link_title . '" target="' . $link_target . '" class="promotion-btn">' . $link_title . '</a>';
                                    }
                                    ?>
                                    <!-- <button class="promotion-btn">Click here for
                                        more information</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
    <?php
        endwhile;
    endif;
    ?>
<?php endif; ?>