<?php if (has_post_thumbnail()) : ?>
    <section class="hero-section">
        <div class="hero-section-containar">
            <div class="hero-section-item">
                <figure>
                    <?php the_post_thumbnail('banner'); ?>
                </figure>
                <div class="hero-slider-container">
                    <div class="hero-slider-content">
                        <h1><?php the_title(); ?></h1>
                        <div class="hero-slider-txt">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
<?php endif; ?>