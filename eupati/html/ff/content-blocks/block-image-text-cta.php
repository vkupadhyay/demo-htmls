<section class="m-entity m-entity__img-text entity-image-text-cta help-content-section section-pd-tb">
    <div class="grid justify-center">
        <div class="col-sm-12 col-md-10 col-lg-10">

            <div class="m-img-text">
                <?php $link = get_sub_field('image_link');
                if ($link) {
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                    <a href="<?php echo $link_url; ?>" title="Open link" target="<?php echo $link_target; ?>" class="m-img-text__link">
                    <?php
                }
                    ?>

                    <?php $image = get_sub_field('image'); ?>
                    <figure>
                        <img src="<?php print $image['sizes']['imgtext']; ?>" alt="<?php print $image['alt']; ?>" />
                    </figure>

                    <?php if ($link) { ?>
                    </a>
                <?php } ?>

                <div class="m-img-text__copy">
                    <h2> <?php the_sub_field('heading'); ?></h2>
                    <?php the_sub_field('copy'); ?>
                    <?php if ($link) : ?>
                        <a href="<?php echo $link_url; ?>" title="Open link" target="<?php echo $link_target; ?>" class="a-btn a-btn--wide"><?php echo  $link_title; ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>