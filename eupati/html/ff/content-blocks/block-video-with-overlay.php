<script>
     jQuery(document).ready(function($){
        $(".video-btn").click(function(){
      $(".video-container iframe")[0].src += "?autoplay=1";
     });
     });
</script>

<section class="full-img-section videos-overlay">
    <div class="full-img-box">
        <?php if (get_sub_field('video')) { ?>
            <div class="video-container">
                <?php the_sub_field('video'); ?>
            </div>
        <?php } ?>
    </div>
    <div class="full-img-container">
        <div class="grid justify-center">
            <div class="col-sm-10">
                <div class="full-img-content a-align--center">
                    <?php if (get_sub_field('title')) { ?>

                        <h2><?php the_sub_field('title'); ?></h2>
                    <?php } ?>

                    <div class="text-paragraph">
                        <?php the_sub_field('copy'); ?>

                    </div>
                    <div class="full-img-button">
                        <button class="video-btn"><img src="<?php echo  site_url() ?>/wp-content/themes/eupati-main/assets/img/video-icon.svg" alt="" /> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>