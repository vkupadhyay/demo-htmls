<?php if (get_sub_field('icon_and_text_row') || get_sub_field('icon_content')) : ?>

	<section class="blue-support-info-section blue-bg section-pd-tb" <?php if (get_sub_field('background_color')) {
																			echo 'style="background-color: ' . get_sub_field('background_color') . ';"';
																		} ?>>
		<div class="grid justify-center">
			<div class="col-sm-10">
				<div class="section-hd a-align--center">
					<?php if (get_sub_field('icon_heading')) { ?>
						<h2>
							<?php the_sub_field('icon_heading'); ?>
						</h2>
					<?php } ?>
					<div class="text-paragraph">
						<?php the_sub_field('icon_content'); ?>
					</div>
				</div>
				<div class="support-info-box a-align--center">
					<?php if (get_sub_field('icon_and_text_row')) : ?>
						<ul class="colm colm--<?php the_sub_field('items_to_show'); ?>">

							<?php while (have_rows('icon_and_text_row')) : the_row(); ?>
								<li>
								<?php $icon_image = get_sub_field('icon_image');
								if ($icon_image) { ?>
									<i><img src="<?php echo $icon_image['url']; ?>" alt="<?php echo $icon_image['alt']; ?>" /></i>
								<?php } ?>
									<h4><?php the_sub_field('icon_title'); ?></h4>
									<div class="text-paragraph">
										<?php the_sub_field('icon_copy'); ?>
									</div>

									<?php
									$link = get_sub_field('icon_cta');
									if ($link) {
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self';
									?>
										<div class="bottom-btn">
											<a href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" target="<?php echo $link_target; ?>" class="a-btn a-btn--alt"><?php echo $link_title; ?></a>
										</div>
									<?php } ?>

								</li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>