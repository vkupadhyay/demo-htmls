<section class="m-entity m-entity__video">

  <div class="grid justify-center">
    <div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6">

      <div class="m-entity__video-inner">
        <?php if (get_sub_field('title')){?>
          <h2 class="a-title a-title--bottom">
            <?php the_sub_field('title'); ?>
          </h2>
        <?php }?>

        <?php if (get_sub_field('video')) {?>
          <div class="video-container">
            <?php the_sub_field('video'); ?>
          </div>
        <?php } ?>
      </div>

    </div>
  </div>

  <?php if (get_sub_field('copy')) {?>
    <div class="grid justify-center">
      <div class="col-sm-8 col-md-6 col-xlg-4">
        <?php the_sub_field('copy'); ?>
      </div>
    </div>
  <?php } ?>

</section>