<section class="m-entity m-entity__embed">
  <div class="grid justify-center">
    <div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6">

      <?php the_sub_field('embed'); ?>
      
    </div>
  </div>
</section>