<?php if (get_sub_field('articles_row') || get_sub_field('icon_content')) : ?>

	<section class="white-support-info-section white-bg section-pd-tb" <?php if (get_sub_field('background_color')) {
																			echo 'style="background-color: ' . get_sub_field('background_color') . ';"';
																		} ?>>
		<div class="grid justify-center">
			<div class="col-sm-10">
				<div class="section-hd a-align--center">
					<?php if (get_sub_field('heading')) { ?>
						<h2>
							<?php the_sub_field('heading'); ?>
						</h2>
					<?php } ?>
					<div class="text-paragraph">
						<?php the_sub_field('content'); ?>

					</div>
				</div>
				<div class="support-info-box">
					<?php if (get_sub_field('articles_row')) : ?>
						<ul class="colm colm--<?php the_sub_field('items_to_show'); ?>">

							<?php while (have_rows('articles_row')) : the_row(); ?>

								<li>
									<div class="support-info--img">
										<?php
										$item_image = get_sub_field('item_image');
										if ($item_image) {
											//print_r($item_image['sizes']['news']);
										?>
											<img data-src="<?php print $item_image['sizes']['news']; ?>" alt="<?php print $item_image['alt']; ?>" class="lazy">
										<?php } ?>
									</div>
									<div class="padding-15">
									<h4><?php the_sub_field('item_title'); ?></h4>
									<div class="text-paragraph">
										<?php the_sub_field('item_copy'); ?>
									</div>

									<?php
									$link = get_sub_field('item_cta');
									if ($link) {
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self';
									?>

										<div class="bottom-btn">
											<a href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" target="<?php echo $link_target; ?>" class="a-btn a-btn--bbdrbtm">Read more<?php //echo $link_title; ?></a>
										</div>
										</div>
									<?php } ?>
								</li>

							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>