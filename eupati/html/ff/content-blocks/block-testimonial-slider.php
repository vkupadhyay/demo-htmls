<?php if (get_sub_field('testimonials_row')) : ?>

    <section class="stories-section white-bg section-pd-tb">
        <div class="grid justify-center">
            <div class="col-sm-10">
                <div class="section-hd a-align--center">
                    <?php if (get_sub_field('testimonial_slider_heading')) { ?>
                        <h2>
                            <?php the_sub_field('testimonial_slider_heading'); ?>
                        </h2>
                    <?php } ?>
                </div>
                <div class="stories-container">
                    <div class="stories-slider">
                        <?php if (have_rows('testimonials_row')) : ?>

							<?php while (have_rows('testimonials_row')) : the_row(); ?>
                                <?php $testimonial_item = get_sub_field('testimonial_item');?>
                                <?php if ($testimonial_item ) { ?>
                                    <div class="stories-slider-item">
                                        <div class="stories-slider-item-inner">
                                            <div class="text-paragraph">
                                            "<?php echo get_post_field('post_content', $testimonial_item->ID);?>"
                                            </div>
                                            <div class="stories-writer">
                                                <?php
                                                $author_picture = get_field('testimonial_author_image', $testimonial_item->ID);
                                                ?>
                                                <div class="stories-writer-img">
                                                    <?php if ($author_picture) { ?>
                                                        <img src="<?php echo $author_picture['sizes']['thumbnail']; ?>" alt="" />
                                                    <?php } else { ?>
                                                    <?php } ?>
                                                </div>
                                                <div class="stories-writer-info">
                                                    <strong><?php the_field('testimonial_author', $testimonial_item->ID); ?></strong>
                                                    <span><?php the_field('testimonial_author_position', $testimonial_item->ID); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>