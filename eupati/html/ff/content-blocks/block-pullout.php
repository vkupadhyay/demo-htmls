<div class="m-entity m-entity__pullout">
  <div class="grid justify-center">
    <div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6">
  
      <?php 
        $image = get_sub_field('icon');
        if ($image){
      ?>
      <div class="m-pullout">
        <figure>
          <img data-src="<?php print $image['sizes']['small'];?>" alt="<?php print $image['alt'];?>" class="lazy">
        </figure>
      <?php } else { ?>
      <div class="m-pullout m-pullout--noimg">
      <?php } ?>
        
        <div class="m-pullout__content">
          <?php if (get_sub_field('title')){?>
            <h2>
              <?php the_sub_field('title'); ?>
            </h2>
          <?php }?>

          <?php the_sub_field('copy'); ?>
          
          <?php
            $link = get_sub_field('button');
            if( $link ){
              $link_url = $link['url'];
              $link_title = $link['title'];
              $link_target = $link['target'] ? $link['target'] : '_self';
              ?>
            <a href="<?php echo $link_url;?>" title="<?php echo $link_title; ?>" target="<?php echo $link_target;?>" class="a-btn a-btn--alt a-btn--icon"><?php echo $link_title; ?></a>
          <?php } ?>
        </div>

      </div>
    </div>
  </div>
</div>