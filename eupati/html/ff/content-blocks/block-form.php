<section class="m-entity m-entity__form">
  <div class="grid justify-center">
    <div class="col-sm-10 col-md-8 col-lg-8 col-xlg-6">
      
      <?php if (get_sub_field('title')){?>
        <h2>
          <?php the_sub_field('title'); ?>
        </h2>
      <?php }?>

      <?php 
      // var_dump(get_sub_field('form'));
        $form = get_sub_field('form');
        gravity_form($form['id'], true, true, false, '', true, 1); 
      ?>
      
    </div>
  </div>
</section>