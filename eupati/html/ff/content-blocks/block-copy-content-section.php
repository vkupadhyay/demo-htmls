<?php if (get_sub_field('copy_content_title') || get_sub_field('copy_content_text')) { ?>

	<section class="simple-text-section light-cyan-bg section-pd-tb" <?php if (get_sub_field('copy_content_bg')) {
																			echo 'style="background-color: ' . get_sub_field('copy_content_bg') . ';"';
																} ?>>
		<div class="grid justify-center">
			<div class="col-sm-8">
				<div class="section-hd a-align--center">
					<?php if (get_sub_field('copy_content_title')) { ?>
						<h2>
							<?php the_sub_field('copy_content_title'); ?>
						</h2>
					<?php } ?>

					<div class="text-paragraph">
						<?php the_sub_field('copy_content_text'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php } ?>