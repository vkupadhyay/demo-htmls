<?php
$parallax_image = get_sub_field('parallax_image');
if ($parallax_image) {
?>
    <section class="img-parallax-section" style="background-image:url(<?php print $parallax_image; ?>);"></section>
<?php } ?>