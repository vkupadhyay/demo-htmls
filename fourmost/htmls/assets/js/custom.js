$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $(".main-header").addClass("white-header");
    } else {
        $(".main-header").removeClass("white-header");
    }
});

$('#navclick').click(function(){
    $(mainContainer).toggleClass('menu-slide');
    $(clickmenu).toggleClass('menu-slide');
});

$('#navToggle').click(function(){
    $(main_nav).toggleClass('menu-slide');
});

$('.sub-menu-toggle').click(function(e){
    e.preventDefault();
    e.stopPropagation();
    $(this).parents('li').toggleClass('submenu-expanded');
});

$('.link-has-childern').click(function(e){
    if(!$(this).parents('li').hasClass('submenu-expanded')&& $('body').hasClass('menu-slide')){
        $(this).parents('li').addClass('submenu-expanded');
        e.preventDefault();
        e.stopPropagation();
    }
});

$(".button").click(() => {
    $("selector").addClass("popshow");
});

$('#letTalkBtn').click(function(){
    $(mainContainer).toggleClass('pop-show');
});

if( $(".home-page-slider").length>0){
    $(".home-page-slider").slick({
        dots: false,
        arrows: false,
        infinite: true,
        autoPlay:true,
        fade: true,
        speed: 100,
        slidesToShow: 1,
        slidesToScroll: 1
    });
    
}


$(".thumb-2-slider").slick({
    dots: false,
    arrows: true,
    infinite: false,
    fade: false,
    slidesToShow: 2,
    slidesToScroll: 2
});
$(".thumb-3-slider").slick({
    dots: false,
    arrows: true,
    infinite: false,
    fade: false,
    slidesToShow: 3,
    slidesToScroll: 3
});


  AOS.init();

