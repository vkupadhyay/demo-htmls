<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Story
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
<!--[if lt IE 9]>
   <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">

<header id="masthead" class="header-site">
	         
           <div class="custom-row">
            <div class="logo"><a href="https://4kpictures.co.in/" title="4k Pictures"><span>4k</span>Pictures</a></div>
            <div class="header-right">
             
              <nav>
                <?php
                  wp_nav_menu(
                    array(
                      'theme_location' => 'menu-1',
                      'menu_id'        => 'primary-menu',
                    )
                  );
                ?>
              </nav>
              
            </div>
          </div>
      
    </header>

	<div id="content" class="mid-content">