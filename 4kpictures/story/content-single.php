<?php
/**
 * @package Story
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>
	>
	<h1 class="entry-title"><?php the_title(); ?></h1>
	<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?> <?php endif; ?>

	<?php the_content(); ?>

	
	<div class="gallery-img">
	<?php 
$images = get_field('gallery'); // Fetch gallery images
if ($images): ?>
    <ul>
        <?php foreach ($images as $image): 
            // Fetch the original image URL
            $original_image_url = wp_get_original_image_url($image['ID']); 
            
            // Fallback to the current URL if original isn't available
            if (!$original_image_url) {
                $original_image_url = $image['url'];
            }
        ?>
            <li>
                <div class="gallery-image-box">
                    <!-- Preview link -->
                    <a class="example-image-link" 
					   href="<?php echo esc_url($image['url']); ?>" 
                       data-lightbox="example-set" 
                       data-title="<?php echo esc_html(str_replace('-', ' ', $image['title'])); ?>">
                        <img
                            src="<?php echo esc_url($image['sizes']['medium']); ?>" 
                            title="<?php echo esc_attr(str_replace('-', ' ', $image['title'])); ?>" 
                            alt="<?php echo esc_attr(!empty($image['alt']) ? $image['alt'] : str_replace('-', ' ', $image['title'])); ?>" 
                            loading="lazy"
                            fetchpriority="high"
                        />
                    </a>
                    
                    <!-- Download button -->
                    <div class="gallery-caption">
                        <h3><?php echo esc_html(str_replace('-', ' ', $image['title'])); ?></h3>
                        <a class="download-btn" 
                           href="<?php echo esc_url($original_image_url); ?>" 
                           download="<?php echo esc_attr(basename($original_image_url)); ?>" 
                           target="_blank">
                           Download
                        </a>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

	 </div>
</article>
<!-- #post-## -->
