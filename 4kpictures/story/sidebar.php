<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package Story
 */
?>
	<div id="secondary" class="widget-area" role="complementary">
		<div class="siderbar-search">
				<h2>Search</h2>
				<?php get_search_form(); ?>
		</div>
		<div class="siderbar-social">
			<h2>Share on Social Media</h2>
			<?php dynamic_sidebar( 'Social Media Share' ); ?>
	   </div>
	   
	   <?php
			if (is_single()) { // Check if the current page is a single post
				echo ''; // Message for posts
				?>
				<div class="related-posts">
					<h3>Related Posts</h3>
					<ul>
						<?php
						$categories = wp_get_post_categories(get_the_ID()); // Get categories of the current post
						if ($categories) {
							$args = array(
								'category__in' => $categories, // Fetch posts from the same categories
								'post__not_in' => array(get_the_ID()), // Exclude the current post
								'posts_per_page' => 4, // Number of related posts
								'orderby' => 'rand', // Random order
							);
							$related_posts = new WP_Query($args);
							if ($related_posts->have_posts()) {
								while ($related_posts->have_posts()) {
									$related_posts->the_post();
									?>
									<li>
										<a href="<?php the_permalink(); ?>">
											<div class="related-thumb"><?php the_post_thumbnail('thumbnail', array('loading' => 'lazy')); ?></div>
											<div class="related-content">
												<h4><?php the_title(); ?></h4>
												<span><?php the_date(); ?></span>
											</div>
										</a>
									</li>
									<?php
								}
								wp_reset_postdata(); // Reset post data after custom query
							} else {
								echo '<li>No related posts found.</li>';
							}
						}
						?>
					</ul>
				</div>
				<?php
			} else {
				// Display a message if it's not a single post
				echo '<div></div>';
				
				// Display a list of pages
				echo '<div class="related-posts related-pages">';
				echo '<h3>Related Pages</h3>';
				echo '<ul>';
				wp_list_pages(array(
					'title_li' => '', // Removes default "Pages" title
					'sort_column' => 'menu_order', // Sorts by menu order
				));
				echo '</ul>';
				echo '</div>';
			}
			?>


	  
		<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>


		<?php endif; // end sidebar widget area ?>
	</div><!-- #secondary -->
