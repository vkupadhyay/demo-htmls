<?php
/**
 * Story functions and definitions
 *
 * @package Story
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'story_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function story_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Story, use a find and replace
	 * to change 'story' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'story', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 820, 450, true );
	add_filter( 'use_default_gallery_style', '__return_false' );

	// This theme uses wp_nav_menu() in one location.
	// register_nav_menus( array(
	// 	'primary' => __( 'Primary Menu', 'story' ),
	// ) );

	register_nav_menus(
		array(
			'menu' => esc_html__( 'Primary', 'pictures' ),
		)
	);

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'image', 'video', 'quote', 'link', 'audio' ) );

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form', ) );

	// Setup the WordPress core custom background feature.
	
}
endif; // story_setup
add_action( 'after_setup_theme', 'story_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function story_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'story' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
};
register_sidebar( array(
	'name'          => __( 'Category Sidebar', 'story' ),
	'id'            => 'category-sidebar',
	'before_widget' => '<aside id="%1$s" class="widget %2$s category-sidebar">',
	'after_widget'  => '</aside>',
	'before_title'  => '<h1 class="widget-title">',
	'after_title'   => '</h1>',
) );
register_sidebar( array(
	'name'          => __( 'Social Media Share', 'story' ),
	'id'            => 'Search',
	'before_widget' => '<aside id="%1$s" class="widget %2$s social-media-share">',
	'after_widget'  => '</aside>',
	'before_title'  => '<h1 class="widget-title">',
	'after_title'   => '</h1>',
) );
register_sidebar(
	array(
		'name'          => esc_html__( 'Search', 'story' ),
		'id'            => 'search-main',
		'description'   => esc_html__( 'Add widgets here.', 'pictures' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	)
);
register_sidebar(
	array(
		'name'          => esc_html__( 'SameSizeAdsOne', 'story' ),
		'id'            => 'SameSizeAdsOne',
		'description'   => esc_html__( 'Add widgets here.', 'pictures' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	)
);
register_sidebar(
	array(
		'name'          => esc_html__( 'SameSizeAdsTwo', 'story' ),
		'id'            => 'SameSizeAdsTwo',
		'description'   => esc_html__( 'Add widgets here.', 'pictures' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	)
);
register_sidebar(
	array(
		'name'          => esc_html__( 'Categories', 'story' ),
		'id'            => 'Categories',
		'description'   => esc_html__( 'Add widgets here.', 'pictures' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	)
);
register_sidebar(
	array(
		'name'          => esc_html__( 'Siteinfo', 'story' ),
		'id'            => 'Siteinfo',
		'description'   => esc_html__( 'Add widgets here.', 'pictures' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	)
);
add_action( 'widgets_init', 'story_widgets_init' );

function story_widget_content_wrapper($content) {
    
    $content = '<div class="widget-content">'.$content.'</div>';
    return $content;
}
add_filter('widget_text', 'story_widget_content_wrapper');

/**
 * Enqueue scripts and styles.
 */

if (!function_exists('story_google_fonts')) {
	function story_google_fonts() {
	
		wp_enqueue_style( 'story-fonts', '//fonts.googleapis.com/css?family=Bitter:400,700|Source+Sans+Pro:400,600|Yesteryear&subset=latin' );
	
	}
}
add_action( 'wp_enqueue_scripts', 'story_google_fonts' );


function story_scripts() {

	wp_enqueue_style( 'story-style', get_stylesheet_uri() );
	
	

	wp_enqueue_style( 'story-icofont', get_template_directory_uri() . '/css/storyicofont.css' );

	wp_enqueue_style( 'story-slicknav-css', get_template_directory_uri() . '/css/slicknav.css' );
	wp_enqueue_style( 'story-lightbox-css', get_template_directory_uri() . '/css/lightbox.min.css' );
	wp_enqueue_style( 'story-global-css', get_template_directory_uri() . '/css/global.css' );
	wp_enqueue_style( 'story-color', get_stylesheet_directory_uri() . '/css/'. get_theme_mod( 'color_scheme_select', 'turquoise' ) .'.css' );
	wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'story-fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'), '20120206', true );
	
	wp_enqueue_script( 'story-slicknav', get_template_directory_uri() . '/js/jquery.slicknav.min.js' );
	wp_enqueue_script( 'story-custom', get_template_directory_uri() . '/js/custom.js' );
	wp_enqueue_script( 'story-lightbox', get_template_directory_uri() . '/js/lightbox.min.js' );
	wp_enqueue_script( 'story-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'story_scripts' );

/**
 * Remove Gallery Inline Styling
 */
/* add_filter( 'use_default_gallery_style', '__return_false' );

/**
 * Extract video from content for video post format.
 */
function story_featured_video( $content ) {
	$url = trim( current ( explode( "\n", $content ) ) );	
	if ( 0 === strpos( $url, 'http://' ) || preg_match ( '#^<(script|iframe|embed|object)#i', $url )) { 	
 		echo apply_filters( 'the_content', $url ); 	
 	}	 		
}

function story_video_content( $content ) {
	$url = trim( current ( explode( "\n", $content ) ) );	
	if ( 0 === strpos( $url, 'http://' ) || preg_match ( '#^<(script|iframe|embed|object)#i', $url )) { 		
 		$content = trim( str_replace( $url, '', $content ) );  	
 	}
	return $content;
}

if (!function_exists('story_footer_js')) {
	function story_footer_js() {
    ?>
        <script>     
       
        jQuery(document).ready(function($) {   
			
				
			$('#reply-title').addClass('section-title');

			

			$('#site-navigation .menu>ul').slicknav({prependTo:'#mobile-menu'});

			$('.post').fitVids();

			var shrinkHeader = 300;
			$(window).scroll(function(){
				var scroll = getCurrentScroll();
				if (scroll > shrinkHeader ) {
					$('#masthead').addClass('shrink');
				} else {
					$('#masthead').removeClass('shrink');
				}
			})

			function getCurrentScroll() {
				return window.pageYOffset;
			}
						
        });
        </script>
    <?php
	}
}
add_action( 'wp_footer', 'story_footer_js', 20, 1 );    


function modify_gallery_captions_to_image_name($content, $block) {
    if ('core/gallery' === $block['blockName']) {
        // Parse the HTML output of the gallery block
        $content = preg_replace_callback('/<img[^>]+class="[^"]*wp-image-(\d+)[^"]*"[^>]*>/', function ($matches) {
            $image_id = $matches[1]; // Extract the image ID from the class
            $image_path = get_attached_file($image_id); // Get the image file path

            // Use pathinfo() to extract the file name without the extension
            $image_info = pathinfo($image_path);
            $image_name = $image_info['filename']; // Get the file name without the extension

            // Remove the '-scaled' suffix if it exists
            $image_name_cleaned = str_replace('-scaled', '', $image_name);

            // Replace hyphens with spaces
            $image_name_cleaned = str_replace('-', ' ', $image_name_cleaned);

            // Convert to title case
            $image_name_title_case = ucwords($image_name_cleaned);

            // Replace or add a caption with the cleaned and formatted image name
            return $matches[0] . '<figcaption>' . esc_html($image_name_title_case) . '</figcaption>';
        }, $content);
    }
    return $content;
}
add_filter('render_block', 'modify_gallery_captions_to_image_name', 10, 2);




/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


