<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Story
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet" as="font" type="text/css" crossorigin>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
<![endif]-->
	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-VWWZV1H3HZ"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-VWWZV1H3HZ');
</script>
	   <script type="application/ld+json">
    {
      "@context" : "https://schema.org",
      "@type" : "WebSite",
      "name" : "4kpictures",
      "alternateName" : "4kpictures",
      "url" : "https://4kpictures.co.in/",
      "potentialAction": {
        "@type": "SearchAction",
        "target": {
          "@type": "EntryPoint",
          "urlTemplate": "https://4kpictures.co.in/?s={search_term_string}"
        },
        "query-input": "required name=search_term_string"
      }
    }
  </script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">
<div class="search-pop">
	<div class="search-pop-in">
		<button id="searchClose">
		<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
	    	<path fill-rule="evenodd" clip-rule="evenodd" d="M15.5459 2.25742C15.6388 2.16133 15.7125 2.04724 15.7628 1.92165C15.8131 1.79606 15.839 1.66143 15.8391 1.52546C15.8392 1.38949 15.8134 1.25483 15.7632 1.12917C15.7131 1.00352 15.6395 0.889329 15.5468 0.793122C15.454 0.696916 15.3439 0.620578 15.2227 0.568466C15.1014 0.516354 14.9715 0.48949 14.8402 0.489406C14.709 0.489322 14.579 0.516021 14.4577 0.567979C14.3364 0.619936 14.2262 0.696134 14.1333 0.792222L8.10594 7.03642L2.08029 0.792222C1.89274 0.597925 1.63836 0.48877 1.37313 0.48877C1.10789 0.48877 0.853514 0.597925 0.665963 0.792222C0.478412 0.98652 0.373047 1.25004 0.373047 1.52482C0.373047 1.7996 0.478412 2.06312 0.665963 2.25742L6.69335 8.49982L0.665963 14.7422C0.573097 14.8384 0.499432 14.9526 0.449173 15.0783C0.398915 15.204 0.373047 15.3388 0.373047 15.4748C0.373047 15.6109 0.398915 15.7456 0.449173 15.8713C0.499432 15.997 0.573097 16.1112 0.665963 16.2074C0.853514 16.4017 1.10789 16.5109 1.37313 16.5109C1.50446 16.5109 1.6345 16.4841 1.75584 16.432C1.87717 16.3799 1.98742 16.3036 2.08029 16.2074L8.10594 9.96322L14.1333 16.2074C14.3209 16.4015 14.5752 16.5104 14.8402 16.5102C15.1053 16.5101 15.3595 16.4008 15.5468 16.2065C15.7341 16.0122 15.8392 15.7488 15.8391 15.4742C15.8389 15.1996 15.7335 14.9363 15.5459 14.7422L9.51853 8.49982L15.5459 2.25742Z" fill="#333333"/>
		</svg>
		</button>
		<strong>Search here</strong>
    <?php dynamic_sidebar( 'Search' ); ?>
	</div>
</div>
    <header id="masthead" class="site-header">
      <div class="full-container">
        <div class="custom-row">
          <div class="logo">
         
            <a href="https://4kpictures.co.in/" title="4k Pictures"><span>4k</span>Pictures</a>
          </div>
          <div class="header-right">
					<nav class="hide-ipad">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'menu',
									'menu_id'        => 'primary-menu',
								)
							);
						?>
					</nav>
					<div class="header-search">
						<span class="search-btn" id="searchClick"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z"/></svg></span>
					 </div>
           <div class="header-social hide-mobile">
              <ul>
                 <li class="fb">
                     <a href="https://www.facebook.com/4K-Pictures-61557031212144/" target="_blank">
                     <svg viewBox="0 0 512 512" preserveAspectRatio="xMidYMid meet">
                      <path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"></path>
                     </svg>
                     </a>
                 </li>
                 <li class="insta">
                     <a href="https://www.instagram.com/4kpicturesweb/">
                     <svg viewBox="0 0 512 512" preserveAspectRatio="xMidYMid meet" >
                        <path d="M256 109.3c47.8 0 53.4 0.2 72.3 1 17.4 0.8 26.9 3.7 33.2 6.2 8.4 3.2 14.3 7.1 20.6 13.4 6.3 6.3 10.1 12.2 13.4 20.6 2.5 6.3 5.4 15.8 6.2 33.2 0.9 18.9 1 24.5 1 72.3s-0.2 53.4-1 72.3c-0.8 17.4-3.7 26.9-6.2 33.2 -3.2 8.4-7.1 14.3-13.4 20.6 -6.3 6.3-12.2 10.1-20.6 13.4 -6.3 2.5-15.8 5.4-33.2 6.2 -18.9 0.9-24.5 1-72.3 1s-53.4-0.2-72.3-1c-17.4-0.8-26.9-3.7-33.2-6.2 -8.4-3.2-14.3-7.1-20.6-13.4 -6.3-6.3-10.1-12.2-13.4-20.6 -2.5-6.3-5.4-15.8-6.2-33.2 -0.9-18.9-1-24.5-1-72.3s0.2-53.4 1-72.3c0.8-17.4 3.7-26.9 6.2-33.2 3.2-8.4 7.1-14.3 13.4-20.6 6.3-6.3 12.2-10.1 20.6-13.4 6.3-2.5 15.8-5.4 33.2-6.2C202.6 109.5 208.2 109.3 256 109.3M256 77.1c-48.6 0-54.7 0.2-73.8 1.1 -19 0.9-32.1 3.9-43.4 8.3 -11.8 4.6-21.7 10.7-31.7 20.6 -9.9 9.9-16.1 19.9-20.6 31.7 -4.4 11.4-7.4 24.4-8.3 43.4 -0.9 19.1-1.1 25.2-1.1 73.8 0 48.6 0.2 54.7 1.1 73.8 0.9 19 3.9 32.1 8.3 43.4 4.6 11.8 10.7 21.7 20.6 31.7 9.9 9.9 19.9 16.1 31.7 20.6 11.4 4.4 24.4 7.4 43.4 8.3 19.1 0.9 25.2 1.1 73.8 1.1s54.7-0.2 73.8-1.1c19-0.9 32.1-3.9 43.4-8.3 11.8-4.6 21.7-10.7 31.7-20.6 9.9-9.9 16.1-19.9 20.6-31.7 4.4-11.4 7.4-24.4 8.3-43.4 0.9-19.1 1.1-25.2 1.1-73.8s-0.2-54.7-1.1-73.8c-0.9-19-3.9-32.1-8.3-43.4 -4.6-11.8-10.7-21.7-20.6-31.7 -9.9-9.9-19.9-16.1-31.7-20.6 -11.4-4.4-24.4-7.4-43.4-8.3C310.7 77.3 304.6 77.1 256 77.1L256 77.1z"></path>
                        <path d="M256 164.1c-50.7 0-91.9 41.1-91.9 91.9s41.1 91.9 91.9 91.9 91.9-41.1 91.9-91.9S306.7 164.1 256 164.1zM256 315.6c-32.9 0-59.6-26.7-59.6-59.6s26.7-59.6 59.6-59.6 59.6 26.7 59.6 59.6S288.9 315.6 256 315.6z"></path>
                        <circle cx="351.5" cy="160.5" r="21.5"></circle>
                    </svg>
                     </a>
                 </li>
                
              </ul>
           </div>
					<div class="hamburger" id="hamburgerClick">
						<span class="line"></span>
						<span class="line"></span>
						<span class="line"></span>
					</div>
				</div>
        </div>
      </div>
    </header>
    <div class="mobile-nav">
    <?php wp_nav_menu(array('theme_location' => 'primary')); ?>
        <?php
                      wp_nav_menu(
                        array(
                          'theme_location' => 'menu',
                          'menu_id'        => 'primary-menu',
                        )
                      );
                    ?>

<div class="header-social hide-ipad">
              <ul>
                 <li class="fb">
                     <a target="_blank" href="https://www.facebook.com/4K-Pictures-61557031212144/">
                     <svg viewBox="0 0 512 512" preserveAspectRatio="xMidYMid meet">
                      <path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"></path>
                     </svg>
                     </a>
                 </li>
                 <li class="insta">
                     <a target="_blank" href="https://www.instagram.com/4kpicturesweb/">
                     <svg viewBox="0 0 512 512" preserveAspectRatio="xMidYMid meet" >
                        <path d="M256 109.3c47.8 0 53.4 0.2 72.3 1 17.4 0.8 26.9 3.7 33.2 6.2 8.4 3.2 14.3 7.1 20.6 13.4 6.3 6.3 10.1 12.2 13.4 20.6 2.5 6.3 5.4 15.8 6.2 33.2 0.9 18.9 1 24.5 1 72.3s-0.2 53.4-1 72.3c-0.8 17.4-3.7 26.9-6.2 33.2 -3.2 8.4-7.1 14.3-13.4 20.6 -6.3 6.3-12.2 10.1-20.6 13.4 -6.3 2.5-15.8 5.4-33.2 6.2 -18.9 0.9-24.5 1-72.3 1s-53.4-0.2-72.3-1c-17.4-0.8-26.9-3.7-33.2-6.2 -8.4-3.2-14.3-7.1-20.6-13.4 -6.3-6.3-10.1-12.2-13.4-20.6 -2.5-6.3-5.4-15.8-6.2-33.2 -0.9-18.9-1-24.5-1-72.3s0.2-53.4 1-72.3c0.8-17.4 3.7-26.9 6.2-33.2 3.2-8.4 7.1-14.3 13.4-20.6 6.3-6.3 12.2-10.1 20.6-13.4 6.3-2.5 15.8-5.4 33.2-6.2C202.6 109.5 208.2 109.3 256 109.3M256 77.1c-48.6 0-54.7 0.2-73.8 1.1 -19 0.9-32.1 3.9-43.4 8.3 -11.8 4.6-21.7 10.7-31.7 20.6 -9.9 9.9-16.1 19.9-20.6 31.7 -4.4 11.4-7.4 24.4-8.3 43.4 -0.9 19.1-1.1 25.2-1.1 73.8 0 48.6 0.2 54.7 1.1 73.8 0.9 19 3.9 32.1 8.3 43.4 4.6 11.8 10.7 21.7 20.6 31.7 9.9 9.9 19.9 16.1 31.7 20.6 11.4 4.4 24.4 7.4 43.4 8.3 19.1 0.9 25.2 1.1 73.8 1.1s54.7-0.2 73.8-1.1c19-0.9 32.1-3.9 43.4-8.3 11.8-4.6 21.7-10.7 31.7-20.6 9.9-9.9 16.1-19.9 20.6-31.7 4.4-11.4 7.4-24.4 8.3-43.4 0.9-19.1 1.1-25.2 1.1-73.8s-0.2-54.7-1.1-73.8c-0.9-19-3.9-32.1-8.3-43.4 -4.6-11.8-10.7-21.7-20.6-31.7 -9.9-9.9-19.9-16.1-31.7-20.6 -11.4-4.4-24.4-7.4-43.4-8.3C310.7 77.3 304.6 77.1 256 77.1L256 77.1z"></path>
                        <path d="M256 164.1c-50.7 0-91.9 41.1-91.9 91.9s41.1 91.9 91.9 91.9 91.9-41.1 91.9-91.9S306.7 164.1 256 164.1zM256 315.6c-32.9 0-59.6-26.7-59.6-59.6s26.7-59.6 59.6-59.6 59.6 26.7 59.6 59.6S288.9 315.6 256 315.6z"></path>
                        <circle cx="351.5" cy="160.5" r="21.5"></circle>
                    </svg>
                     </a>
                 </li>

                
                
              </ul>
           </div>
	</div>
    <div id="content" class="mid-content">