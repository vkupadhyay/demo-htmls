<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Story
 */

get_header(); ?>

<div class="single-page">
	<div class="full-container">
		<div class="custom-row">
			<div class="page-left">

				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

		
						<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'single' ); ?>

					
						
						<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) :
					comments_template();
				endif;
			?>

						<?php endwhile; // end of the loop. ?>
					</main>
					<!-- #main -->
				</div>
			</div>
			<div class="page-right">
				<?php get_sidebar(); ?>
		
				

		
		
		</div>
		</div>
	</div>
</div>


	<!-- #primary -->


<?php get_footer(); ?>