<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pictures
 */

?>

<div class="related-categories-picture-list">
<h2>Top 5 Pages Links of 4k Pictures</h2>
<figure class="wp-block-table">
   <table class="has-fixed-layout">
      <tbody>
      <?php
// Create a custom query to get the latest 5 posts
$args = array(
    'post_type' => 'post',  // Query only posts
    'posts_per_page' => 5,  // Limit to 5 posts
);
$query = new WP_Query($args);

// The Loop
if ($query->have_posts()) :
    while ($query->have_posts()) : $query->the_post();
        echo '<h2>' . get_the_title() . '</h2>'; // Display the post title
        echo '<p>' . get_the_excerpt() . '</p>'; // Display the post excerpt
    endwhile;
else :
    echo '<p>No posts found.</p>';
endif;

// Reset Post Data
wp_reset_postdata();
?>
     
      </tbody>
   </table>
</figure>
</div>
