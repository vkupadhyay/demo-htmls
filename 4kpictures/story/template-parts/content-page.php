<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pictures
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="mysite-banner"><?php dynamic_sidebar('Horizontal Banner 2')?></div>
<div class="site-welcome">
  <h1><?php the_title(); ?></h1>
</div>

	<?php pictures_post_thumbnail(); ?>

	<div class="page-content"><?php the_content(); ?></div>
	

	
</div><!-- #post-<?php the_ID(); ?> -->
