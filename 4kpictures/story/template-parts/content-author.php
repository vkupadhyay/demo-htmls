<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pictures
 */

?>

<section class="author-section">
       <?php $author_id=$post->post_author; ?>
        <div class="author-section-top-img">
           <img src="<?php echo get_avatar_url(get_the_author_meta('ID')); ?>" alt="<?php echo get_the_author(); ?>" />
        </div>
        <h3>
           <?php the_author_meta( 'user_nicename' , $author_id ); ?>
        </h3>
     
        <div class="author-section-description">
        <?php the_author_meta('description', $author_id); ?>
        </div>
      

	
</section><!-- .no-results -->


