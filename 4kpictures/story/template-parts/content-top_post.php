<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pictures
 */

?> 


<div class="post-row">
	
          
            <?php
            $args = array(
            'post_type'=> 'post',
            'orderby'    => 'ID',
            'post_status' => 'publish',
            'order'    => 'DESC',
            'posts_per_page' =>  12, // this will retrive all the post that is published 
            'offset'          => 4, 
            );
            $result = new WP_Query( $args );
            if ( $result-> have_posts() ) : ?>
            <?php while ( $result->have_posts() ) : $result->the_post(); ?>
               <div class="post-box items">  
                  <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
                     
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">  
                           <div class="post-box-img">
                           <img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="<?php the_title_attribute(); ?>" loading="lazy"  fetchpriority="high" />
                              <div class="post-admin-time"><?php get_template_part( 'template-parts/content', 'author_date' ); ?></div>
                           </div>
                        </a>
                     
                     <div class="post-box-content">
                        <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?>  </a></h3>
                       
                     </div>

               </div>

            <?php endwhile; ?>
            <?php endif; wp_reset_postdata(); ?>

         </div>
