<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pictures
 */

?>

	<div class="same-size-banner">
		<?php dynamic_sidebar('DetailsPageAdsOne')?>
	</div>

	<div class="rem1space hide-in-mobile"></div>

	<div class="same-size-banner hide-in-mobile">
		<?php dynamic_sidebar('DetailsPageAdsTwo')?>
	</div>
	
	<div class="rem1space hide-in-mobile"></div>
	
	<div class="post-list-sidebar hide-in-mobile">
		<?php dynamic_sidebar('Post List Sidebar')?>
	</div>




