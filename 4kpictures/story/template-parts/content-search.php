<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pictures
 */

?>

<div id="post-<?php the_ID(); ?>" class="top-post-box"><?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
      <div class="top-post-box-img" style="background-image: url('<?php echo $url ?>');">
          <?php pictures_post_thumbnail('medium'); ?> 
          <div class="post-admin-time"><?php get_template_part( 'template-parts/content', 'author_date' ); ?></div>
      </div>
  </a>
  <div class="top-post-box-content">
                        <h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?>  </a></h5>
                        <div class="page-content"><?php echo wp_trim_words( get_the_content(), 40, '...' );?></div>
   </div>
  
 
</div><!-- #post-<?php the_ID(); ?> -->
