<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pictures
 */

?>
   <?php
          
            $args = array(
               'posts_per_page' => 1,
               'orderby' => 'modified',
               'order' => 'DESC',
               'posts_per_page' =>  2, // this will retrive all the post that is published 
               'offset'          => 2, 
              );
            $result = new WP_Query( $args );
            if ( $result-> have_posts() ) : ?>
            <?php while ( $result->have_posts() ) : $result->the_post(); ?>
<div class="hero-box">  <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>

<div class="hero-box-img"><img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="<?php the_title_attribute(); ?>" loading="eager"  fetchpriority="high" /> </div>
                  <div class="hero-box-content">
				  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">  
                    <div class="hero-box-content-in">
                       <h3><?php the_title(); ?></h3>
                       
                       <?php get_template_part( 'template-parts/content', 'author_date' ); ?>
                    </div>
                    </a>
                  </div>
</div>
<?php endwhile; ?>
            <?php endif; wp_reset_postdata(); ?>
