<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Story
 */

get_header(); ?>
<div class="search-list">
  <div class="full-container">
     <div class="custom-row">
	    <div class="page-left">
		<?php if ( have_posts() ) : ?>

<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'story' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
<div class="post-row">
<?php /* Start the Loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'content', 'search' ); ?>

<?php endwhile; ?>
</div>
<?php story_paging_nav(); ?>

<?php else : ?>

<?php get_template_part( 'content', 'none' ); ?>

<?php endif; ?>
		</div>
		<div class="page-right"><?php get_sidebar(); ?></div>
	 </div>
  </div>
</div>



<?php get_footer(); ?>
