<?php
/**
 * Template Name: Home Page
 */

get_header(); ?>

<section class="middle">
	<div class="full-container">
	<div class="site-welcome"><?php echo the_content(); ?></div>
	<div class="rem1space"></div>
		<div class="hero-section">
			<div class="hero-section-left">
				<?php get_template_part( 'template-parts/content', 'hero_1' ); ?>
			</div>

			<div class="hero-section-right">
				<div class="hero-section-right-top">
					<?php get_template_part( 'template-parts/content', 'hero_2' ); ?>
				</div>
				<div class="hero-section-right-btm">
					<?php get_template_part( 'template-parts/content', 'hero_3' ); ?>
				</div>
			</div>
		</div>
		<div class="rem1space"></div>
		

		<div class="sce-section">
			<div class="custom-row">
				<div class="sce-section-left">
					<?php get_template_part( 'template-parts/content', 'top_post' ); ?>
				</div>
				<div class="sce-section-right">
					<?php dynamic_sidebar( 'HomeAdsOne' ); ?>
					<div class="rem1space"></div>
					<div class="home-categories">
						<h4>Top Categories</h4>
						<?php dynamic_sidebar( 'Categories' ); ?>
					</div>
					<div class="rem1space" style="margin-top: 20px;"></div>
					<?php dynamic_sidebar( 'SameSizeAdsOne' ); ?>
				</div>
			</div>
		</div>

		<div class="rem1space"></div>
		  
	</div>
</section>




<?php get_footer(); ?>
