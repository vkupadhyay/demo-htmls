<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Story
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

			<section class="error-404 not-found entry-wrapper">
                 <div class="not-found-img"></div>
				<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'story' ); ?></h1>

				<div class="page-content sdsad">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'story' ); ?></p>
					<div class="not-found-search-form">
						   <?php get_search_form(); ?>
					</div>
					

					<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>


				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->
	
	<?php get_sidebar(); ?>

<?php get_footer(); ?>