<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Story
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
	   
	   <div class="siteinfo">
		 <?php dynamic_sidebar('Siteinfo')?>
	   </div>
	   <div class="footer-links">
		   <ul>
			   <li><a href="https://4kpictures.co.in/copyright-policy/">Copyright Policy</a></li>
			   <li><a href="https://4kpictures.co.in/privacy-policy/">Privacy Policy</a></li>
		   </ul>
	   </div>
	  
   
   </footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>