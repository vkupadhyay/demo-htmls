<?php
/**
 * @package Story
 */
?>

<article id="post-<?php the_ID(); ?>" class="post-box">

		<a href="<?php the_permalink(); ?>" rel="bookmark">
           <div class="post-box-img">
				<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
					<?php the_post_thumbnail('medium', ['loading' => 'lazy']); ?>
				<?php endif; ?>
			<div class="post-admin-time"><?php get_template_part( 'template-parts/content', 'author_date' ); ?></div>
		   </div>

		</a>
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="post-box-content">
			<h2 ><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			
		</div>
	   <?php endif; ?>
	

	

</article><!-- #post-## -->
