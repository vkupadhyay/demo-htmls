(function ($) {
  $(document).ready(function () {
    $("#searchClick").click(function () {
      $("body").toggleClass("pop");
    });
  });

  $(document).ready(function () {
    $("#searchClose").click(function () {
      $("body").removeClass("pop");
    });
  });

  $(document).ready(function () {
    $("#hamburgerClick").click(function () {
      $(".hamburger").toggleClass("is-active");
      $("body").toggleClass("menu-show");
    });
  });

  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 300) {
        $("body").addClass("headerfixed");
    } else {
        $("body").removeClass("headerfixed");
    }
});
  
})(jQuery);

