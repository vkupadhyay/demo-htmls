<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pictures
 */

?> 
<div>
    
    <div class="sidebar-latest-post box1 yellow-box1">
		<?php dynamic_sidebar('Latest News')?>
	</div>

	<div class="sidebar-latest-post box1 margin-top-2">
		<h2>Latest Post</h2>
		<ul class="right-latest-post">
			<?php
				query_posts(array('orderby' => 'rand', 'showposts' => 25));
				if (have_posts()) :
				while (have_posts()) : the_post(); ?>
				<li>
					<div class="right-latest-post-img">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
					</div>
					<div class="right-latest-post-content">
						<a href="<?php the_permalink() ?>" class="wpp-post-title" target="_self"><?php the_title(); ?></a>
						<span><?php the_date(); ?></span>
					</div>
                </li>
				<?php endwhile; endif; ?>
			</ul>
	</div>

	<div class="sidebar-latest-post box1 margin-top-2">
			<?php dynamic_sidebar('AdsDisplay')?>
	</div>

	
</div>

<style>
	.right-latest-post{
		margin:0px;
		padding:0px;
		list-style:none;
	}
	.right-latest-post li{
		display:flex;
		    margin: 0 0 20px 0;
	}
	.right-latest-post-img{
		width:70px;
		height:70px;
		
	}
	.right-latest-post-img a{
		display:block;
	}
	.right-latest-post-img a img{
		display:block;
		width:100%;
		height:100%;
	}
	.right-latest-post-content{
		width:calc(100% - 70px);
		padding-left:10px;
	}
	.right-latest-post-content a{
		display:block;
		margin-bottom:5px;
		word-break:break-all;
	}
	.right-latest-post-content span{
		display:block;
		font-size:12px;
		font-weight:300;
	}
	 @media (min-width: 320px) and (max-width: 767px) {
     .site-details-right{
		display:none;
	}
    }
	
</style>


