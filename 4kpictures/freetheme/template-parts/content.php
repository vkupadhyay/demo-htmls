<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wallpaper
 */

?>

	<div class="news-title">
				<h1 title="<?php the_title(); ?>"><?php the_title(); ?></h1>
	</div>

	<div class="margin-tp-btm"><?php dynamic_sidebar('AdsDisplay')?></div>

	<div class="news-content">
					<picture><?php the_post_thumbnail('full'); ?></picture>

					<?php echo the_content(); ?>

					<div class="custom-full-img" style="margin-top: 10px;">
						<?php if( get_field('image_upload_1') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_1'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" href="<?php the_field('image_upload_1'); ?>" download>download</a>
							</div>
						</figure>
						
						<?php endif; ?>

						<?php if( get_field('image_upload_2') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_2'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" href="<?php the_field('image_upload_2'); ?>" download>download</a>
							</div>
						</figure>
						
						<?php endif; ?>

						<?php if( get_field('image_upload_3') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_3'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" href="<?php the_field('image_upload_3'); ?>" download>download</a>
							</div>
						</figure>
						
						<?php endif; ?>

						<?php if( get_field('image_upload_4') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_4'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" href="<?php the_field('image_upload_4'); ?>" download>download</a>
							</div>
						</figure>
						
						<?php endif; ?>

						<?php if( get_field('image_upload_5') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_5'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" href="<?php the_field('image_upload_5'); ?>" download>download</a>
							</div>
						</figure>
						<div class="margin-tp-btm"><?php dynamic_sidebar('AdsDisplay')?></div>
						<?php endif; ?>

						<?php if( get_field('image_upload_6') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_6'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" href="<?php the_field('image_upload_6'); ?>" download>download</a>
							</div>
						</figure>
						
						<?php endif; ?>
						<?php if( get_field('image_upload_7') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_7'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" href="<?php the_field('image_upload_7'); ?>" download>download</a>
							</div>
						</figure>
						
						<?php endif; ?>
						<?php if( get_field('image_upload_8') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_8'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" href="<?php the_field('image_upload_8'); ?>" download>download</a>
							</div>
						</figure>
						
						<?php endif; ?>
						<?php if( get_field('image_upload_9') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_9'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" href="<?php the_field('image_upload_9'); ?>" download>download</a>
							</div>
						</figure>
						
						<?php endif; ?>
						<?php if( get_field('image_upload_10') ): ?>
						<figure class="acf--img">
							<img src="<?php the_field('image_upload_10'); ?>" alt="<?php the_title(); ?>" />
							<div class="download-link">
								<a class="btn-primary" href="<?php the_field('image_upload_10'); ?>" download>download</a>
							</div>
						</figure>
						<div class="margin-tp-btm"><?php dynamic_sidebar('AdsDisplay')?></div>
						<?php endif; ?>
					</div>

					<div class="featured_post">
						<?php
							$featured_posts = get_field('featured_post');
							if( $featured_posts ): ?>
							
													<div class="figure-row">
														<?php foreach( $featured_posts as $post ): 

								// Setup this post for WP functions (variable must be named $post).
								setup_postdata($post); ?>
														<figure class="acf--img">
															<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(full); ?></a>
															<div class="download-link">
																<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
																<a class="btn-primary" href="<?php echo $url ?>" download>download</a>
															</div>
														</figure>

														
														<?php endforeach; ?>
													</div>
													<?php 
							// Reset the global post object so that the rest of the page works correctly.
							wp_reset_postdata(); ?>
						<?php endif; ?>
					</div>
				</div>

