<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wallpaper
 */

get_header('custom');
?>

<div class="news-details-page">
	<div class="main-container-div">
		<div class="custom-row">
			<div class="site-details-left">
				<?php
						while ( have_posts() ) :
							the_post();

							get_template_part( 'template-parts/content', get_post_type() );

							//the_post_navigation();

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						endwhile; // End of the loop.
						?>
			</div>
			<div class="site-details-right">
				<div class="sidebar-latest-post box1">
					<?php dynamic_sidebar('AdsDisplay')?>
				</div>

				<div class="sidebar-latest-post box1 margin-top-2">
					<h2>Latest Post</h2>
					<?php dynamic_sidebar('AdsFeed')?>
				</div>

				

				<div class="sidebar-latest-post box1 margin-top-2">
					<?php dynamic_sidebar('Tags')?>
				</div>

				<div class="sidebar-latest-post yellow-box1 margin-top-2">
					<?php dynamic_sidebar('Latest News')?>
				</div>
			</div>
		</div>
	</div>
	<div class="related-wallpaper-section">
		<div class="main-container-div">
			<h3>Popular <span>Wallpapers</span></h3>
			<div class="related-wallpaper">
			<ul class="wpp-list wpp-list-with-thumbnails">
			<?php
query_posts(array('orderby' => 'rand', 'showposts' => 16));
if (have_posts()) :
while (have_posts()) : the_post(); ?>


        <li>
                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
                <a href="<?php the_permalink() ?>" class="wpp-post-title" target="_self"><?php the_title(); ?></a>
        </li>

 

 
<?php endwhile;

endif; ?></ul>
			   <//?php dynamic_sidebar('Add')?>
			</div>
		</div>
	</div>
</div>


		
		
<?php

get_footer();