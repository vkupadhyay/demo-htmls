<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
 <script type="application/ld+json">
    {
      "@context" : "https://schema.org",
      "@type" : "WebSite",
      "name" : "freewallpapers4u",
      "alternateName" : "freewallpapers4u",
      "url" : "https://www.freewallpapers4u.in/",
      "potentialAction": {
        "@type": "SearchAction",
        "target": {
          "@type": "EntryPoint",
          "urlTemplate": "https://www.freewallpapers4u.in/?s={search_term_string}"
        },
        "query-input": "required name=search_term_string"
      }
    }
  </script>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3682776889470101"
     crossorigin="anonymous"></script>
	<meta name="google-site-verification" content="t4MUNzfRlx8lSv1ymV4SAQW5dvqPcgVmWV3QeN_S_vo" />
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;700&display=swap" rel="stylesheet">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<meta name="keywords" content="cool wallpapers,wallpaper hd,best wallpapers,high quality wallpapers,desktop wallpaper,wallpaper website,high resolution wallpaper,picture wallpaper,wallpaper for computer,desktop backgrounds,4k wallpapers,high res wallpaper,wallpaper for pc,cool wallpapers for phone,nice wallpaper,4k wallpaper for pc,windows wallpaper,free wallpaper downloads,wallpaper hd download,fantasy wallpaper,hd pics,beautiful wallpapers,cool wallpapers for free,desktop wallpaper 4k,wallpaper unique,free hd wallpapers,high quality desktop backgrounds,wallpaper backgrounds,wallpaper site,hd wallpaper desktop,high resolution desktop wallpaper,wallpaper space,hd pictures download,computer backgrounds,wallpaper for pc download,screen wallpaper,free wallpaper background,wallpaper minimalist,cool wallpapers hd,wallpaper pc 4k,high resolution wallpaper 4k,wallpaper collection,windows wallpaper 4k,wallpaper quality,screen backgrounds,free wallpaper 4k,top quality wallpaper,desktop images,hd desktop,best wallpapers for pc,desktop backgrounds 4k,desktop wallpaper free download,funky wallpapers,wallpaper images download,cool wallpapers for phone free,download wallpaper 4k,wallpaper desktop 4k,images wallpaper,cool wallpapers 4k,pc backgrounds,all wallpaper,hd wallpaper desktop 4k,favorite wallpaper,wallpaper free download for mobile,1080p wallpaper,high quality desktop wallpapers,free hd wallpapers for desktop,wallpaper mobile,high resolution desktop backgrounds,gorgeous wallpapers,cool photos hd download,best desktop wallpapers,4k hd wallpapers,cool photos for wallpaper hd,best hd wallpapers,free high quality wallpapers,free high resolution wallpaper,wallpaper of the year,free wallpaper,wallpaper download,happy wallpaper,popular wallpaper,free desktop wallpaper,most popular wallpaper,amazing wallpaper,free wallpaper for computer,best quality wallpaper,wide wallpaper,hd wallpapers for pc,interesting wallpapers,free desktop backgrounds,cool photos for wallpaper,theme wallpaper,4k backgrounds,good wallpapers,free wallpaper images,download hd,desktop photo,wallpaper good quality,free 4k backgrounds," />
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/global.css" rel="stylesheet" />
	<!-- <script data-ad-client="ca-pub-3682776889470101" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-63758721-2"></script>
	<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());
	gtag('config', 'UA-63758721-2');
	</script>
	
	<script async src="https://fundingchoicesmessages.google.com/i/pub-3682776889470101?ers=1" nonce="8dSBpC04Wb1lsjvY7bFcBQ"></script><script nonce="8dSBpC04Wb1lsjvY7bFcBQ">(function() {function signalGooglefcPresent() {if (!window.frames['googlefcPresent']) {if (document.body) {const iframe = document.createElement('iframe'); iframe.style = 'width: 0; height: 0; border: none; z-index: -1000; left: -1000px; top: -1000px;'; iframe.style.display = 'none'; iframe.name = 'googlefcPresent'; document.body.appendChild(iframe);} else {setTimeout(signalGooglefcPresent, 0);}}}signalGooglefcPresent();})();</script>
	
	
	<?php wp_head(); ?>
</head>

<body class="main-background main-background-1">
	<!-- <body id="mainbox" <?php body_class(); ?>> -->

	  <header class="main-header">
	    <div class="main-header-in">
		     <div class="logo"><a href="<?php echo get_home_url(); ?>">Free<span>wallpapers4u</span></a></div>
			<div class="search"><?php dynamic_sidebar('search'); ?></div>
			<nav class="main-nav">
				<?php
                           			wp_nav_menu( array(
                           				'theme_location' => 'menu-1',
                           				'menu_id'        => 'primary-menu',
                           			) );
                           			?>
			</nav>
			</div>
			<div class="main-header-img"><?php dynamic_sidebar('HeaderImage')?></div>
		</header>


		<section class="container-fluid">
              <div class="slider-post"><?php dynamic_sidebar('UpdatePost')?></div>
		<section class="mid-container">
			