<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package wallpaper
 */

get_header();
?>
<div class="custom-row">
   <div class="site-details-left">
   <div class="wallpapershow-section">
            <h1><?php printf( esc_html__( 'Search Results for: %s', 'wallpaper' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
            <div class="wallpapershow wallpapershow-3">
			  <?php if ( have_posts() ) : ?>
				<div class="wallpaper-list">
                 <div class="figure-row">
			        <?php
			          while ( have_posts() ) :
				        the_post();
				        get_template_part( 'template-parts/content', 'search' );
					endwhile;
					else :
						get_template_part( 'template-parts/content', 'none' );

		           endif; ?>
                 </div>
			    </div> 
	        </div>
	</div>
   </div>
   <div class="site-details-right">
			  <?php get_template_part( 'template-parts/content', 'right' ); ?>
	</div>
</div>




<?php
get_footer();
