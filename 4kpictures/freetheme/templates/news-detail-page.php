<?php
/**
 * Template Name: News detail Page
 * Template Post Type: post, page, News
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
?>

	<div class="news-details-page">
		<div class="main-container-div">
			<div class="news-banner">
			           <?php if( get_field('image_upload_1') ): ?>
							<figure class="acf--img">
								<img src="<?php the_field('image_upload_1'); ?>" alt="<?php the_title(); ?>" />
								
						    </figure> 
                        <?php endif; ?>	
			</div>
            <div class="news-title">
			  <h1 title="<?php the_title(); ?>"><?php the_title(); ?></h1>
			  <div class="author-details-page"><?php get_template_part( 'template-parts/content', 'author' ); ?></div>
			</div>
			<div class="custom-row">
				<div class="site-details-left">
				<div class="margin-tp-btm"><?php dynamic_sidebar('AdsDisplay')?></div>
				<div class="news-content">
					
					<?php echo the_content(); ?>
					<picture><?php the_post_thumbnail(medium); ?></picture>
				</div>
				</div>
				<div class="site-details-right">
				<?php get_template_part( 'template-parts/content', 'right' ); ?>
				
				</div>
			</div>

			
			<div class="related-wallpaper-section">
		      <div class="main-container-div">
			     <h3>Popular <span>Wallpapers</span></h3>
			     <?php get_template_part( 'template-parts/content', 'related_wallpaper' ); ?>
		      </div>
	        </div>
	</div>

<?php get_footer(); ?>
