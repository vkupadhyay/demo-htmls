<?php
/**
* Template Name: Wallpaper Category
 */

get_header(); ?>
<div class="custom-row">
   <div class="site-details-left">
      <h1>Wallpaper <span>categories</span></h1>
      <div class="wallpapershow-section wallpapershow-3">
      <?php get_template_part( 'template-parts/content', 'category' ); ?>
      </div>
    </div>
    <div class="site-details-right">
      <?php get_template_part( 'template-parts/content', 'right' ); ?>
    </div>
</div>



<?php get_footer(); ?>
