<?php
/**
 * Template Name: Home Page
 */

get_header(); ?>

<div class="site-desktop-view dsdsa">

            <div class="latest-update-post">
                <?php 
                $args = array(
                    'posts_per_page' => 2,
                    'orderby' => 'modified',
                    'order' => 'DESC',
                );
                $latest_updated_post = new WP_Query($args);
                if ($latest_updated_post->have_posts()) :
                    while ($latest_updated_post->have_posts()) : $latest_updated_post->the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" class="latest-update-post-box">
                        <a href="<?php the_permalink(); ?>">
                        <picture><?php the_post_thumbnail('large'); ?></picture> 
                        <div class="latest-update-post-box-in"><h4><?php the_title(); ?></h4>
                        <span class="posted-on">Last updated on <?php the_modified_date(); ?></span></div> 
                        
                    </a>
                        </article>
                    <?php endwhile;
                    wp_reset_postdata();
                else : ?>
                    <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif;
                
                ?>
            </div>

            <div class="wallpapershow-section">
                <div class="site-hd-section">
                    <h2>Wallpapers <span></span></h2>
                    <a class="abst-rigth-top btn-primary" href="https://www.freewallpapers4u.in/category/background-wallpapers/">More Wallpapers</a>
                </div>
                <div class='home-page-section wallpapershow wallpapershow-2'> 
                    <?php dynamic_sidebar('Page List Home')?>
                </div>
                <div class="wallpapershow wallpapershow-2">
                    <div class="figure-row">
                    <?php
                    // the query
                    $the_query = new WP_Query(array(
                    'posts_per_page' => 12,
                    'orderby' => 'modified',
                    'order' => 'DESC',
                    ));
                    ?>

                    <?php if ($the_query->have_posts()) : ?>
                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        
                        <figure class="Post__box">
                            <a href="<?php the_permalink(); ?>">
                                <picture><?php the_post_thumbnail('thumbnail'); ?></picture>
                                <figcaption><?php the_title(); ?></figcaption>
                            </a>
                        </figure>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>

                    <?php else : ?>
                        <p><?php esc_html_e('No News'); ?></p>
                    <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="site-welcome">
                <?php the_content(); ?>
            </div>

</div>

<div class="site-mobile-view">
    <div class="latest-update-post">
        <?php 
        $args = array(
            'posts_per_page' => 1,
            'orderby' => 'modified',
            'order' => 'DESC',
        );
        $latest_updated_post = new WP_Query($args);
        if ($latest_updated_post->have_posts()) :
            while ($latest_updated_post->have_posts()) : $latest_updated_post->the_post(); ?>
                <article id="post-<?php the_ID(); ?>" class="latest-update-post-box">
                <a href="<?php the_permalink(); ?>">
                <picture><?php the_post_thumbnail('large'); ?></picture> 
                <div class="latest-update-post-box-in"><h4><?php the_title(); ?></h4>
                <span class="posted-on">Last updated on <?php the_modified_date(); ?></span></div> 
                
            </a>
                </article>
            <?php endwhile;
            wp_reset_postdata();
        else : ?>
            <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif;
        
        ?>
    </div>
    <div class="mobile-post-row">
        <h2>Latest Wallpapers</h2>
        <div class="mobile-figure-row">
            <?php
            // the query
            $the_query = new WP_Query(array(
            'posts_per_page' => 12,
            'orderby' => 'modified',
            'order' => 'DESC',
            ));
            ?>

            <?php if ($the_query->have_posts()) : ?>
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                
                <figure class="mobile-Post__box">
                    <a href="<?php the_permalink(); ?>">
                        <picture><?php the_post_thumbnail('thumbnail'); ?></picture>
                        <figcaption><?php the_title(); ?></figcaption>
                    </a>
                </figure>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>

            <?php else : ?>
                <p><?php esc_html_e('No News'); ?></p>
            <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
