<?php
/**
* Template Name: News
 */

get_header(); ?>

<div class="custom-row">
	<div class="site-details-left">
		<div class="wallpapershow-section">
			<div class="wallpapershow wallpapershow-3">
				<div class="">
					<div class="figure-row">
						<?php $args = [
                      'post_type' =>
						'post', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 8, 'category_name' => 'News', 'paged' => get_query_var('paged'), 'post_parent' => $parent, ]; ?>
						<?php query_posts($args); ?>
						<?php if (have_posts()): ?>
						<?php while (have_posts()): the_post(); ?>
						<figure class="Post__box">
							<a href="<?php the_permalink(); ?>">
								<?php $thumb = get_the_post_thumbnail_url(); ?>
								<picture><?php the_post_thumbnail(medium); ?></picture>
								<figcaption><?php the_title(); ?></figcaption>
							</a>
						</figure>
						<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
        </div>
		<div class="site-details-right"><?php get_template_part( 'template-parts/content', 'right' ); ?></div>
	
</div>
<?php get_footer(); ?>
