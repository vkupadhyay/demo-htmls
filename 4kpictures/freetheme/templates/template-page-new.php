<?php
/**
 * Template Name: Page all
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

 get_header('custom');
?>

<div class="news-details-page">
	<div class="main-container-div">
		<div class="news-title">
			<h1 title="<?php the_title(); ?>"><?php the_title(); ?></h1>
      <div class="author-details-page"><?php get_template_part( 'template-parts/content', 'author' ); ?></div>
		</div>
		<div class="custom-row">
			<div class="site-details-left">
			<div class="margin-tp-btm"><?php dynamic_sidebar('AdsDisplay')?></div>
				<div class="news-content">
					<picture><?php the_post_thumbnail(medium); ?></picture>

					<?php echo the_content(); ?>

					<div class="custom-full-img" style="margin-top: 10px;">
					<?php if( get_field('image_page_1') ): ?>
              <figure class="acf--img">
                    <img src="<?php the_field('image_page_1'); ?>" alt="<?php the_title(); ?>" />
                    <div class="download-link">
                     <a class="btn-primary" href="<?php the_field('image_page_1'); ?>" download>download</a>
                  </div>
               </figure> 
               
            <?php endif; ?>	
                  
            
						<?php if( get_field('image_page_2') ): ?>
                <figure class="acf--img">
                      <img src="<?php the_field('image_page_2'); ?>" alt="<?php the_title(); ?>" />
                      <div class="download-link">
                      <a class="btn-primary" href="<?php the_field('image_page_2'); ?>" download>download</a>
                    </div>
                </figure>
                
            <?php endif; ?>	
                   
           
						<?php if( get_field('image_page_3') ): ?>
              <figure class="acf--img">
                    <img src="<?php the_field('image_page_3'); ?>" alt="<?php the_title(); ?>" />
                    <div class="download-link">
                     <a class="btn-primary" href="<?php the_field('image_page_3'); ?>" download>download</a>
                  </div>
				      </figure>
              
            <?php endif; ?>	
                  
            
						<?php if( get_field('image_page_4') ): ?>
                <figure class="acf--img">
                      <img src="<?php the_field('image_page_4'); ?>" alt="<?php the_title(); ?>" />
                      <div class="download-link">
                      <a class="btn-primary" href="<?php the_field('image_page_4'); ?>" download>download</a>
                    </div>
                </figure>
               
            <?php endif; ?>	
                   
            
						<?php if( get_field('image_page_5') ): ?>
                <figure class="acf--img">
                      <img src="<?php the_field('image_page_5'); ?>" alt="<?php the_title(); ?>" />
                      <div class="download-link">
                      <a class="btn-primary" href="<?php the_field('image_page_5'); ?>" download>download</a>
                    </div>
                </figure>
                <div class="margin-tp-btm"><?php dynamic_sidebar('AdsDisplay')?></div>
            <?php endif; ?>	
                   
            
						<?php if( get_field('image_page_6') ): ?>
                <figure class="acf--img">
                      <img src="<?php the_field('image_page_6'); ?>" alt="<?php the_title(); ?>" />
                      <div class="download-link">
                      <a class="btn-primary" href="<?php the_field('image_page_6'); ?>" download>download</a>
                    </div>
              </figure>
             
            <?php endif; ?>	
                   
            
						<?php if( get_field('image_page_7') ): ?>
              <figure class="acf--img">
                    <img src="<?php the_field('image_page_7'); ?>" alt="<?php the_title(); ?>" />
                    <div class="download-link">
                     <a class="btn-primary" href="<?php the_field('image_page_7'); ?>" download>download</a>
                  </div>
				      </figure>
             
            <?php endif; ?>	
                   
						<?php if( get_field('image_page_8') ): ?>
                <figure class="acf--img">
                  <img src="<?php the_field('image_page_8'); ?>" alt="<?php the_title(); ?>" />
                        <div class="download-link">
                        <a class="btn-primary" href="<?php the_field('image_page_8'); ?>" download>download</a>
                      </div>
                </figure> 
                
            <?php endif; ?>	
                  
           
						<?php if( get_field('image_page_9') ): ?>
                <figure class="acf--img">
                      <img src="<?php the_field('image_page_9'); ?>" alt="<?php the_title(); ?>" />
                      <div class="download-link">
                      <a class="btn-primary" href="<?php the_field('image_page_9'); ?>" download>download</a>
                    </div>
                </figure>
                
            <?php endif; ?>	
                  
            
						<?php if( get_field('image_page_10') ): ?>
              <figure class="acf--img">
                    <img src="<?php the_field('image_page_10'); ?>" alt="<?php the_title(); ?>" />
                    <div class="download-link">
                     <a class="btn-primary" href="<?php the_field('image_page_10'); ?>" download>download</a>
                  </div>
              </figure> 
              <div class="margin-tp-btm"><?php dynamic_sidebar('AdsDisplay')?></div>
            <?php endif; ?>	
					</div>

					
				</div>
			</div>

			<div class="site-details-right">
         <?php get_template_part( 'template-parts/content', 'right' ); ?>
			</div>
		</div>

		
	</div>
  <div class="related-wallpaper-section">
		<div class="main-container-div">
			<h3>Popular <span>Wallpapers</span></h3>
      <?php get_template_part( 'template-parts/content', 'related_wallpaper' ); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
