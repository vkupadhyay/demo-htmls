<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Story
 */

get_header(); ?>
<div class="single-page">
		<div class="banner">
			<div class="main-header-img"><?php the_post_thumbnail('large'); ?></div>
			<div class="container">	<h1 ><?php the_title(); ?></h1></div>
		</div>
		<div class="single-page-row">
			<div class="container">
				<div class="custom-row">
				 
					<div class="site-details-left">
					<?php get_template_part( 'content', 'single' ); ?>
					
					<div style="clear:both"></div>
					<div class="related-posts-section">
					<div class="related-posts">
					<h3>Related Posts</h3>
					<ul>
						<?php
						$categories = wp_get_post_categories(get_the_ID()); // Get categories of the current post
						if ($categories) {
							$args = array(
								'category__in' => $categories, // Fetch posts from the same categories
								'post__not_in' => array(get_the_ID()), // Exclude the current post
								'posts_per_page' => 3, // Number of related posts
								'orderby' => 'rand', // Random order
							);
							$related_posts = new WP_Query($args);
							if ($related_posts->have_posts()) {
								while ($related_posts->have_posts()) {
									$related_posts->the_post();
									?>
									<li>
										<a href="<?php the_permalink(); ?>">
											<div class="related-thumb"><?php the_post_thumbnail('thumbnail', array('loading' => 'lazy')); ?></div>
											<div class="related-content">
												<h4><?php the_title(); ?></h4>
												<span><?php the_date(); ?></span>
											</div>
										</a>
									</li>
									<?php
								}
								wp_reset_postdata(); // Reset post data after custom query
							} else {
								echo '<li>No related posts found.</li>';
							}
						}
						?>
					</ul>
				</div>
					</div>
					<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() ) :
								comments_template();
							endif;
						?>

					</div>
					
				</div>
			</div>
		</div>
</div>

<?php get_footer(); ?>