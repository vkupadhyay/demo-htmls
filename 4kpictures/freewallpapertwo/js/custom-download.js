jQuery(document).ready(function ($) {
    $(".wp-block-image").each(function () {
        let figure = $(this);
        let img = figure.find("img");
        let imageUrl = img.data("original-url") || img.attr("src"); // Fallback to src if data-original-url is missing
        let pageUrl = window.location.href; // Get current page URL

        // Remove any existing buttons and count
        figure.find(".download-info").remove();

        if (imageUrl) {
            let filename = imageUrl.split('/').pop(); // Extract filename

            let downloadBtn = $("<a></a>")
            .attr("href", `#`)

            let countDisplay = $("<span></span>")
                .attr("data-url", imageUrl)
                .text("");

            // Create Social Share Buttons
        

            let facebookBtn = $("<a></a>")
                .attr("href", `https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(pageUrl)}`)
                .attr("target", "_blank")
                .addClass("facebook-share")

            let twitterBtn = $("<a></a>")
                .attr("href", `https://twitter.com/intent/tweet?url=${encodeURIComponent(pageUrl)}&text=Check this out!`)
                .attr("target", "_blank")
                .addClass("twitter-share")

            let whatsappBtn = $("<a></a>")
                .attr("href", `https://api.whatsapp.com/send?text=Check this out: ${encodeURIComponent(pageUrl)}`)
                .attr("target", "_blank")
                .addClass("whatsapp-share")

           

            // Create UL and LI structure
            let ul = $("<ul></ul>").addClass("download-info");
            let li1 = $("<li class='download-icon'></li>").append(downloadBtn);
            let li2 = $("<li class='love-icon'></li>").append(countDisplay);
            let li3 = $("<li class='facebook-icon'></li>").append(facebookBtn); 
            let li4 = $("<li class='twitter-icon'></li>").append(twitterBtn);
            let li5 = $("<li class='whatsapp-icon'></li>").append(whatsappBtn);

            ul.append(li1).append(li2).append(li3).append(li4).append(li3).append(li5); // Append <li> inside <ul>
            figure.append(ul); // Append <ul> inside figure

            // Fetch and display the download count
            $.get(ajaxurl, { action: "get_download_count", file_url: imageUrl }, function (count) {
                countDisplay.text(count);
            });

            // Handle Download Click
            downloadBtn.on("click", function (e) {
                e.preventDefault();

                // Send AJAX request to increase download count
                $.post(ajaxurl, { action: "update_download_count", file_url: imageUrl }, function (newCount) {
                    countDisplay.text(newCount);

                    // Force download
                    let tempLink = document.createElement("a");
                    tempLink.href = imageUrl;
                    tempLink.setAttribute("download", filename);
                    document.body.appendChild(tempLink);
                    tempLink.click();
                    document.body.removeChild(tempLink);
                }).fail(function () {
                    alert("Failed to update download count.");
                });
            });
        }
    });
});
