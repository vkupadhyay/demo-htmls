jQuery(document).ready(function ($) {
    $(".wp-block-image img").each(function () {
        let img = $(this);
        let originalUrl = img.attr("src"); // Default to the src

        // Check if srcset exists and get the last (largest) image
        let srcset = img.attr("srcset");
        if (srcset) {
            let urls = srcset.split(", ").map(item => item.split(" ")[0]); // Extract URLs
            originalUrl = urls[urls.length - 1]; // Get the last (largest) image URL
        }

        let link = $("<a></a>")
            .attr("href", originalUrl)
            .attr("data-lightbox", "gallery")
            .attr("data-title", img.attr("title"));

        img.wrap(link);
    });
});
