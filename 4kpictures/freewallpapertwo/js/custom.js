(function ($) {
   
  // window.addEventListener("scroll", function () {
  //   const sidebar = document.getElementsByClassName("site-details-right")[0]; // Access the first element
  //   const offsetTop = 105; // Space from the top of the viewport
  //   const scrollY = window.scrollY || window.pageYOffset;
  
  //   if (scrollY > offsetTop) {
  //     sidebar.style.position = "fixed";
  //     sidebar.style.top = offsetTop + "px";
  //   } else {
  //     sidebar.style.position = "fixed";
  //   }
  // });
    
    $(document).ready(function () {
      $("#hamburgerClick").click(function () {
        $(".hamburger").toggleClass("is-active");
        $("body").toggleClass("menu-show");
      });
    });
  
    $(window).scroll(function() {    
      var scroll = $(window).scrollTop();
  
      if (scroll >= 200) {
          $("body").addClass("headerfixed");
      } else {
          $("body").removeClass("headerfixed");
      }
  });
	
	
    
  })(jQuery);
  
  
  