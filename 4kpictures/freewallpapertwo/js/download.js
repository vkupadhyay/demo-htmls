jQuery(document).ready(function ($) {
    $(".wp-block-image").each(function () {
        let figure = $(this);
        let img = figure.find("img");
        let imageUrl = img.data("original-url"); // Get the original image URL

        // Remove any existing download buttons
        figure.find(".download-btn").remove();

        if (imageUrl) {
            let downloadBtn = $("<a></a>")
                .attr("href", imageUrl)
                .attr("download", imageUrl.split('/').pop()) // Extract filename
                .text("Download Image")
                .addClass("download-btn")
                

            // Append Download Button after the figcaption
            figure.append(downloadBtn);
        }
    });
});
