<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Story
 */
?>

	</div><!-- #content -->

	<footer  class="site-footer" role="contentinfo" id="footer">
		<div class="container">
			<div class="footer-info"><?php dynamic_sidebar( 'SiteInfo' ); ?></div>
			<div class="footer-link"><a href="/privacy-policy/">Privacy policy</a></div>
			</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var grid = document.querySelector('.wp-block-gallery');
    if (grid) {
        new Masonry(grid, {
            itemSelector: '.wp-block-image',
            columnWidth: '.wp-block-image',
            percentPosition: true
        });
    }
});
</script>	



</body>
</html>