<?php
/**
 * Template Name: Home Page
 */

get_header(); ?>

<div class="home-page">
    <div class="banner">
       <div class="main-header-img"><?php the_post_thumbnail('large'); ?></div>
       <div class="container">
		   <h1>
			   Free Download HD Wallpapers
		   </h1>
		   <p>
			   Free Download beautiful HD and 4K Wallpapers and Pictures for your desktop and mobile. On freewallpapers4u website you found copy right free images and pictures to use your blog and website.
		   </p>
          
       </div>
    </div>
    <div class="home-catogory-list">
        <div class="container">
            <div class="custom-row">
            <h2>Top Pages</h2>
           
            </div>
           
        </div>
               
    </div>
    <div class="home-page-post">
        <div class="container">
            <div class="post-row">
            <?php
                $args = array(
                    'post_type'      => 'post',
                    'orderby'        => 'modified',
                    'post_status'    => 'publish',
                    'order'          => 'DESC',
                    'posts_per_page' => 9, // Retrieve 12 published posts
                );

                $result = new WP_Query($args);
                if ($result->have_posts()) : ?>
                    <?php while ($result->have_posts()) : $result->the_post(); ?>
                        <div class="post-box">
                            <?php 
                            // Get the original image URL
                            $thumbnail_id = get_post_thumbnail_id($post->ID);
                            $original_image_url = wp_get_original_image_url($thumbnail_id);

                            // Fallback if the original image URL is not available
                            if (!$original_image_url) {
                                $original_image_url = wp_get_attachment_url($thumbnail_id);
                            }
                            ?>
                            
                            <div class="post-box-img">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <?php the_post_thumbnail('medium'); ?>
                                </a>
                                <div class="post-box-info">
                                    <span class="post-date"><?php echo get_the_date('F j, Y'); ?> by <?php the_author(); ?></span>
                                    <!-- Download button with original image URL -->
                                    <a class="download-btn" download href="<?php echo esc_url($original_image_url); ?>">Download</a>
                                </div>
                            </div>
                            <div class="post-box-content">
                                <h3>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </h3>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; wp_reset_postdata(); ?>

            </div>
        </div>
    </div>
	
	
</div>


<?php get_footer(); ?>
