<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Story
 */

get_header(); ?>

<div class="single-page">
		<div class="banner">
			<div class="main-header-img"><?php the_post_thumbnail('large'); ?></div>
			<div class="container">	<h1 ><?php the_title(); ?></h1></div>
		</div>
		<div class="single-page-row">
			<div class="container">
				<div class="custom-row">
					<div class="site-details-left">
					<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'content', 'page' ); ?>

							<?php
								// If comments are open or we have at least one comment, load up the comment template
								if ( comments_open() || '0' != get_comments_number() ) :
									comments_template();
								endif;
							?>

							<?php endwhile; // end of the loop. ?>
					</div>
					
				</div>
			</div>
</div>



<?php get_footer(); ?>
