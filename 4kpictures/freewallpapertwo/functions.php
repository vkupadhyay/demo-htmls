<?php
/**
 * Story functions and definitions
 *
 * @package Story
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'story_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function story_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Story, use a find and replace
	 * to change 'story' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'story', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 820, 450, true );
	add_filter( 'use_default_gallery_style', '__return_false' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu' => __( 'Primary', 'story' ),
	) );



	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'image', 'video', 'quote', 'link', 'audio' ) );

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form', ) );

	// Setup the WordPress core custom background feature.
	
}
endif; // story_setup
add_action( 'after_setup_theme', 'story_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function story_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Search', 'story' ),
		'id'            => 'mainSearch',
		'before_widget' => '<div id="%1$s" class="site-main-search">',
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'SocialMedia', 'story' ),
		'id'            => 'SocialMedia',
		'before_widget' => '<div id="%1$s" class="social-media">',
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'SiteInfo', 'story' ),
		'id'            => 'SiteInfo',
		'before_widget' => '<div id="%1$s" class="site-info-text">',
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'CategoryImage', 'story' ),
		'id'            => 'CategoryImage',
		'before_widget' => '<div id="%1$s" class="banner">',
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Related Posts', 'story' ),
		'id'            => 'RelatedPosts',
		'before_widget' => '<div id="%1$s" class="related-posts">',
		'after_widget'  => '</div>',
	) );

}
add_action( 'widgets_init', 'story_widgets_init' );

function story_widget_content_wrapper($content) {
    
    $content = '<div class="widget-content">'.$content.'</div>';
    return $content;
}
add_filter('widget_text', 'story_widget_content_wrapper');

/**
 * Enqueue scripts and styles.
 */

if (!function_exists('story_google_fonts')) {
	function story_google_fonts() {
	
		wp_enqueue_style( 'story-fonts', '//fonts.googleapis.com/css?family=Bitter:400,700|Source+Sans+Pro:400,600|Yesteryear&subset=latin' );
	
	}
}
add_action( 'wp_enqueue_scripts', 'story_google_fonts' );

function story_scripts() {

	wp_enqueue_style( 'story-style', get_stylesheet_uri() );
	
	wp_enqueue_style( 'story-color', get_stylesheet_directory_uri() . '/css/'. get_theme_mod( 'color_scheme_select', 'turquoise' ) .'.css' );

	wp_enqueue_style( 'story-icofont', get_template_directory_uri() . '/css/storyicofont.css' );

	wp_enqueue_style( 'story-slicknav-css', get_template_directory_uri() . '/css/slicknav.css' );

	wp_enqueue_style( 'story-lightbox-css', get_template_directory_uri() . '/css/lightbox.min.css' );

	wp_enqueue_style( 'story-global-css', get_template_directory_uri() . '/css/global.css' );

	wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'story-fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'), '20120206', true );
	
	wp_enqueue_script( 'story-slicknav', get_template_directory_uri() . '/js/jquery.slicknav.min.js' );

	wp_enqueue_script( 'story-custom', get_template_directory_uri() . '/js/custom.js' );

	wp_enqueue_script( 'story-lightbox', get_template_directory_uri() . '/js/lightbox.min.js' );

	wp_enqueue_script( 'story-custom-lightbox', get_template_directory_uri() . '/js/custom-fancybox.js' );
    wp_enqueue_script( 'story-custom-masonry', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js' );

	wp_enqueue_script( 'story-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'story_scripts' );

/**
 * Remove Gallery Inline Styling
 */
/* add_filter( 'use_default_gallery_style', '__return_false' );

/**
 * Extract video from content for video post format.
 */
function story_featured_video( $content ) {
	$url = trim( current ( explode( "\n", $content ) ) );	
	if ( 0 === strpos( $url, 'http://' ) || preg_match ( '#^<(script|iframe|embed|object)#i', $url )) { 	
 		echo apply_filters( 'the_content', $url ); 	
 	}	 		
}

function story_video_content( $content ) {
	$url = trim( current ( explode( "\n", $content ) ) );	
	if ( 0 === strpos( $url, 'http://' ) || preg_match ( '#^<(script|iframe|embed|object)#i', $url )) { 		
 		$content = trim( str_replace( $url, '', $content ) );  	
 	}
	return $content;
}

if (!function_exists('story_footer_js')) {
	function story_footer_js() {
    ?>
        <script>     
       
        jQuery(document).ready(function($) {   
			
				
			$('#reply-title').addClass('section-title');

			$('#content').css('margin-top', $('#masthead').height() + 50);

			$('#site-navigation .menu>ul').slicknav({prependTo:'#mobile-menu'});

			$('.post').fitVids();

			var shrinkHeader = 300;
			$(window).scroll(function(){
				var scroll = getCurrentScroll();
				if (scroll > shrinkHeader ) {
					$('#masthead').addClass('shrink');
				} else {
					$('#masthead').removeClass('shrink');
				}
			})

			function getCurrentScroll() {
				return window.pageYOffset;
			}
						
        });
        </script>
    <?php
	}
}
add_action( 'wp_footer', 'story_footer_js', 20, 1 );  

function modify_gallery_captions_to_image_name($content, $block) {
    if ('core/gallery' === $block['blockName']) {
        // Parse the HTML output of the gallery block
        $content = preg_replace_callback('/<img[^>]+class="[^"]*wp-image-(\d+)[^"]*"[^>]*>/', function ($matches) {
            $image_id = $matches[1]; // Extract the image ID from the class
            $image_path = get_attached_file($image_id); // Get the image file path

            // Use pathinfo() to extract the file name without the extension
            $image_info = pathinfo($image_path);
            $image_name = $image_info['filename']; // Get the file name without the extension

            // Remove the '-scaled' suffix if it exists
            $image_name_cleaned = str_replace('-scaled', '', $image_name);

            // Replace hyphens with spaces
            $image_name_cleaned = str_replace('-', ' ', $image_name_cleaned);

            // Convert to title case
            $image_name_title_case = ucwords($image_name_cleaned);

            // Replace or add a caption with the cleaned and formatted image name
            return $matches[0] . '<figcaption>' . esc_html($image_name_title_case) . '</figcaption>';
        }, $content);
    }
    return $content;
}
add_filter('render_block', 'modify_gallery_captions_to_image_name', 10, 2);


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


function add_download_button_to_images($content) {
    // Find all images inside <figure> tags
    $content = preg_replace_callback('/<figure class="wp-block-image.*?">.*?<img(.*?)>/s', function ($matches) {
        $img_tag = $matches[1];

        // Extract attachment ID from img tag
        preg_match('/wp-image-(\d+)/', $img_tag, $id_match);
        $attachment_id = isset($id_match[1]) ? intval($id_match[1]) : 0;

        // Get original image URL
        if ($attachment_id) {
            $original_url = wp_get_original_image_url($attachment_id);
        } else {
            $original_url = '';
        }

        // Add data-original-url attribute to the image tag
        $updated_img_tag = str_replace('<img', '<img data-original-url="' . esc_url($original_url) . '"', $matches[0]);

        return $updated_img_tag;
    }, $content);

    return $content;
}
add_filter('the_content', 'add_download_button_to_images');


function custom_enqueue_scripts() {
    wp_enqueue_script('custom-download-button', get_template_directory_uri() . '/js/custom-download.js', array('jquery'), null, true);
}
add_action('wp_enqueue_scripts', 'custom_enqueue_scripts');


function create_download_table() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'download_counts';

    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE IF NOT EXISTS $table_name (
        id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        file_url VARCHAR(255) NOT NULL,
        download_count INT UNSIGNED NOT NULL DEFAULT 0,
        UNIQUE KEY file_url (file_url)
    ) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}
register_activation_hook(__FILE__, 'create_download_table');


function get_download_count() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'download_counts';
    $file_url = $_GET['file_url'];

    $count = $wpdb->get_var($wpdb->prepare("SELECT download_count FROM $table_name WHERE file_url = %s", $file_url));

    echo $count ? $count : 0;
    wp_die();
}
add_action('wp_ajax_get_download_count', 'get_download_count');
add_action('wp_ajax_nopriv_get_download_count', 'get_download_count');


function update_download_count() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'download_counts';
    $file_url = $_POST['file_url'];

    // Check if entry exists
    $exists = $wpdb->get_var($wpdb->prepare("SELECT download_count FROM $table_name WHERE file_url = %s", $file_url));

    if ($exists !== null) {
        $wpdb->query($wpdb->prepare("UPDATE $table_name SET download_count = download_count + 1 WHERE file_url = %s", $file_url));
        $new_count = $wpdb->get_var($wpdb->prepare("SELECT download_count FROM $table_name WHERE file_url = %s", $file_url));
    } else {
        $wpdb->insert($table_name, ['file_url' => $file_url, 'download_count' => 1]);
        $new_count = 1;
    }

    echo $new_count;
    wp_die();
}
add_action('wp_ajax_update_download_count', 'update_download_count');
add_action('wp_ajax_nopriv_update_download_count', 'update_download_count');


function enqueue_custom_js() {
    wp_enqueue_script('custom-download-tracking', get_template_directory_uri() . '/js/custom-download.js', array('jquery'), null, true);
    wp_localize_script('custom-download-tracking', 'ajaxurl', admin_url('admin-ajax.php'));
}
add_action('wp_enqueue_scripts', 'enqueue_custom_js');
// 

