<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Story
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
			<meta name="google-site-verification" content="4rRQDj39Ecy0Goee_OXXvx92RKkDIMBpF-ZMWXDkaKg" />
			<meta charset="<?php bloginfo( 'charset' ); ?>" />
			<meta name="viewport" content="width=device-width, initial-scale=1" />
			<title><?php wp_title( '|', true, 'right' ); ?></title>

			<!-- Google Tag Manager -->
			<script>
				(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				})(window,document,'script','dataLayer','GTM-5ZDL7RLM');
			</script>
			<!-- End Google Tag Manager -->

			<link rel="profile" href="http://gmpg.org/xfn/11" />
			<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
			<link rel="preconnect" href="https://fonts.googleapis.com" />
			<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
			<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet" />
			<!--[if lt IE 9]>
				<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
			<![endif]-->
			<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3682776889470101" crossorigin="anonymous"></script>
			<!-- Google tag (gtag.js) -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=G-MFFL0FYWQQ"></script>
			<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', 'G-MFFL0FYWQQ');
			</script>

			<script type="application/ld+json">
				{
				  "@context": "https://schema.org",
				  "@type": "WebSite",
				  "name": "Freewallpapers4u",
				  "url": "https://freewallpapers4u.in",
				  "potentialAction": {
				    "@type": "SearchAction",
				    "target": "https://freewallpapers4u.in/search?q={search_term_string}",
				    "query-input": "required name=search_term_string"
				  }
				}
			</script>

			<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZDL7RLM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="hfeed site">

<header class="header-site">
	         <div class="container">
              <div class="custom-row">
                  <div class="logo"><a href="/"><span>Free</span> wallpapers4u</a></div>
                  <div class="main-search hide-mobile"><?php dynamic_sidebar( 'Search' ); ?></div>
                  <div class="main-menu">
                    <nav>
                     <?php
							wp_nav_menu(
								array(
									'theme_location' => 'menu',
									'menu_id'        => 'primary-menu',
								)
							);
						?>
                    </nav>
                    <div class="header-social-media">
                       <?php dynamic_sidebar( 'SocialMedia' ); ?>
                    </div>
                    
                    
                  </div>
                  <div class="hamburger" id="hamburgerClick">
                      <span class="line"></span>
                      <span class="line"></span>
                      <span class="line"></span>
                    </div>
              </div>
           </div>
         
      
    </header>

	<div id="content" class="mid-content">