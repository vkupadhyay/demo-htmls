<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Story
 */

get_header(); ?>

	<section id="primary" class="page-middle archive-pages">

		<div class="banner">
		<div class="main-header-img">
			<?php dynamic_sidebar('CategoryImage')?>
		</div>
       <div class="container">
	   <h1 class="page-title">
					<?php
						if ( is_category() ) :
							single_cat_title();

						elseif ( is_tag() ) :
							single_tag_title();

						elseif ( is_author() ) :
							printf( __( 'Author: %s', 'story' ), '<span class="vcard">' . get_the_author() . '</span>' );

						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'story' ), '<span>' . get_the_date() . '</span>' );

						elseif ( is_month() ) :
							printf( __( 'Month: %s', 'story' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'story' ) ) . '</span>' );

						elseif ( is_year() ) :
							printf( __( 'Year: %s', 'story' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'story' ) ) . '</span>' );

						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'story' );

						elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
							_e( 'Galleries', 'story');

						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'story');

						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'story' );

						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'story' );

						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'story' );

						elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
							_e( 'Statuses', 'story' );

						elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
							_e( 'Audios', 'story' );

						elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
							_e( 'Chats', 'story' );

						else :
							_e( 'Archives', 'story' );

						endif;
					?>
				</h1>
       </div>
    </div>

		

		<?php if ( have_posts() ) : ?>
			<div class="container pd-top-btm">

			<?php /* Start the Loop */ ?> 
			<div class="post-row">
				<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile; ?>
			</div>
			<?php story_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>
			</div><!-- #main -->
		<?php endif; ?>

		
	</section><!-- #primary -->


<?php get_footer(); ?>
