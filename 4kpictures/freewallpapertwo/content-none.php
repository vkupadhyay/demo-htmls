<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Story
 */
?>

<section class="no-results not-found page-middle not-found-pages">
   <div class="banner">
      <div class="main-header-img"><?php dynamic_sidebar('CategoryImage')?></div>
	  <div class="container"><h1 class="page-title"><?php _e( 'Nothing Found', 'story' ); ?></h1></div>
   </div>
	

	<div class="container pd-top-btm min-height">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p class="txt-center font-size-sm"> <?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'story' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p class="txt-center font-size-sm"><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'story' ); ?></p>
		<div class="not-found-search-form">	<?php get_search_form(); ?></div>

		<?php else : ?>

			<p class="txt-center font-size-sm"><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'story' ); ?></p>
			<div class="not-found-search-form">	<?php get_search_form(); ?></div>

		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
