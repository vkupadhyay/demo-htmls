<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Story
 */

get_header(); ?>

		<div id="primary" class="no-results not-found page-middle not-found-pages">
			<div class="banner">
					<div class="main-header-img"><?php dynamic_sidebar('CategoryImage')?></div>
					<div class="container"><h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'story' ); ?></h1></div>
			</div>
			<div  class="container pd-top-btm min-height" role="main">

			<section class="error-404 not-found entry-wrapper">

				

				<div class="page-content">
					<p class="txt-center font-size-sm"><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'story' ); ?></p>

					<div class="not-found-search-form">	<?php get_search_form(); ?></div>

					


				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</div><!-- #main -->
	</div><!-- #primary -->
	


<?php get_footer(); ?>