<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Story
 */

get_header(); ?>

	<section id="primary" class="page-middle search-page">
	<?php if ( have_posts() ) : ?>
		 <div class="banner">
	       <div class="main-header-img"><?php dynamic_sidebar('CategoryImage')?></div>
	       <div class="container"><h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'story' ), '<span>' . get_search_query() . '</span>' ); ?></h1></div>
	    </div>
		<div class="container pd-top-btm">

	

			

			<?php /* Start the Loop */ ?>
			<div class="post-row">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'search' ); ?>

			<?php endwhile; ?>
			</div>
			<?php story_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		

		</div><!-- #main --><?php endif; ?>
	</section><!-- #primary -->


<?php get_footer(); ?>
