<?php
/**
 * @package Story
 */
?>

<article id="vimal" class="post-box">
   <div class="post-box-img">
   <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">  <?php the_post_thumbnail( 'medium' ); ?></a>
                            <div class="post-box-info">
                                 <span class="post-date"><?php echo get_the_date('F j, Y'); ?> by <?php the_author(); ?></span>
                                 
                            </div>
   </div>
   <div class="post-box-content">
     <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
   </div>


</article><!-- #post-## -->
