<?php
/**
 * @package Story
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if (has_post_thumbnail() && !post_password_required()): ?>
	

	<?php endif; ?>

	    
	
	<?php the_content(); ?>


	
</article>
<!-- #post-## -->
