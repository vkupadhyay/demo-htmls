<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pictures
 */

?>

<div class="wallpapershow wallpapershow-3">
	<div class="figure-row">
		<figure>
			<a href="https://www.freewallpapers4u.in/category/animal-wallpaper/" title="Animal Wallpaper">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Animal-Wallpaper.jpg" alt="Animal Wallpaper" />
				</picture>
				<figcaption>Animal Wallpaper</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/background-wallpapers/" title="Background Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Background-Wallpapers.jpg" alt="Background Wallpapers" />
				</picture>
				<figcaption>Background Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/bird-wallpaper/" title="Bird wallpaper">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Bird-wallpaper.jpg" alt="Bird wallpaper" />
				</picture>
				<figcaption>Bird wallpaper</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/digital-wallpaper/" title="Digital Wallpaper">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Digital-Wallpaper.jpg" alt="Digital Wallpaper" />
				</picture>
				<figcaption>Digital Wallpaper</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/flower/" title="Flower Wallpaper">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Flower-Wallpaper.jpg" alt="Flower Wallpaper" />
				</picture>
				<figcaption>Flower Wallpaper</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/football-players/" title="Football Players Wallpaper">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Football-Players-Wallpaper.jpg" alt="Football Players Wallpaper" />
				</picture>
				<figcaption>Football Players Wallpaper</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/girls-wallpapers/" title="Girls Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Girls-Wallpapers.jpg" alt="Girls Wallpapers" />
				</picture>
				<figcaption>Girls Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/love/" title="Love Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Love-Wallpapers.jpg" alt="Love Wallpapers" />
				</picture>
				<figcaption>Love Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/national-cricket-players/" title="National Cricket Players">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/National-Cricket-Players-Wallpapers.jpg" alt="National Cricket Players" />
				</picture>
				<figcaption>National Cricket Players Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/national-cricket-team/" title="National Cricket Team Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/National-Cricket-Team-Wallpapers.jpg" alt="National Cricket Team Wallpapers" />
				</picture>
				<figcaption>National Cricket Team Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/national-football-team/" title="National Football Team Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/National-Football-Team-Wallpapers.jpg" alt="National Football Team Wallpapers" />
				</picture>
				<figcaption>National Football Team Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/popular-actor/" title="Popular Actor Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Popular-Actor-Wallpapers.jpg" alt="Popular Actor Wallpapers" />
				</picture>
				<figcaption>Popular Actor Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/popular-actress/" title="Popula Actress Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Popula-Actress-Wallpapers.jpg" alt="Popula Actress Wallpapers" />
				</picture>
				<figcaption>Popula Actress Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/sport-wallpapers/" title="Sport Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Sport-Wallpapers.jpg" alt="Sport Wallpapers" />
				</picture>
				<figcaption>Sport Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/nature-wallpapers/" title="Nature Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Nature-Wallpapers.jpg" alt="Nature Wallpapers" />
				</picture>
				<figcaption>Nature Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/flower/" title="Flower Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Flower-Wallpapers.jpg" alt="Flower Wallpapers" />
				</picture>
				<figcaption>Flower Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/car-wallpapers/" title="Car Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Car-Wallpapers.jpg" alt="Car Wallpapers" />
				</picture>
				<figcaption>Car Wallpapers</figcaption>
			</a>
		</figure>
		<figure>
			<a href="https://www.freewallpapers4u.in/category/bike-wallpaper/" title="Bike Wallpapers">
				<picture>
					<img src="https://www.freewallpapers4u.in/wp-content/themes/wallpaper2022/assets/img/category/Bike-Wallpapers.jpg" alt="Bike Wallpapers" />
				</picture>
				<figcaption>Bike Wallpapers</figcaption>
			</a>
		</figure>
	</div>
</div>

