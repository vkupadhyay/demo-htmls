<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wallpaper
 */

?>



<figure id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<a href="<?php the_permalink() ?>">
                                <picture>
                                    <!-- <source media="(min-width:650px)" srcset="img_pink_flowers.jpg"> -->
                                    <?php the_post_thumbnail(thumbnail); ?>
                                </picture>
                            <?php the_title( sprintf( '<figcaption>', esc_url( get_permalink() ) ), '</figcaption>' ); ?>
                        </a>
	
    
 
</figure><!-- #post-<?php the_ID(); ?> -->
