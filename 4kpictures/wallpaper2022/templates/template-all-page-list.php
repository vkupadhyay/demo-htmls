<?php
/**
* Template Name: All Page List
 */

get_header(); ?>
<div class="custom-row">
   <div class="site-details-left">
   <h1>Pages</h1>
	<div class="wallpapershow-section">
			<div class="wallpapershow wallpapershow-3">
				<div class="figure-row">
					<?php
					// Custom query to retrieve pages
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args = array(
						'post_type' => 'page',
						'post_status' => 'publish',
						'orderby' => 'menu_order',
						'order' => 'ASC',
						'posts_per_page' => 23, // Number of pages to display per page
						'paged' => $paged
					);

					$pages_query = new WP_Query($args);

					while ($pages_query->have_posts()) : $pages_query->the_post();
					?>
					

					<?php if (has_post_thumbnail()) : ?>
						<figure id="<?php the_ID(); ?>">
						<a href="<?php the_permalink(); ?>">
								<picture><?php the_post_thumbnail(thumbnail ); ?></picture>
								<figcaption><?php the_title(); ?></figcaption>
						</a>
						</figure>
						
						<?php endif; ?>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
					</div>
						</div>
						<div class="margin-tp-btm"><?php dynamic_sidebar('AdsDisplay')?></div>
					<div class="pgntn-page-pagination">
						<div class="pgntn-page-pagination-block">
					<?php
					// Pagination
					echo paginate_links(array(
						'base' => str_replace(999999999, '%#%', esc_url(get_pagenum_link(999999999))),
						'total' => $pages_query->max_num_pages,
						'current' => max(1, $paged),
						'prev_text' => '&laquo; Previous',
						'next_text' => 'Next &raquo;',
					));
					?>
</div>
				</div>
			</div>
	</div>
  
   <div class="site-details-right">
   <?php get_template_part( 'template-parts/content', 'right' ); ?>
   </div>
</div>


<?php get_footer(); ?>
