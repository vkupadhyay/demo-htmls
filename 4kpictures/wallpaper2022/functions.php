<?php
/**
 * wallpaper functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wallpaper
 */

if ( ! function_exists( 'wallpaper_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wallpaper_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on wallpaper, use a find and replace
		 * to change 'wallpaper' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'wallpaper', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'wallpaper' ),
		) );
		
		register_nav_menus( array(
			'menu-3' => esc_html__( 'Pg', 'wallpaper' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'wallpaper_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'wallpaper_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wallpaper_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'wallpaper_content_width', 640 );
}
add_action( 'after_setup_theme', 'wallpaper_content_width', 0 );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wallpaper_widgets_init() {

	register_sidebar( array(
		'name'          => esc_html__( 'HeaderImage', 'wallpaper' ),
		'id'            => 'headerimage',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="header-image">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'UpdatePost', 'wallpaper' ),
		'id'            => 'UpdatePost',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="update-post">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'AdsArticle ', 'wallpaper' ),
		'id'            => 'AdsArticle',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'AdsMultiplex', 'wallpaper' ),
		'id'            => 'AdsMultiplex',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'AdsFeed', 'wallpaper' ),
		'id'            => 'AdsFeed',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'AdsDisplay ', 'wallpaper' ),
		'id'            => 'AdsDisplay',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Category', 'wallpaper' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Latest News', 'wallpaper' ),
		'id'            => 'latestnews',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="latest-news">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Search', 'wallpaper' ),
		'id'            => 'search',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="search-form">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
	
		register_sidebar( array(
		'name'          => esc_html__( 'Page List', 'wallpaper' ),
		'id'            => 'pagelist',

		'before_widget' => '<div id="%1$s" class="pagelist">',
		'after_widget'  => '</div>',
	) );
	
		register_sidebar( array(
		'name'          => esc_html__( 'Page List Home', 'wallpaper' ),
		'id'            => 'pagelisthome',

		'before_widget' => '<div id="%1$s" class="pagelist">',
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Tags', 'wallpaper' ),
		'id'            => 'tagsshow',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="tagsshow">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Categories', 'wallpaper' ),
		'id'            => 'categorieslist',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="categoriesshow">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
	
		register_sidebar( array(
		'name'          => esc_html__( 'Add', 'wallpaper' ),
		'id'            => 'add',
		'description'   => esc_html__( 'Add widgets here.', 'wallpaper' ),
		'before_widget' => '<section id="%1$s" class="latest-news">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );

	
}

add_action( 'widgets_init', 'wallpaper_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wallpaper_scripts() {
	wp_enqueue_style( 'wallpaper-style', get_stylesheet_uri() );

	wp_enqueue_script( 'wallpaper-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'wallpaper-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wallpaper_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}




if ( ! function_exists( 'yourtheme_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 * Based on paging nav function from Twenty Fourteen
 */

function yourtheme_paging_nav() {
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}

	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );

	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}

	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

	$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

	// Set up paginated links.
	$links = paginate_links( array(
		'base'     => $pagenum_link,
		'format'   => $format,
		'total'    => $GLOBALS['wp_query']->max_num_pages,
		'current'  => $paged,
		'mid_size' => 3,
		'add_args' => array_map( 'urlencode', $query_args ),
		'prev_text' => __( '&larr; Previous', 'yourtheme' ),
		'next_text' => __( 'Next &rarr;', 'yourtheme' ),
		'type'      => 'list',
	) );

	if ( $links ) :

	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'yourtheme' ); ?></h1>
			<?php echo $links; ?>
	</nav><!-- .navigation -->
	<?php
	endif;
}

// Make sure featured images are enabled
add_theme_support( 'post-thumbnails' );

// Add featured image sizes
add_image_size( 'featured-large', 400, 700, true ); 

endif;


add_shortcode('pagelist', function ($args) {
    $args = wp_parse_args($args, [
        'type'  => 'page',
        'limit' => 10,
    ]);
    $out = [];
    $ids = [];
    // Check if we have a predefined list od IDs
    if ( ! empty($args['id'])) {
        $ids = array_filter(explode(',', $args['id']), function ($id) {
            return ! empty($id);
        });
        $ids = array_map('intval', $ids);
    }
    // If we don't have a predefined list od IDs, get the latest posts based on 'limit' parameter
    if (empty($ids)) {
        $queryArgs = [
            'post_type'              => isset($args['type']) && post_type_exists($args['type']) ? $args['type'] : 'page',
            'posts_per_page'         => ! empty($args['limit']) && is_numeric($args['limit']) ? intval($args['limit']) : 10,
            'ignore_sticky_posts'    => true,
            'fields'                 => 'ids',
            'cache_results'          => false,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
        ];
        $ids = get_posts($queryArgs);
        wp_reset_postdata();
    }
    foreach ($ids as $id) {
        $img = has_post_thumbnail($id)
            ? get_the_post_thumbnail($id, [60, 60])
            : '<span class="wpb-post-list__no-image"></span>';
        $excerpt = has_excerpt($id) ? wpautop(get_the_excerpt($id)) : '';
        $out[] = "<figure  class='wpb-page-list__item'>
		<a href='" . get_the_permalink($id) . "' class='wpb-page-list__item'>
            <picture>{$img}</picture>
            <figcaption>
                " . get_the_title($id) . "
                {$excerpt}
            </figcaption>
			</a>
        </figure>";
    }
    return "<div class='figure-row'>" . implode('', $out) . "</div>";
});

