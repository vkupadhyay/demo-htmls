$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $(".main-site-header").addClass("bluebackbg");
    } else {
        $(".main-site-header").removeClass("bluebackbg");
    }
});



$('#navclick').click(function(){
    $(opennav).toggleClass('menu-show');

});


 $('#searchicon').click(function(){
    $(searchbox).toggleClass('search-show');

});

 $('#searchclose').click(function(){
    $(searchbox).removeClass('search-show');

});

$(".wp-block-categories").slick({
    dots: false,
    infinite: false,
    variableWidth: true,
    slidesToScroll: 2,
    responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToScroll: 2,
            slidesToShow: 2,
            infinite: false,
            variableWidth: false,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToScroll: 1,
            slidesToShow: 1,
            infinite: false,
            variableWidth: false,
          }
        }
      ]
  });