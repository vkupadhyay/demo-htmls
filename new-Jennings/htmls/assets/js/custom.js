$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $(".main-header").addClass("white-header");
    } else {
        $(".main-header").removeClass("white-header");
    }
});

$('#navclick').click(function(){
    $(mainContainer).toggleClass('menu-slide');
    $(clickmenu).toggleClass('menu-slide');
});

$('#navToggle').click(function(){
    $(body).toggleClass('menu-slide');
});

$('.sub-menu-toggle').click(function(e){
    e.preventDefault();
    e.stopPropagation();
    $(this).parents('li').toggleClass('submenu-expanded');
});

$('.link-has-childern').click(function(e){
    if(!$(this).parents('li').hasClass('submenu-expanded')&& $('body').hasClass('menu-slide')){
        $(this).parents('li').addClass('submenu-expanded');
        e.preventDefault();
        e.stopPropagation();
    }
});

$(".button").click(() => {
    $("selector").addClass("popshow");
});

$(".home-page-slider").slick({
    dots: true,
    arrows: false,
    infinite: false,
    fade: true,
    speed: 600,
    slidesToShow: 1,
    slidesToScroll: 1
});


