<!-- header -->
<header class="fixed_header">
    <div class="container clearfix">
        <div class="custom_row">
            <div class="logo">
                <a href="<?php echo home_url(); ?>" tabindex="0">
                    <?php if(is_front_page()){
                        echo '<h1 class="homepage-title">';
                    }?>
                    <?php if (get_field('site_logo_header', 'option')) : ?>
                        <img src="<?php the_field('site_logo_header', 'option'); ?>" alt="Chilterns Logo" />
                    <?php else : ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/logo-header.svg" alt="Chilterns Logo" />
                    <?php endif; ?>
                    <?php if(is_front_page()){
                        echo '</h1>';
                    }?>
                </a>
            </div>
            <div class="site_search search__mobile">
                <div class="search_icon" id="searchIcona" tabindex="0">
                        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/search.svg" alt="Search Icon" />
                    </div>
                    <div class="hdr_search" id="searchBoxa">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            <div class="menu_icon"><a href="#" tabindex="0"><span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span></a></div>
            <div class="main-nav">
                
                <!-- nav -->
                <nav class="nav nav-section">
                    <?php wp_nav_menu(
                        array(
                            'theme_location'  => 'header-menu',
                            'menu'            => 'header-top-menu',
                        )
                    );
                    ?>
                </nav>
                <!-- /nav -->
                <div class="site_search">


                    <div class="search_icon" id="searchIcon" tabindex="0">
                        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/search.svg" alt="Search Icon" />
                    </div>
                    <div class="hdr_search" id="searchBox">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>
        </div>


    </div>
</header>
<!-- /header -->